﻿using CarcassonneDiscovery.Ai.Players;
using CarcassonneDiscovery.Client.Base;
using CarcassonneDiscovery.Entity;
using System;
using System.Collections.Generic;

namespace CarcassonneDiscovery.Ai.Evaluation
{
    /// <summary>
    /// Generování zápasů stylem každý s každým.
    /// </summary>
    public class RoundRobin
    {
        public static IList<Func<PlayerColor, int, ClientApiService, AiPlayer>[]> Generate(BatchArguments batch)
        {
            var previous = new List<Func<PlayerColor, int, ClientApiService, AiPlayer>[]>
            {
                new Func<PlayerColor, int, ClientApiService, AiPlayer>[0]
            };

            foreach (var color in batch.Params.PlayerOrder)
            {
                var next = new List<Func<PlayerColor, int, ClientApiService, AiPlayer>[]>();

                foreach (var prevMatch in previous)
                {
                    foreach (var nextPlayer in batch.Builders[color])
                    {
                        var nextMatch = new Func<PlayerColor, int, ClientApiService, AiPlayer>[prevMatch.Length + 1];
                        Array.Copy(prevMatch, nextMatch, prevMatch.Length);
                        nextMatch[prevMatch.Length] = nextPlayer;

                        next.Add(nextMatch);
                    }
                }

                previous = next;
            }

            return previous;
        }
    }
}
