﻿namespace CarcassonneDiscovery.Ai.Evaluation
{
    using CarcassonneDiscovery.Client.Base;
    using System;

    /// <summary>
    /// Hlavní třída programu.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Vstupní bod programu.
        /// </summary>
        /// <param name="args">Argumenty příkazové řádky.</param>
        public static int Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.Error.WriteLine("Expected one argument.");
                return 1;
            }

            SimulationArguments simulationArguments;

            try
            {
                simulationArguments = ArgumentParser.Parse(args[0]);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine($"Error reading configuration file: {ex.Message}");
                return 1;
            }

            var mainService = new ClientApiService($"{simulationArguments.BaseUrl.TrimEnd('/')}/Api/{{0}}/{{1}}");

            var batchCount = simulationArguments.Batches.Count;
            Console.WriteLine($"[{DateTime.Now}] Read {batchCount} batches");

            for (var i = 0; i < batchCount; i++)
            {
                Console.WriteLine($"[{DateTime.Now}] Starting batch {i+1}/{batchCount}");
                BatchRunner.Run(simulationArguments.Batches[i], mainService);
            }

            Console.WriteLine("Finihsed all.");

            return 0;
        }
    }
}
