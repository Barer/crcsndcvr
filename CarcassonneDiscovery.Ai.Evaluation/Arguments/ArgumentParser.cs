﻿using CarcassonneDiscovery.Ai.Players;
using CarcassonneDiscovery.Client.Base;
using CarcassonneDiscovery.Entity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace CarcassonneDiscovery.Ai.Evaluation
{
    /// <summary>
    /// Parsování vstupu.
    /// </summary>
    public class ArgumentParser
    {
        /// <summary>
        /// Přečte argumenty ze vstupního souboru.
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static SimulationArguments Parse(string filePath)
        {
            var simulationArguments = new SimulationArguments
            {
                BaseUrl = "http://localhost:5000",
                Batches = new List<BatchArguments>()
            };

            BatchArguments batchArguments = null;
            IList<PlayerColor> playerOrder = null;
            IList<Func<PlayerColor, int, ClientApiService, AiPlayer>> playerBuilders = null;

            bool expectBaseAddress = true;
            bool expectNewBatch = true;
            bool expectNewColor = false;
            bool expectNewPlayer = false;
            bool expectEnd = true;

            using (var sr = new StreamReader(filePath))
            {
                string line;

                while ((line = sr.ReadLine()) != null)
                {
                    line = line.Trim().ToUpper();

                    if (line.Length == 0 || line.StartsWith("%"))
                    {
                        continue;
                    }

                    var splitted = line.Split(" ");

                    switch (splitted[0])
                    {
                        case "BASE_ADDRESS":
                            CheckValidity(expectBaseAddress, "BASE_ADDRESS");
                            CheckLength(splitted, 2, "BASE_ADDRESS");
                            simulationArguments.BaseUrl = splitted[1];
                            expectBaseAddress = false;
                            break;

                        case "BATCH":
                            if (playerOrder != null)
                            {
                                batchArguments.Params.PlayerOrder = playerOrder.ToArray();
                                playerOrder = null;
                                playerBuilders = null;
                            }
                            CheckValidity(expectNewBatch, "BATCH");
                            CheckLength(splitted, 4, "BATCH");
                            CheckValue("ROUND_ROBIN", splitted[1], "BATCH(type)");
                            batchArguments = new BatchArguments
                            {
                                Rounds = ParsePositiveInt(splitted[2], "BATCH(rounds)"),
                                Size = ParsePositiveInt(splitted[3], "BATCH(size)"),
                                Params = new GameParams
                                {
                                    FollowerCount = 4,
                                    TileSetId = "Standard"
                                },
                                Builders = new Dictionary<PlayerColor, IList<Func<PlayerColor, int, ClientApiService, AiPlayer>>>()
                            };
                            simulationArguments.Batches.Add(batchArguments);
                            playerOrder = new List<PlayerColor>();
                            expectBaseAddress = false;
                            expectNewBatch = false;
                            expectNewColor = true;
                            expectNewPlayer = false;
                            expectEnd = false;
                            break;

                        case "COLOR":
                            CheckValidity(expectNewColor, "COLOR");
                            CheckLength(splitted, 2, "COLOR");
                            var color = AddColor(splitted[1], playerOrder, "COLOR");
                            playerBuilders = new List<Func<PlayerColor, int, ClientApiService, AiPlayer>>();
                            batchArguments.Builders.Add(color, playerBuilders);
                            expectBaseAddress = false;
                            expectNewBatch = false;
                            expectNewColor = false;
                            expectNewPlayer = true;
                            expectEnd = false;
                            break;

                        case "PLAYER":
                            CheckValidity(expectNewPlayer, "PLAYER");
                            AddPlayer(splitted, playerBuilders, "PLAYER");
                            expectBaseAddress = false;
                            expectNewBatch = playerOrder.Count > 1;
                            expectNewColor = true;
                            expectNewPlayer = true;
                            expectEnd = playerOrder.Count > 1;
                            break;

                        default:
                            throw new ArgumentException($"Unexpected {splitted[0]}");
                    }
                }

                CheckValidity(expectEnd, "END-OF-FILE");

                if (playerOrder != null)
                {
                    batchArguments.Params.PlayerOrder = playerOrder.ToArray();
                }
            }

            return simulationArguments;
        }


        /// <summary>
        /// Naparsuje hráče a zařadí ho.
        /// </summary>
        /// <param name="splitted">Hodnoty příkazu.</param>
        /// <param name="builders">Konstruktory hráčů.</param>
        /// <param name="name">Název příkazu.</param>
        private static void AddPlayer(string[] splitted, IList<Func<PlayerColor, int, ClientApiService, AiPlayer>> builders, string name)
        {
            if (splitted.Length < 2)
            {
                throw new ArgumentException($"Expected at least 2 arguments in {name}");
            }

            switch (splitted[1])
            {
                case "MONTECARLOAI":
                    CheckLength(splitted, 6, "MONTECARLOAI");
                    var simulationCount = ParsePositiveInt(splitted[3], "MONTECARLOAI(simulations)");
                    var discount = ParseSmallDouble(splitted[4], "MONTECARLOAI(discount)");
                    var limit = ParseSmallDouble(splitted[5], "MONTECARLOAI(limit)");
                    Console.WriteLine($"Parsed MC with discount={discount} and limit={limit}");
                    switch (splitted[2])
                    {
                        case "GREEDYHEURISTICS":
                            builders.Add((color, gameId, service) => new MonteCarloAi<GreedyHeuristics>(color, gameId, service, simulationCount, discount, limit));
                            break;
                        case "POTENTIALHEURISTICS":
                            builders.Add((color, gameId, service) => new MonteCarloAi<PotentialHeuristics>(color, gameId, service, simulationCount, discount, limit));
                            break;
                        case "RANDOMHEURISTICS":
                            builders.Add((color, gameId, service) => new MonteCarloAi<RandomHeuristics>(color, gameId, service, simulationCount, discount, limit));
                            break;
                    }
                    break;

                case "EXPECTIMINIMAXAI":
                    CheckLength(splitted, 4, "EXPECTIMINIMAXAI");
                    var depth = ParsePositiveInt(splitted[3], "EXPECTIMINIMAXAI(depth)");
                    Console.WriteLine($"Parsed Expectiminimax with depth={depth}");
                    switch (splitted[2])
                    {
                        case "GREEDYHEURISTICS":
                            builders.Add((color, gameId, service) => new ExpectiminimaxAi<GreedyHeuristics>(color, gameId, service, depth));
                            break;
                        case "POTENTIALHEURISTICS":
                            builders.Add((color, gameId, service) => new ExpectiminimaxAi<PotentialHeuristics>(color, gameId, service, depth));
                            break;
                        case "RANDOMHEURISTICS":
                            builders.Add((color, gameId, service) => new ExpectiminimaxAi<RandomHeuristics>(color, gameId, service, depth));
                            break;
                    }
                    break;

                case "HEURISTICSAI":
                    CheckLength(splitted, 3, "HEURISTICSAI");
                    switch (splitted[2])
                    {
                        case "GREEDYHEURISTICS":
                            builders.Add((color, gameId, service) => new HeuristicsAi<GreedyHeuristics>(color, gameId, service));
                            break;
                        case "POTENTIALHEURISTICS":
                            builders.Add((color, gameId, service) => new HeuristicsAi<PotentialHeuristics>(color, gameId, service));
                            break;
                        case "RANDOMHEURISTICS":
                            builders.Add((color, gameId, service) => new HeuristicsAi<RandomHeuristics>(color, gameId, service));
                            break;
                    }
                    break;

                case "POTENTIALLEARNINGAI":
                    CheckLength(splitted, 4, "POTENTIALLEARNINGAI");
                    var path = splitted[2];
                    var step = ParseSmallDouble(splitted[3], "POTENTIALLEARNINGAI");
                    Console.WriteLine($"Parsed {step}");
                    builders.Add((color, gameId, service) => new PotentialLearningAi(color, gameId, service, path, path, step));
                    break;

                default:
                    throw new ArgumentException($"Unexpected value {splitted[1]} of {name}");
            }

        }

        /// <summary>
        /// Naparsuje barvu hráče a zařadí ji do pořadí.
        /// </summary>
        /// <param name="value">Hodnota barvy.</param>
        /// <param name="order">Pořadí hráčů na tahu.</param>
        /// <param name="name">Název příkazu.</param>
        private static PlayerColor AddColor(string value, IList<PlayerColor> order, string name)
        {
            PlayerColor color;

            switch (value)
            {
                case "R":
                case "RED":
                    color = PlayerColor.Red;
                    break;
                case "G":
                case "GREEN":
                    color = PlayerColor.Green;
                    break;
                case "B":
                case "BLUE":
                    color = PlayerColor.Blue;
                    break;
                case "Y":
                case "YELLOW":
                    color = PlayerColor.Yellow;
                    break;
                case "K":
                case "BLACK":
                    color = PlayerColor.Black;
                    break;
                default:
                    throw new ArgumentException($"Unexpected value {value} of {name}");
            }

            if (order.Contains(color))
            {
                throw new ArgumentException($"Color {value} has already been added in the batch.");
            }

            order.Add(color);

            return color;
        }

        /// <summary>
        /// Naparsuje kladné číslo.
        /// </summary>
        /// <param name="value">Hodnota čísla.</param>
        /// <param name="name">Název příkazu.</param>
        /// <returns>Kladné číslo.</returns>
        private static int ParsePositiveInt(string value, string name)
        {
            if (!int.TryParse(value, out var i))
            {
                throw new ArgumentException($"Expected integer in {name}");
            }

            if (i <= 0)
            {
                throw new ArgumentException($"Expected positive integer in {name}");
            }

            return i;
        }

        /// <summary>
        /// Naparsuje reálné číslo v rozsahu 0-1.
        /// </summary>
        /// <param name="value">Hodnota čísla.</param>
        /// <param name="name">Název příkazu.</param>
        /// <returns>Reáné číslo.</returns>
        private static double ParseSmallDouble(string value, string name)
        {
            if (!double.TryParse(value, NumberStyles.Float, CultureInfo.GetCultureInfo("cs-CZ"), out var d))
            {
                throw new ArgumentException($"Expected double in {name}");
            }

            if (d < 0 || d > 1)
            {
                throw new ArgumentException($"Expected double in range [0;1] in {name}");
            }

            return d;
        }

        /// <summary>
        /// Zkontroluje platnost příkazu a délku argumentů.
        /// </summary>
        /// <param name="expected">Očekávaná hodnota.</param>
        /// <param name="expected">Skutečná hodnota.</param>
        /// <param name="name">Název příkazu.</param>
        private static void CheckValue(string expected, string actual, string name)
        {
            if (expected != actual)
            {
                throw new ArgumentException($"Expected value {expected} of {name}, got {actual}");
            }
        }

        /// <summary>
        /// Zkontroluje platnost příkazu a délku argumentů.
        /// </summary>
        /// <param name="expected">Byl příkaz očekávaný?</param>
        /// <param name="name">Název příkazu.</param>
        private static void CheckValidity(bool expected, string name)
        {
            if (!expected)
            {
                throw new ArgumentException($"Unexpected {name}");
            }
        }

        /// <summary>
        /// Zkontroluje délku argumentů.
        /// </summary>
        /// <param name="splitted">Řádek rozdělený podle částí.</param>
        /// <param name="argsCount">Očekávaný počet argumentů.</param>
        /// <param name="name">Název příkazu.</param>
        private static void CheckLength(string[] splitted, int argsCount, string name)
        {
            if (splitted.Length != argsCount)
            {
                throw new ArgumentException($"Expected {argsCount} arguments in {name}, got {splitted.Length}");
            }
        }
    }
}
