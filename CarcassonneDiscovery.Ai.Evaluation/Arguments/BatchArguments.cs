﻿using CarcassonneDiscovery.Ai.Players;
using CarcassonneDiscovery.Client.Base;
using CarcassonneDiscovery.Entity;
using System;
using System.Collections.Generic;

namespace CarcassonneDiscovery.Ai.Evaluation
{
    /// <summary>
    /// Argumenty jedné dávky.
    /// </summary>
    public class BatchArguments
    {
        /// <summary>
        /// Maximání počet paralelně běžících her.
        /// </summary>
        public int Size { get; set; }

        /// <summary>
        /// Celkový počet kol.
        /// </summary>
        public int Rounds { get; set; }

        /// <summary>
        /// Parametry hry.
        /// </summary>
        public GameParams Params { get; set; }

        /// <summary>
        /// Konstruktory hráčů.
        /// </summary>
        public IDictionary<PlayerColor, IList<Func<PlayerColor, int, ClientApiService, AiPlayer>>> Builders { get; set; }
    }
}