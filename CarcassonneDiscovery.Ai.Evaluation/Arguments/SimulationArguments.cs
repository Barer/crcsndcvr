﻿using System.Collections.Generic;

namespace CarcassonneDiscovery.Ai.Evaluation
{
    /// <summary>
    /// Argumenty simulace.
    /// </summary>
    public class SimulationArguments
    {
        /// <summary>
        /// Základ URL.
        /// </summary>
        public string BaseUrl { get; set; }

        /// <summary>
        /// Agumenty jednotlivých dávek.
        /// </summary>
        public IList<BatchArguments> Batches {get; set; }
    }
}
