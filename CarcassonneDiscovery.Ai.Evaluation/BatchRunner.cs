﻿using CarcassonneDiscovery.Ai.Players;
using CarcassonneDiscovery.Api;
using CarcassonneDiscovery.Client.Base;
using CarcassonneDiscovery.Entity;
using System;
using System.Collections.Generic;
using System.Threading;

namespace CarcassonneDiscovery.Ai.Evaluation
{
    /// <summary>
    /// Spouštění dávkových simulací.
    /// </summary>
    public class BatchRunner
    {
        /// <summary>
        /// Spustí dávku simulací.
        /// </summary>
        /// <param name="batch">Argumenty.</param>
        /// <param name="mainService">Hlavní služba pro komunikaci.</param>
        public static void Run(BatchArguments batch, ClientApiService mainService)
        {
            var matchList = RoundRobin.Generate(batch);

            Console.WriteLine($"[{DateTime.Now}] Generated {matchList.Count} matches for {batch.Rounds} rounds: total {matchList.Count * batch.Rounds}");

            var finishedEvent = new ManualResetEventSlim();
            var rwLock = new object();
            var running = 0;
            var round = 0;
            var finished = false;

            using (var matchEnumerator = matchList.GetEnumerator())
            {
                var finishedAction = new Action(() =>
                {
                    lock (rwLock)
                    {
                        running--;
                        finishedEvent.Set();
                        mainService.ArchiveFinishedGames().Wait();
                    }
                });

                finishedEvent.Set();

                while (!finished)
                {
                    finishedEvent.Wait();

                    lock (rwLock)
                    {
                        // Spouštěj nové simulace, dokud máme kapacitu
                        while (running < batch.Size)
                        {
                            if (round == 0)
                            {
                                Console.WriteLine($"[{DateTime.Now}] Started round {++round}/{batch.Rounds}");
                            }

                            if (matchEnumerator.MoveNext())
                            {
                                running++;
                                StartNext(mainService, batch.Params, matchEnumerator.Current, finishedAction);
                            }
                            else
                            {
                                matchEnumerator.Reset();

                                // Pokud jsme došli na konec, dále nespouštíme a jdeme do vyčkávacího režimu
                                if (round == batch.Rounds)
                                {
                                    finished = true;
                                    break;
                                }

                                Console.WriteLine($"[{DateTime.Now}] Started round {++round}/{batch.Rounds}");
                            }
                        }

                        finishedEvent.Reset();
                    }
                }

                // Už čekáme jen na dohrání
                while (running > 0)
                {
                    finishedEvent.Wait();
                    finishedEvent.Reset();
                }
            }

            Console.WriteLine($"[{DateTime.Now}] Finished batch");
        }


        /// <summary>
        /// Zahájí novou simulaci.
        /// </summary>
        /// <param name="mainService">Služba pro komunikaci klienta se serveren.</param>
        /// <param name="gameParams">Parametry hry.</param>
        /// <param name="builders">Buildery hráčů.</param>
        /// <param name="finishedAction">Akce, která se provede po ukonční simulace.</param>
        private static void StartNext(ClientApiService mainService, GameParams gameParams, Func<PlayerColor, int, ClientApiService, AiPlayer>[] builders, Action finishedAction)
        {
            var thread = new Thread(() =>
            {
                var gameIdTask = mainService.StartGame(new StartGameRequest
                {
                    FollowerCount = gameParams.FollowerCount,
                    PlayerOrder = gameParams.PlayerOrder,
                    TileSetId = gameParams.TileSetId
                });
                gameIdTask.Wait();

                if (gameIdTask.Result.IsError)
                {
                    Console.Error.WriteLine($"[{DateTime.Now}] Cannot receive new game id: " + gameIdTask.Result.ErrorMessage);
                    return;
                }

                var gameId = gameIdTask.Result.GameId;

                Console.WriteLine($"\t[{DateTime.Now}] Starting game #{gameId}");

                var players = new List<AiPlayer>();

                for (var i = 0; i < builders.Length; i++)
                {
                    var aiPlayer = builders[i].Invoke(gameParams.PlayerOrder[i], gameId, mainService);
                    aiPlayer.Start();
                    players.Add(aiPlayer);
                }

                var isRunningIndex = 0;

                while (isRunningIndex < players.Count)
                {
                    if (players[isRunningIndex].IsRunning)
                    {
                        Thread.Sleep(5000);
                    }
                    else
                    {
                        isRunningIndex++;
                    }
                }

                Console.WriteLine($"\t[{DateTime.Now}] Finished game #{gameId}");
                finishedAction.Invoke();
            });

            thread.Start();
        }
    }
}
