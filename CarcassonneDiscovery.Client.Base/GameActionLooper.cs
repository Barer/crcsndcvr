﻿namespace CarcassonneDiscovery.Client.Base
{
    using CarcassonneDiscovery.Api;
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// Smyčka pro získávání herních akcí.
    /// </summary>
    public class GameActionLooper
    {
        /// <summary>
        /// Služba pro komunikaci s API serveru.
        /// </summary>
        protected PlayerApiService Service;

        /// <summary>
        /// Kontruktor.
        /// </summary>
        /// <param name="service">Služba pro komunikaci s API serveru.</param>
        public GameActionLooper(PlayerApiService service)
        {
            Service = service;
        }

        /// <summary>
        /// Postupně čte herní akce.
        /// </summary>
        /// <param name="gameId">Identifikátor hry.</param>
        /// <param name="actionId">Identifikátor první akce.</param>
        /// <returns>Posloupnost herních akcí.</returns>
        public IEnumerable<GameActionResponse> Run(int gameId, int actionId = 0)
        {
            GameActionResponse response;

            while (true)
            {
                try
                {
                    var responseTask = Service.GameAction(new GameActionRequest
                    {
                        GameId = gameId,
                        ActionId = actionId
                    });

                    responseTask.Wait();

                    response = responseTask.Result;
                }
                catch (ThreadAbortException)
                {
                    Console.Error.WriteLine("ThreadAbortException");
                    break;
                }
                catch (TaskCanceledException)
                {
                    WaitOnTimeOut();
                    continue;
                }
                catch (AggregateException)
                {
                    WaitOnTimeOut();
                    continue;
                }

                if (response == null)
                {
                    Console.Error.WriteLine("Null response");
                    continue;
                }

                if (!response.IsError)
                {
                    yield return response;
                    actionId++;

                    if (response.Type == GameActionType.GameEnd || response.Type == GameActionType.GameQuit)
                    {
                        break;
                    }
                }
                else if (response.ErrorMessage.ToLower().Contains("timeout"))
                {
                    WaitOnTimeOut();
                    continue;
                }
                else
                {
                    Console.Error.WriteLine($"Error ({actionId}): {response.ErrorMessage}");
                    break;
                }
            }
        }

        /// <summary>
        /// Čeká při časovém timeoutu.
        /// </summary>
        protected void WaitOnTimeOut()
        {
            Thread.Sleep(1000);
        }
    }
}
