﻿using System;

namespace CarcassonneDiscovery.Client.Base
{
    using CarcassonneDiscovery.Api;
    using System.Net.Http;
    using System.Text;
    using System.Text.Json;
    using System.Threading.Tasks;

    /// <summary>
    /// Služba pro komunikaci s API serveru.
    /// </summary>
    public class PlayerApiService
    {
        /// <summary>
        /// Základ URL pro posílání požadavků.
        /// </summary>
        protected string UrlPattern;

        /// <summary>
        /// Testovací mód?
        /// </summary>
        public bool TestMode;

        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="urlPattern">Šablona URL pro posílání žádostí.</param>
        public PlayerApiService(string urlPattern = "https://localhost/Api/{0}/{1}")
        {
            UrlPattern = urlPattern;
        }

        #region Control sekce
        /// <summary>
        /// Žádost o ukončení hry.
        /// </summary>
        /// <param name="request">Parametry.</param>
        /// <returns>Výsledek akce.</returns>
        public async Task<ResponseBase> StopGame(StopGameRequest request)
        {
            return await PostAndReadAsJsonAsync<StopGameRequest, ResponseBase>("Control", "StopGame", request);
        }

        /// <summary>
        /// Žádost o registraci jména hráče.
        /// </summary>
        /// <param name="request">Parametry.</param>
        /// <returns>Výsledek akce.</returns>
        public async Task<ResponseBase> RegisterName(RegisterNameRequest request)
        {
	        if (TestMode)
	        {
		        request.Name += "-Test";
	        }
            return await PostAndReadAsJsonAsync<RegisterNameRequest, ResponseBase>("Control", "RegisterName", request);
        }
        #endregion

        #region Game sekce
        /// <summary>
        /// Žádost o výsledek herní akce.
        /// </summary>
        /// <param name="request">Parametry.</param>
        /// <returns>Výsledek akce.</returns>
        public async Task<GameActionResponse> GameAction(GameActionRequest request)
        {
            return await PostAndReadAsJsonAsync<GameActionRequest, GameActionResponse>("Game", "GameAction", request);
        }

        /// <summary>
        /// Žádost o umístění kartičky.
        /// </summary>
        /// <param name="request">Parametry.</param>
        /// <returns>Výsledek akce.</returns>
        public async Task<ResponseBase> PlaceTile(PlaceTileRequest request)
        {
	        if (TestMode)
	        {
                Console.WriteLine($"{request.GameId},{request.Color}: Move");
	        }

            return await PostAndReadAsJsonAsync<PlaceTileRequest, ResponseBase>("Game", "PlaceTile", request);
        }

        /// <summary>
        /// Žádost o umístění figurky.
        /// </summary>
        /// <param name="request">Parametry.</param>
        /// <returns>Výsledek akce.</returns>
        public async Task<ResponseBase> PlaceFollower(PlaceFollowerRequest request)
        {
            return await PostAndReadAsJsonAsync<PlaceFollowerRequest, ResponseBase>("Game", "PlaceFollower", request);
        }

        /// <summary>
        /// Žádost o odebrání figurky.
        /// </summary>
        /// <param name="request">Parametry.</param>
        /// <returns>Výsledek akce.</returns>
        public async Task<ResponseBase> RemoveFollower(RemoveFollowerRequest request)
        {
            return await PostAndReadAsJsonAsync<RemoveFollowerRequest, ResponseBase>("Game", "RemoveFollower", request);
        }

        /// <summary>
        /// Žádost o předání tahu.
        /// </summary>
        /// <param name="request">Parametry.</param>
        /// <returns>Výsledek akce.</returns>
        public async Task<ResponseBase> PassMove(PassMoveRequest request)
        {
            return await PostAndReadAsJsonAsync<PassMoveRequest, ResponseBase>("Game", "PassMove", request);
        }

        /// <summary>
        /// Žádost o detail hry.
        /// </summary>
        /// <param name="request">Parametry.</param>
        /// <returns>Výsledek akce.</returns>
        public async Task<GameDetailResponse> GameDetail(GameDetailRequest request)
        {
            return await PostAndReadAsJsonAsync<GameDetailRequest, GameDetailResponse>("Game", "GameDetail", request);
        }
        #endregion

        /// <summary>
        /// Průběh žádosti.
        /// </summary>
        /// <typeparam name="TRequest">Typ žádosti.</typeparam>
        /// <typeparam name="TResponse">Typ výsedku akce.</typeparam>
        /// <param name="section">Název sekce akce.</param>
        /// <param name="action">Název akce.</param>
        /// <param name="request">Žádost.</param>
        /// <returns>Výsledek akce.</returns>
        protected async Task<TResponse> PostAndReadAsJsonAsync<TRequest, TResponse>(string section, string action, TRequest request)
        {
            var json = JsonSerializer.Serialize(request);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            return await PostContentAndReadAsJsonAsync<TResponse>(section, action, content);
        }

        /// <summary>
        /// Průběh žádosti.
        /// </summary>
        /// <typeparam name="TResponse">Typ výsedku akce.</typeparam>
        /// <param name="section">Název sekce akce.</param>
        /// <param name="action">Název akce.</param>
        /// <param name="content">Obsah žádosti.</param>
        /// <returns>Výsledek akce.</returns>
        protected async Task<TResponse> PostContentAndReadAsJsonAsync<TResponse>(string section, string action, HttpContent content)
        {
            string data = null;
            var url = string.Format(UrlPattern, section, action);

            using (var client = new HttpClient())
            {
                while (string.IsNullOrEmpty(data))
                {
                    try
                    {
                        //Console.WriteLine($"{DateTime.Now} Sending to {url}");
                        var msg = await client.PostAsync(url, content);
                        data = await msg.Content.ReadAsStringAsync();
                    }
                    catch (Exception ex)
                    {
                        //Console.WriteLine($"{DateTime.Now} {ex.Message}");
                        data = null;
                    }
                }

                //Console.WriteLine($"{DateTime.Now} Received {url}");
                var response = JsonSerializer.Deserialize<TResponse>(data, new JsonSerializerOptions() { PropertyNameCaseInsensitive = true });
                return response;
            }
        }
    }
}
