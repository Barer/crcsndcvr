﻿namespace CarcassonneDiscovery.Client.Base
{
    using CarcassonneDiscovery.Api;
    using System.Threading.Tasks;

    /// <summary>
    /// Služba pro komunikaci s API serveru.
    /// </summary>
    public class ClientApiService : PlayerApiService
    {
        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="urlPattern">Šablona URL pro posílání žádostí.</param>
        public ClientApiService(string urlPattern = "https://localhost/Api/{0}/{1}") : base(urlPattern)
        {
        }

        #region Control sekce
        /// <summary>
        /// Žádost o zahájení nové hry.
        /// </summary>
        /// <param name="request">Parametry.</param>
        /// <returns>Výsledek akce.</returns>
        public async Task<StartGameResponse> StartGame(StartGameRequest request)
        {
            return await PostAndReadAsJsonAsync<StartGameRequest, StartGameResponse>("Control", "StartGame", request);
        }

        /// <summary>
        /// Žádost o seznam her v průběhu.
        /// </summary>
        /// <returns>Výsledek akce.</returns>
        public async Task<GameListResponse> GameList()
        {
            return await PostAndReadAsJsonAsync<GameListResponse>("Control", "GameList");
        }

        /// <summary>
        /// Žádost o archivaci ukončených her.
        /// </summary>
        /// <returns>Výsledek akce.</returns>
        public async Task<ResponseBase> ArchiveFinishedGames()
        {
            return await PostAndReadAsJsonAsync<ResponseBase>("Control", "ArchiveFinishedGames");
        }
        #endregion

        /// <summary>
        /// Průběh žádosti.
        /// </summary>
        /// <typeparam name="TResponse">Typ výsedku akce.</typeparam>
        /// <param name="section">Název sekce akce.</param>
        /// <param name="action">Název akce.</param>
        /// <returns>Výsledek akce.</returns>
        protected async Task<TResponse> PostAndReadAsJsonAsync<TResponse>(string section, string action)
        {
            return await PostContentAndReadAsJsonAsync<TResponse>(section, action, null);
        }
    }
}
