﻿namespace CarcassonneDiscovery.Server.Database
{
    using CarcassonneDiscovery.Api;
    using System;

    /// <summary>
    /// Chyba v žádosti.
    /// </summary>
    public class RequestException : Exception
    {
        /// <summary>
        /// Kód chyby.
        /// </summary>
        public ErrorCode ErrorCode { get; protected set; }

        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="code">Kód chyby.</param>
        /// <param name="msg">Chybová hláška.</param>
        public RequestException(ErrorCode code, string msg = null) : base(msg ?? code.ToString())
        {
            ErrorCode = code;
        }
    }
}
