﻿namespace CarcassonneDiscovery.Server.Database
{
    using CarcassonneDiscovery.Api;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Hra.
    /// </summary>
    public class Game
    {
        /// <summary>
        /// Identifikátor hry.
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Skončila hra?
        /// </summary>
        public GameProgress Progress { get; set; }

        /// <summary>
        /// Počet hráčů ve hře.
        /// </summary>
        public int PlayerCount { get; set; }

        /// <summary>
        /// Název použité sady kartiček.
        /// </summary>
        public string TileSetId { get; set; }

        /// <summary>
        /// Počet figurek každého hráče.
        /// </summary>
        public int FollowerCount { get; set; }

        /// <summary>
        /// Seznam hráčů.
        /// </summary>
        public virtual ICollection<GamePlayer> GamePlayers { get; set; }

        /// <summary>
        /// Seznam herních akcí.
        /// </summary>
        public virtual ICollection<GameAction> GameActions { get; set; }
    }
}
