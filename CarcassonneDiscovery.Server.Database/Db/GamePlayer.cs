﻿namespace CarcassonneDiscovery.Server.Database
{
    using CarcassonneDiscovery.Entity;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Hráč ve hře.
    /// </summary>
    public class GamePlayer
    {
        /// <summary>
        /// Primární klíč záznamu.
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Identifikátor hry.
        /// </summary>
        public int GameId { get; set; }

        /// <summary>
        /// Reference na hru.
        /// </summary>
        public Game Game { get; set; }

        /// <summary>
        /// Pořadí hráče na tahu.
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// Barva hráče.
        /// </summary>
        public PlayerColor Color { get; set; }

        /// <summary>
        /// Skóre hráče.
        /// </summary>
        public int Score { get; set; }

        /// <summary>
        /// Jméno hráče.
        /// </summary>
        public string Name { get; set; }
    }
}
