﻿namespace CarcassonneDiscovery.Server.Database
{
    using CarcassonneDiscovery.Api;
    using CarcassonneDiscovery.Entity;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// Herní akce.
    /// </summary>
    public class GameAction
    {
        /// <summary>
        /// Primární klíč záznamu.
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Identifikátor hry.
        /// </summary>
        public int GameId { get; set; }

        /// <summary>
        /// Reference na hru.
        /// </summary>
        public Game Game { get; set; }

        /// <summary>
        /// Pořadí akce ve hře.
        /// </summary>
        public int ActionId { get; set; }

        /// <summary>
        /// Typ akce.
        /// </summary>
        public GameActionType Type { get; set; }

        /// <summary>
        /// Barva hráče na tahu.
        /// </summary>
        public PlayerColor? Color { get; set; }

        /// <summary>
        /// X-souřadnice kartičky nebo figurky.
        /// </summary>
        public int? CoordsX { get; set; }

        /// <summary>
        /// Y-souřadnice kartičky nebo figurky.
        /// </summary>
        public int? CoordsY { get; set; }

        /// <summary>
        /// Souřadnice kartičky nebo figurky.
        /// </summary>
        [NotMapped]
        public Coords? Coords
        {
            get
            {
                if (CoordsX.HasValue && CoordsY.HasValue)
                {
                    return new Coords(CoordsX.Value, CoordsY.Value);
                }

                return null;
            }

            set
            {
                CoordsX = value?.X;
                CoordsY = value?.Y;
            }
        }

        /// <summary>
        /// Identifikátor kartičky.
        /// </summary>
        public string TileId { get; set; }

        /// <summary>
        /// Orientace kartičky.
        /// </summary>
        public TileOrientation? TileOrientation { get; set; }

        /// <summary>
        /// Id regionu na kartičce.
        /// </summary>
        public int? RegionId { get; set; }

        /// <summary>
        /// Skóre za tah.
        /// </summary>
        public int? Score { get; set; }
    }
}
