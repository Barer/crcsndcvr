﻿namespace CarcassonneDiscovery.Server.Database
{
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Kontext pro práci s databází.
    /// </summary>
    public class DatabaseContext : DbContext
    {
        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="options">Parametry.</param>
        public DatabaseContext(DbContextOptions options) : base(options)
        {            
        }

        /// <summary>
        /// Tabulka her.
        /// </summary>
        public DbSet<Game> Games { get; set; }

        /// <summary>
        /// Tabulka herních akcí.
        /// </summary>
        public DbSet<GameAction> GameActions { get; set; }

        /// <summary>
        /// Tabulka hráčů.
        /// </summary>
        public DbSet<GamePlayer> GamePlayers { get; set; }
    }
}
