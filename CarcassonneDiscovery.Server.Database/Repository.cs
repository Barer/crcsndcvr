﻿namespace CarcassonneDiscovery.Server.Database
{
    using CarcassonneDiscovery.Api;
    using CarcassonneDiscovery.Entity;
    using Microsoft.EntityFrameworkCore;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Pomocná třída pro přístup k databázi.
    /// </summary>
    public class Repository
    {
        /// <summary>
        /// Kontext databáze.
        /// </summary>
        private readonly DatabaseContext DB;

        /// <summary>
        /// Konsttruktor.
        /// </summary>
        /// <param name="dbContext">Kontext databáze.</param>
        public Repository(DatabaseContext dbContext)
        {
            DB = dbContext;
        }

        /// <summary>
        /// Vrátí akci ve hře.
        /// </summary>
        /// <param name="gameId">Identifikátor hry.</param>
        /// <param name="actionId">Číslo akce ve hře.</param>
        /// <returns>Akce ve hře.</returns>
        /// <exception cref="RequestException">Pokud je akce neplatná.</exception>

        public GameAction GetAction(int gameId, int actionId)
        {
            if (gameId < 0)
            {
                throw new RequestException(ErrorCode.InvalidParameters);
            }

            var action = DB.GameActions.FirstOrDefault(x => x.GameId == gameId && x.ActionId == actionId);

            if (action != null)
            {
                return action;
            }

            var game = DB.Games.Include(x => x.GameActions).FirstOrDefault(x => x.Id == gameId);

            if (game == null)
            {
                throw new RequestException(ErrorCode.InvalidParameters);
            }

            if (actionId > game.GameActions.Count)
            {
                throw new RequestException(ErrorCode.InvalidParameters, $"Too high value. Last played action id is {game.GameActions.Count - 1}.");
            }

            if (actionId == game.GameActions.Count)
            {
                if (game.GameActions.First(x => x.ActionId == actionId - 1).Type == GameActionType.GameEnd)
                {
                    throw new RequestException(ErrorCode.GameAlreadyFinished);
                }

                throw new RequestException(ErrorCode.WaitTimeout);
            }

            throw new RequestException(ErrorCode.UnexpectedTimeout);
        }

        /// <summary>
        /// Registrace jména hráče.
        /// </summary>
        /// <param name="gameId">Identifikátor hry.</param>
        /// <param name="color">Barva hráče.</param>
        /// <param name="name">Jméno hráče.</param>
        /// <exception cref="RequestException">Pokud je akce neplatná.</exception>
        public void RegisterPlayerName(int gameId, PlayerColor color, string name)
        {
            var game = DB.Games.Include(x => x.GamePlayers).FirstOrDefault(x => x.Id == gameId);

            if (game == null)
            {
                throw new RequestException(ErrorCode.InvalidParameters);
            }

            var player = game.GamePlayers.FirstOrDefault(x => x.Color == color);

            if (player == null)
            {
                throw new RequestException(ErrorCode.InvalidParameters);
            }

            player.Name = name;

            DB.SaveChanges();
        }

        /// <summary>
        /// Přidá novou hru.
        /// </summary>
        /// <param name="gameParams">Parametry hry.</param>
        /// <returns>Identifikátor hry.</returns>
        /// <exception cref="RequestException">Pokud je akce neplatná.</exception>
        public int AddGame(GameParams gameParams)
        {
            if (gameParams.PlayerOrder == null)
            {
                throw new RequestException(ErrorCode.InvalidParameters);
            }

            var game = new Game
            {
                Progress = GameProgress.InProgress,
                FollowerCount = gameParams.FollowerCount,
                PlayerCount = gameParams.PlayerOrder.Length,
                TileSetId = gameParams.TileSetId,
                GamePlayers = new List<GamePlayer>()
            };

            for (var i = 0; i < gameParams.PlayerOrder.Length; i++)
            {
                game.GamePlayers.Add(new GamePlayer
                {
                    Color = gameParams.PlayerOrder[i],
                    Order = i + 1,
                    Score = 0,
                    Name = null
                });
            }

            DB.Add(game);
            DB.SaveChanges();

            return game.Id;
        }

        /// <summary>
        /// Přidá akci hry.
        /// </summary>
        /// <param name="action">Akce ve hře.</param>
        /// <exception cref="RequestException">Pokud je akce neplatná.</exception>
        public void AddAction(GameAction action)
        {
            var game = DB.Games.Include(x => x.GameActions).FirstOrDefault(x => x.Id == action.GameId);

            if (game == null)
            {
                throw new RequestException(ErrorCode.InvalidParameters);
            }

            action.ActionId = game.GameActions.Any() ? game.GameActions.Max(x => x.ActionId) + 1 : 0;


            game.GameActions.Add(action);

            DB.SaveChanges();
        }

        /// <summary>
        /// Vrátí hru a hráče. podle identifikátoru.
        /// </summary>
        /// <param name="gameId">Identifikátor hry.</param>
        /// <param name="includePlayers">Vrátit hru včeteně hráčů?</param>
        /// <param name="includeActions">Vrátit hru včetně akcí?</param>
        public Game GetGame(int gameId, bool includePlayers = false, bool includeActions = false)
        {
            var query = DB.Games.AsNoTracking();

            if (includePlayers)
            {
                query = query.Include(x => x.GamePlayers);
            }

            if (includeActions)
            {
                query = query.Include(x => x.GameActions);
            }

            return query.FirstOrDefault(x => x.Id == gameId);
        }

        /// <summary>
        /// Ukončí hru a nastaví skóre.
        /// </summary>
        /// <param name="gameId">Identifikátor hry.</param>
        /// <param name="scores">Skóre hráčů.</param>
        /// <exception cref="RequestException">Pokud je akce neplatná.</exception>
        public void FinishGame(int gameId, IDictionary<PlayerColor, int> scores)
        {
            var game = DB.Games.Include(x => x.GamePlayers).FirstOrDefault(x => x.Id == gameId);

            if (game == null)
            {
                throw new RequestException(ErrorCode.InvalidParameters);
            }

            game.Progress = GameProgress.Finished;

            foreach (var player in game.GamePlayers)
            {
                if (!scores.TryGetValue(player.Color, out int score))
                {
                    throw new RequestException(ErrorCode.InvalidParameters);
                }

                player.Score = score;
            }

            DB.SaveChanges();
        }

        /// <summary>
        /// Nastaví skóre hráče.
        /// </summary>
        /// <param name="gameId">Identifikátor hry.</param>
        /// <param name="color">Barva hráče.</param>
        /// <param name="score">Skóre hráče.</param>
        /// <exception cref="RequestException">Pokud je akce neplatná.</exception>

        public void SetScore(int gameId, PlayerColor color, int score)
        {
            var player = DB.GamePlayers.FirstOrDefault(x => x.GameId == gameId && x.Color == color);

            if (player == null)
            {
                throw new RequestException(ErrorCode.InvalidParameters);
            }

            player.Score = score;

            DB.SaveChanges();
        }

        /// <summary>
        /// Ukončí hru.
        /// </summary>
        /// <param name="gameId">Id hry.</param>
        public void StopGame(int gameId)
        {
            var game = DB.Games
                .Include(x => x.GameActions)
                .FirstOrDefault(x => x.Id == gameId);

            if (game == null)
            {
                throw new RequestException(ErrorCode.InvalidParameters);
            }

            if (game.Progress == GameProgress.InProgress)
            {
                game.Progress = GameProgress.Dead;

                game.GameActions.Add(new GameAction
                {
                    GameId = gameId,
                    ActionId = game.GameActions.Any() ? game.GameActions.Max(x => x.ActionId) + 1 : 0,
                    Type = GameActionType.GameEnd
                });
            }

            DB.SaveChanges();
        }
    }
}
