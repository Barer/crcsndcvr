﻿namespace CarcassonneDiscovery.Server.Database.Migrations
{
    using Microsoft.EntityFrameworkCore.Migrations;

    public partial class RenameEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Finished",
                table: "Games",
                newName: "Progress");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Progress",
                table: "Games",
                newName: "Finished");
        }
    }
}
