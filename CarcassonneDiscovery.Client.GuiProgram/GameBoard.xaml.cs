﻿namespace CarcassonneDiscovery.Client.GuiProgram
{
    using CarcassonneDiscovery.Api;
    using CarcassonneDiscovery.Entity;
    using CarcassonneDiscovery.StandardTileSet;
    using CarcassonneDiscovery.Tools;
    using System;
    using System.Collections.Generic;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;

    /// <summary>
    /// Deska s položenými kartičkami.
    /// </summary>
    public partial class GameBoard : UserControl
    {
        /// <summary>
        /// Uživatel vybral danou pozici.
        /// </summary>
        public event Action<Coords> CoordsSelected = (coords) => { };

        /// <summary>
        /// Uživatel vybral daný region.
        /// </summary>
        public event Action<Coords, int> RegionSelected = (coords, regionId) => { };

        /// <summary>
        /// Uživatel vybral danou figurku.
        /// </summary>
        public event Action<PlayerColor, Coords, int> FollowerSelected = (color, coords, regionId) => { };

        /// <summary>
        /// Velikost kartičky.
        /// </summary>
        protected const double TileSize = 480;

        /// <summary>
        /// Seznam světových směrů.
        /// </summary>
        protected readonly TileOrientation[] CardinalDirections = new TileOrientation[4]
        {
            TileOrientation.N, TileOrientation.E, TileOrientation.S, TileOrientation.W
        };

        /// <summary>
        /// Seznam kartiček ze standardní sady.
        /// </summary>
        protected IList<ITileScheme> StandardTileSet;

        /// <summary>
        /// Umístěné kartičky.
        /// </summary>
        protected Dictionary<Coords, PlacedTileInfo> PlacedTiles;

        /// <summary>
        /// Neumístěné kartičky - okolí stávajících kartiček.
        /// </summary>
        protected Dictionary<Coords, PlacedTileInfo> EmptyTiles;

        /// <summary>
        /// Umístěné figurky.
        /// </summary>
        protected Dictionary<Coords, PlacedFollowerInfo> PlacedFollowers;

        /// <summary>
        /// Volná místa pro položení figurky.
        /// </summary>
        protected List<RegionPlaceInfo> RegionPlaces;

        /// <summary>
        /// Konstruktor.
        /// </summary>
        public GameBoard()
        {
            InitializeComponent();

            Clear();
        }

        /// <summary>
        /// Vyprázdní celou mřížku.
        /// </summary>
        public virtual void Clear(bool showRegionPlaces = true)
        {
            StandardTileSet = StandardTileGenerator.GenerateStandardTileSet();

            PlacedTiles = new Dictionary<Coords, PlacedTileInfo>();
            EmptyTiles = new Dictionary<Coords, PlacedTileInfo>();
            PlacedFollowers = new Dictionary<Coords, PlacedFollowerInfo>();
            RegionPlaces = new List<RegionPlaceInfo>();

            Dispatcher.Invoke(() =>
            {
                Grid.Children.Clear();

                CenterGrid();
            });
        }

        /// <summary>
        /// Změní mřížku podle herní akce.
        /// </summary>
        /// <param name="response">Provedená herní akce.</param>
        public virtual void ShowAction(GameActionResponse response)
        {
            switch (response.Type)
            {
                case GameActionType.StartGame:
                    PlaceTile(response.TileId, response.Coords.Value, response.TileOrientation.Value);
                    RemoveRegions();
                    break;

                case GameActionType.PlaceTile:
                    PlaceTile(response.TileId, response.Coords.Value, response.TileOrientation.Value);
                    PlaceRegions(response.Coords.Value);
                    break;

                case GameActionType.PlaceFollower:
                    PlaceFollower(response.Color.Value, response.Coords.Value, response.RegionId.Value);
                    RemoveRegions();
                    break;

                case GameActionType.RemoveFollower:
                    RemoveFollower(response.Coords.Value);
                    RemoveRegions();
                    break;

                case GameActionType.PassMove:
                    RemoveRegions();
                    break;
            }
        }

        /// <summary>
        /// Změní mřížku podle vrácení herní akce.
        /// </summary>
        /// <param name="response">Provedená herní akce.</param>
        public virtual void RevertAction(GameActionResponse response)
        {
            switch (response.Type)
            {
                case GameActionType.StartGame:
                case GameActionType.PlaceTile:
                    RemoveTile(response.Coords.Value);
                    RemoveRegions();
                    break;

                case GameActionType.PlaceFollower:
                    RemoveFollower(response.Coords.Value);
                    RemoveRegions();
                    break;

                case GameActionType.RemoveFollower:
                    PlaceFollower(response.Color.Value, response.Coords.Value, response.RegionId.Value);
                    RemoveRegions();
                    break;

                case GameActionType.PassMove:
                    RemoveRegions();
                    break;
            }
        }

        /// <summary>
        /// Položí kartičku do mřížky.
        /// </summary>
        /// <param name="tileId">Id kartičky.</param>
        /// <param name="coords">Souřadnice kartičky.</param>
        /// <param name="orientation">Orientace kartičky.</param>
        protected virtual void PlaceTile(string tileId, Coords coords, TileOrientation orientation)
        {
            if (!int.TryParse(tileId, out int id) || id < 0 || id >= StandardTileSet.Count)
            {
                MessageBox.Show("Unexpected tile id.");
                return;
            }

            Dispatcher.Invoke(() =>
            {
                var bitmap = GraphicsProvider.GetTileGraphics(tileId);

                if (bitmap == null)
                {
                    MessageBox.Show("Cannot load image of tile: " + tileId);
                    return;
                }

                var image = new Image
                {
                    Source = bitmap,
                    Width = TileSize,
                    Height = TileSize
                };

                switch (orientation)
                {
                    case TileOrientation.N:
                        image.LayoutTransform = new RotateTransform(0, TileSize / 2, TileSize / 2);
                        break;

                    case TileOrientation.E:
                        image.LayoutTransform = new RotateTransform(90, TileSize / 2, TileSize / 2);
                        break;

                    case TileOrientation.S:
                        image.LayoutTransform = new RotateTransform(180, TileSize / 2, TileSize / 2);
                        break;

                    case TileOrientation.W:
                        image.LayoutTransform = new RotateTransform(-90, TileSize / 2, TileSize / 2);
                        break;
                }

                var info = new PlacedTileInfo
                {
                    Coords = coords,
                    Orientation = orientation,
                    Scheme = StandardTileSet[id].Copy(),
                    Graphics = image
                };

                PlacedTiles.Add(coords, info);

                image.MouseDown += (o, ev) => CoordsSelected.Invoke(coords);

                Canvas.SetLeft(image, coords.X * TileSize);
                Canvas.SetTop(image, coords.Y * TileSize);
                Panel.SetZIndex(image, 4);

                Grid.Children.Add(image);

                FixEmptyTiles(coords);
            });
        }

        /// <summary>
        /// Umístí figurku do mřížky.
        /// </summary>
        /// <param name="color">Barva figurky.</param>
        /// <param name="coords">Souřadnice figurky.</param>
        /// <param name="regionId">Identifikátor regionu.</param>
        protected virtual void PlaceFollower(PlayerColor color, Coords coords, int regionId)
        {
            if (!PlacedTiles.TryGetValue(coords, out var placedTileInfo))
            {
                return;
            }

            Dispatcher.Invoke(() =>
            {
                var image = GraphicsProvider.GetFollowerGraphics(color, TileSize);

                var borders = placedTileInfo.Scheme.GetRegionBorders(regionId);

                var info = new PlacedFollowerInfo
                {
                    Color = color,
                    Coords = coords,
                    RegionId = regionId,
                    Position = TileOrientation.None,
                    Graphics = image
                };

                foreach (var border in CardinalDirections)
                {
                    if ((borders & border) != TileOrientation.None)
                    {
                        info.Position = border.Rotate(placedTileInfo.Orientation);
                        break;
                    }
                }

                PlacedFollowers.Add(coords, info);

                image.MouseDown += (o, ev) => FollowerSelected.Invoke(color, coords, regionId);

                var offset = GetOffsetForPosition(info.Position);
                Canvas.SetLeft(image, (coords.X + offset.X) * TileSize);
                Canvas.SetTop(image, (coords.Y + offset.Y) * TileSize);
                Panel.SetZIndex(image, 5);

                Grid.Children.Add(image);
            });
        }

        /// <summary>
        /// Odstraní figurku z mřížky.
        /// </summary>
        /// <param name="coords">Souřadnice figurky.</param>
        protected virtual void RemoveFollower(Coords coords)
        {
            if (!PlacedFollowers.TryGetValue(coords, out var info))
            {
                return;
            }

            Dispatcher.Invoke(() =>
            {
                Grid.Children.Remove(info.Graphics);

                PlacedFollowers.Remove(coords);
            });
        }

        /// <summary>
        /// Odstraní kartičku z mřížky.
        /// </summary>
        /// <param name="coords">Souřadnice kartičky.</param>
        protected virtual void RemoveTile(Coords coords)
        {
            if (!PlacedTiles.TryGetValue(coords, out var info))
            {
                return;
            }

            Dispatcher.Invoke(() =>
            {
                Grid.Children.Remove(info.Graphics);

                PlacedTiles.Remove(coords);

                FixEmptyTiles(coords);
            });
        }

        /// <summary>
        /// Opraví prázdná místa kolem umístěných kartiček.
        /// </summary>
        /// <param name="coords">Souřadnice poslední změny.</param>
        protected virtual void FixEmptyTiles(Coords coords)
        {
            if (PlacedTiles.ContainsKey(coords))
            {
                // Smazat zde
                RemoveEmptyTile(coords);

                // Přidat na okolní, pokud je to vhodné
                foreach (var dir in CardinalDirections)
                {
                    PlaceEmptyTile(coords.GetNeighboringCoords(dir));
                }
            }
            else
            {
                // Přidat zde
                PlaceEmptyTile(coords);

                // Aktualizovat na ostatních
                foreach (var nDir in CardinalDirections)
                {
                    var nCoords = coords.GetNeighboringCoords(nDir);

                    // Umístěné - není co mazat
                    if (PlacedTiles.ContainsKey(nCoords))
                    {
                        continue;
                    }

                    // U ostatních zkontrolujeme okolí
                    var remove = true;
                    foreach (var nnDir in CardinalDirections)
                    {
                        var nnCoords = nCoords.GetNeighboringCoords(nnDir);

                        // Má souseda, nemazat
                        if (PlacedTiles.ContainsKey(nCoords))
                        {
                            remove = false;
                            break;
                        }
                    }
                    if (remove)
                    {
                        RemoveEmptyTile(nCoords);
                    }

                }

            }
        }

        /// <summary>
        /// Přidá prázdné místo.
        /// </summary>
        /// <param name="coords">Souřadnice.</param>
        protected virtual void PlaceEmptyTile(Coords coords)
        {
            if (!PlacedTiles.ContainsKey(coords) && !EmptyTiles.ContainsKey(coords))
            {
                Dispatcher.Invoke(() =>
                {
                    var image = GraphicsProvider.GetEmptyTile(coords, TileSize);
                    image.Width = TileSize;
                    image.Height = TileSize;

                    EmptyTiles.Add(coords, new PlacedTileInfo
                    {
                        Coords = coords,
                        Graphics = image
                    });

                    image.MouseDown += (o, ev) => CoordsSelected.Invoke(coords);

                    Canvas.SetLeft(image, coords.X * TileSize);
                    Canvas.SetTop(image, coords.Y * TileSize);
                    Panel.SetZIndex(image, 3);

                    Grid.Children.Add(image);
                });
            }
        }

        /// <summary>
        /// Odstraní prázdné místo.
        /// </summary>
        /// <param name="coords">Souřadnice.</param>
        protected virtual void RemoveEmptyTile(Coords coords)
        {
            if (EmptyTiles.TryGetValue(coords, out var info))
            {
                EmptyTiles.Remove(coords);

                Dispatcher.Invoke(() =>
                {
                    Grid.Children.Remove(info.Graphics);
                });
            }
        }

        /// <summary>
        /// Umístí místa pro regiony.
        /// </summary>
        /// <param name="coords">Souřadnice.</param>
        protected virtual void PlaceRegions(Coords coords)
        {
            if (PlacedTiles.TryGetValue(coords, out var tileInfo))
            {
                Dispatcher.Invoke(() =>
                {
                    foreach (var position in CardinalDirections)
                    {
                        var image = GraphicsProvider.GetRegionPlaceGraphics(TileSize);

                        var regionId = tileInfo.Scheme.GetRegionOnBorder(position.Derotate(tileInfo.Orientation));

                        var info = new RegionPlaceInfo
                        {
                            Coords = coords,
                            Position = position,
                            RegionId = regionId,
                            Graphics = image
                        };

                        RegionPlaces.Add(info);

                        image.MouseDown += (o, ev) => RegionSelected.Invoke(coords, info.RegionId);

                        var offset = GetOffsetForPosition(info.Position);
                        Canvas.SetLeft(image, (coords.X + offset.X) * TileSize);
                        Canvas.SetTop(image, (coords.Y + offset.Y) * TileSize);
                        Panel.SetZIndex(image, 5);

                        Grid.Children.Add(image);
                    }
                });
            }
        }

        /// <summary>
        /// Odstraní místa pro regiony.
        /// </summary>
        protected virtual void RemoveRegions()
        {
            Dispatcher.Invoke(() =>
            {
                foreach (var place in RegionPlaces)
                {
                    Grid.Children.Remove(place.Graphics);
                }

                RegionPlaces.Clear();
            });
        }

        /// <summary>
        /// Vrátí offsety pozic figurek.
        /// </summary>
        /// <param name="position">Pozice figurky.</param>
        /// <returns>Offset.</returns>
        protected Point GetOffsetForPosition(TileOrientation position)
        {
            return position switch
            {
                TileOrientation.N => new Point(0.4, 0.0),
                TileOrientation.E => new Point(0.8, 0.4),
                TileOrientation.S => new Point(0.4, 0.8),
                TileOrientation.W => new Point(0.0, 0.4),
                _ => new Point(0.4, 0.4),
            };
        }

        #region Ovládání mřížky
        /// <summary>
        /// Předchozí bod při pousouvání
        /// </summary>
        private Point LastMovePoint;

        /// <summary>
        /// Pohybuje se s mřížkou?
        /// </summary>
        private bool MovingCanvas;

        /// <summary>
        /// Aktuální přiblížení
        /// </summary>
        private double Zoom;

        /// <summary>
        /// Vycentruje mřížku.
        /// </summary>
        private void CenterGrid()
        {
            Zoom = 50.0 / 480;
            Grid.LayoutTransform = new ScaleTransform(Zoom, Zoom);

            Canvas.SetLeft(Grid, Wrap.RenderSize.Width / 2);
            Canvas.SetTop(Grid, Wrap.RenderSize.Height / 2);
        }

        private void Wrap_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            LastMovePoint = e.GetPosition(this);
            MovingCanvas = true;
        }

        private void Wrap_MouseMove(object sender, MouseEventArgs e)
        {
            if (MovingCanvas)
            {
                var pt = e.GetPosition(this);
                var delta = pt - LastMovePoint;

                Canvas.SetLeft(Grid, Canvas.GetLeft(Grid) + delta.X);
                Canvas.SetTop(Grid, Canvas.GetTop(Grid) + delta.Y);

                LastMovePoint = pt;
            }
        }

        private void Wrap_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            MovingCanvas = false;
        }

        private void Wrap_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            Zoom *= Math.Pow(1.2, e.Delta / 40);
            Grid.LayoutTransform = new ScaleTransform(Zoom, Zoom);
        }
        #endregion

        /// <summary>
        /// Informace o umístěné kartičce.
        /// </summary>
        protected class PlacedTileInfo
        {
            /// <summary>
            /// Schéma kartičky.
            /// </summary>
            public ITileScheme Scheme { get; set; }

            /// <summary>
            /// Souřadnice kartičky.
            /// </summary>
            public Coords Coords { get; set; }

            /// <summary>
            /// Orientace kartičky.
            /// </summary>
            public TileOrientation Orientation { get; set; }

            /// <summary>
            /// GUI objekt reprezentující kartičku.
            /// </summary>
            public UIElement Graphics { get; set; }
        }

        /// <summary>
        /// Informace o umístěné figurce.
        /// </summary>
        protected class PlacedFollowerInfo
        {
            /// <summary>
            /// Barva figurky.
            /// </summary>
            public PlayerColor Color { get; set; }

            /// <summary>
            /// Souřadnice figurky.
            /// </summary>
            public Coords Coords { get; set; }

            /// <summary>
            /// Id regionu figurky.
            /// </summary>
            public int RegionId { get; set; }

            /// <summary>
            /// Umístění figurky na kartičce (vzhledem k absolutnímu směru).
            /// </summary>
            public TileOrientation Position { get; set; }

            /// <summary>
            /// GUI objekt reprezentující figurku.
            /// </summary>
            public UIElement Graphics { get; set; }
        }

        /// <summary>
        /// Informace o regionu.
        /// </summary>
        protected class RegionPlaceInfo
        {
            /// <summary>
            /// Souřadnice regionu.
            /// </summary>
            public Coords Coords { get; set; }

            /// <summary>
            /// Umístění regionu na kartičce.
            /// </summary>
            public TileOrientation Position { get; set; }

            /// <summary>
            /// Id regionu.
            /// </summary>
            public int RegionId { get; set; }

            /// <summary>
            /// GUI objekt reprezentující region.
            /// </summary>
            public UIElement Graphics { get; set; }
        }
    }
}
