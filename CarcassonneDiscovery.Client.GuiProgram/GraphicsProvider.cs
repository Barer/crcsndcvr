﻿namespace CarcassonneDiscovery.Client.GuiProgram
{
    using CarcassonneDiscovery.Entity;
    using CarcassonneDiscovery.StandardTileSet;
    using System.IO;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Sprostředkovatel obrázků.
    /// </summary>
    public static class GraphicsProvider
    {
        /// <summary>
        /// Výplň obrázku regionu.
        /// </summary>
        private readonly static Brush RegionPlaceFill = new SolidColorBrush(Color.FromArgb(128, 128, 128, 128));

        /// <summary>
        /// Vrátí obrázek kartičky.
        /// </summary>
        /// <param name="tileId">Identifikátor kartičky.</param>
        /// <param name="tileSize">Velikost strany kartičky.</param>
        /// <returns>Obrázek kartičky.</returns>
        public static BitmapImage GetTileGraphics(string tileId)
        {
            var resource = typeof(CardImages);
            var property = resource.GetProperty("t" + tileId, System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
            var graphics = (byte[])property.GetValue(null);

            if (graphics == null)
            {
                return null;
            }

            try
            {
                var stream = new MemoryStream();
                stream.Write(graphics, 0, graphics.Length);
                stream.Position = 0;

                var img = System.Drawing.Image.FromStream(stream);

                var returnImage = new BitmapImage();
                returnImage.BeginInit();
                var ms = new MemoryStream();
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                ms.Seek(0, SeekOrigin.Begin);
                returnImage.StreamSource = ms;
                returnImage.EndInit();

                return returnImage;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Vrátí obrázek prázdné kartičky.
        /// </summary>
        /// <param name="coords">Souřadnice prázdné kartičky.</param>
        /// <returns>Obrázek kartičky.</returns>
        public static FrameworkElement GetEmptyTile(Coords coords, double tileSize)
        {
            var panel = new DockPanel
            {
                Background = Brushes.LightGray
            };

            panel.Children.Add(new Label
            {
                Content = $"[{coords.X},{coords.Y}]",
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                HorizontalContentAlignment = HorizontalAlignment.Center,
                VerticalContentAlignment = VerticalAlignment.Center,
                FontSize = tileSize * 0.2
            });

            return panel;
        }

        /// <summary>
        /// Vrátí obrázek figurky.
        /// </summary>
        /// <param name="color">Barva figurky.</param>
        /// <param name="tileSize">Velikost strany kartičky.</param>
        /// <returns>Obrázek figurky.</returns>
        public static FrameworkElement GetFollowerGraphics(PlayerColor color, double tileSize)
        {
            Brush fill;

            switch (color)
            {
                case PlayerColor.Red:
                    fill = Brushes.Red;
                    break;
                case PlayerColor.Green:
                    fill = Brushes.Green;
                    break;
                case PlayerColor.Blue:
                    fill = Brushes.Blue;
                    break;
                case PlayerColor.Yellow:
                    fill = Brushes.Yellow;
                    break;
                case PlayerColor.Black:
                    fill = Brushes.Black;
                    break;
                default:
                    fill = Brushes.Gray;
                    break;
            }

            var followerSize = tileSize * 0.2;

            return new System.Windows.Shapes.Path
            {
                Data = new PathGeometry
                {
                    Figures = new PathFigureCollection
                    {
                        new PathFigure
                        {
                            StartPoint = new Point(0, followerSize),
                            Segments = new PathSegmentCollection
                            {
                                new LineSegment(new Point(followerSize, followerSize), true),
                                new LineSegment(new Point(followerSize / 2, 0), true)
                            },
                            IsClosed = true,
                            IsFilled = true
                        }
                    },
                    FillRule = FillRule.Nonzero
                },
                Stroke = (color == PlayerColor.Black) ? Brushes.White : Brushes.Black,
                Fill = fill
            };
        }

        /// <summary>
        /// Vrátí obrázek regionu na kartičce.
        /// </summary>
        /// <param name="tileSize">Velikost strany kartičky.</param>
        /// <returns>Obrázek regionu.</returns>
        public static FrameworkElement GetRegionPlaceGraphics(double tileSize)
        {
            return new System.Windows.Shapes.Ellipse
            {
                Height = tileSize * 0.2,
                Width = tileSize * 0.2,
                Fill = RegionPlaceFill
            };
        }
    }
}
