﻿namespace CarcassonneDiscovery.Client.GuiProgram
{
    using CarcassonneDiscovery.Api;
    using CarcassonneDiscovery.Entity;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;

    /// <summary>
    /// Panel pro ovládání hry.
    /// </summary>
    public partial class ControlPanel : UserControl
    {
        /// <summary>
        /// Uživatel žádá o umístění kartičky.
        /// </summary>
        public event Action<PlayerColor, string, Coords, TileOrientation> PlaceTileClicked = (color, tileId, coords, orientation) => { };

        /// <summary>
        /// Uživatel žádá o umístění figurky.
        /// </summary>
        public event Action<PlayerColor, Coords, int> PlaceFollowerClicked = (color, coords, regionId) => { };

        /// <summary>
        /// Uživatel žádá o odebrání figurky.
        /// </summary>
        public event Action<PlayerColor, Coords, int> RemoveFollowerClicked = (color, coords, regionId) => { };

        /// <summary>
        /// Uživatel žádá o předání tahu.
        /// </summary>
        public event Action<PlayerColor> PassMoveClicked = (color) => { };

        /// <summary>
        /// Uživatel žádá o aktualizaci detailu hry.
        /// </summary>
        public event Action RefreshGameDetailClicked = () => { };

        /// <summary>
        /// Uživatel žádá o připojení k jiné hře.
        /// </summary>
        public event Action<string, int> ChangeGameClicked = (baseUrl, gameId) => { };

        /// <summary>
        /// Konstruktor.
        /// </summary>
        public ControlPanel()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Nastaví zobrazované souřadnice.
        /// </summary>
        /// <param name="coords">Souřdnice.</param>
        public void SetCoords(Coords coords)
        {
            Dispatcher.InvokeAsync(() =>
            {
                CoordsXTextBox.Text = coords.X.ToString();
                CoordsYTextBox.Text = coords.Y.ToString();
            });
        }

        /// <summary>
        /// Nastaví parametry pro položení figurky.
        /// </summary>
        /// <param name="coords">Souřdnice.</param>
        /// <param name="regionId">Identifikátor regionu.</param>
        public void SetPlaceFollower(Coords coords, int regionId)
        {
            Dispatcher.InvokeAsync(() =>
            {
                ActionTypeComboBox.SelectedIndex = 1; 
                CoordsXTextBox.Text = coords.X.ToString();
                CoordsYTextBox.Text = coords.Y.ToString();
                RegionIdTextBox.Text = regionId.ToString();
            });
        }

        /// <summary>
        /// Nastaví parametry pro odebrání figurky.
        /// </summary>
        /// <param name="color">Barva figurky.</param>
        /// <param name="coords">Souřdnice.</param>
        /// <param name="regionId">Identifikátor regionu.</param>
        public void SetRemoveFollower(PlayerColor color, Coords coords, int regionId)
        {
            Dispatcher.InvokeAsync(() =>
            {
                ActionTypeComboBox.SelectedIndex = 2; 
                ColorComboBox.SelectedIndex = (int)color - 1; 
                CoordsXTextBox.Text = coords.X.ToString();
                CoordsYTextBox.Text = coords.Y.ToString();
                RegionIdTextBox.Text = regionId.ToString();
            });
        }

        /// <summary>
        /// Nastaví stav hry.
        /// </summary>
        /// <param name="response">Odpověď žádosti.</param>
        public void SetGameDetail(GameDetailResponse response)
        {
            Dispatcher.InvokeAsync(() =>
            {
                ChangeGameButton.IsEnabled = false;

                CurrentStateFinishedLabel.Content = response.Progress switch
                {
                    GameProgress.InProgress => "Stav: V průběhu",
                    GameProgress.Finished => "Stav: Ukončena",
                    GameProgress.Dead => "Stav: Nedohratelná",
                    _ => "Stav: Neznámý",
                };
                CurrentStateFollowerCountLabel.Content = $"Počet figurek: {response.FollowerCount}";
                CurrentStateTileSetIdLabel.Content = $"Kartičky: {response.TileSetId}";

                CurrentStatePlayersPanel.Children.Clear();

                foreach (var player in response.Players ?? new GamePlayerInfo[0])
                {
                    var playerLabel = new Label
                    {
                        Content = $"{player.Score} b. - {player.Color}({(int)player.Color}) - {player.Name ?? "(jméno neuvedeno)"}"
                    };

                    CurrentStatePlayersPanel.Children.Add(playerLabel);
                }
            });
        }

        /// <summary>
        /// Nastaví parametry po odehrání události.
        /// </summary>
        /// <param name="response">Odpověď žádosti.</param>
        public void SetGameAction(GameActionResponse response)
        {
            switch (response.Type)
            {
                case GameActionType.StartMove:
                    Dispatcher.InvokeAsync(() =>
                    {
                        var bitmap = GraphicsProvider.GetTileGraphics(response.TileId);

                        if (bitmap != null)
                        {
                            CurrentTileImage.Source = bitmap;
                        }

                        CurrentTileIdLabel.Content = "Id: " + response.TileId;
                        TileIdTextBox.Text = response.TileId;
                        ColorComboBox.SelectedIndex = (int)response.Color - 1; 
                        ActionTypeComboBox.SelectedIndex = 0; 
                    });
                    break;

                case GameActionType.PlaceTile:
                    Dispatcher.InvokeAsync(() =>
                    {
                        ActionTypeComboBox.SelectedIndex = 3; 
                        CoordsXTextBox.Text = response.CoordsX.ToString();
                        CoordsYTextBox.Text = response.CoordsY.ToString();
                    });
                    break;
            }
        }

        private void ChangeGameButton_Click(object sender, RoutedEventArgs e)
        {
            if (!int.TryParse(GameIdTextBox.Text, out int gameId))
            {
                MessageBox.Show("Špatný formát identifikátoru hry.");
                return;
            }

            ChangeGameClicked.Invoke(UrlTextBox.Text, gameId);
        }

        private void SendActionButton_Click(object sender, RoutedEventArgs e)
        {
            var errors = new List<string>();

            var color = SafeParsePlayerColor((string)((ComboBoxItem)ColorComboBox.SelectedItem).Content, errors);

            switch (ActionTypeComboBox.SelectedIndex) 
            {
                case 0:
                    var placeTileCoords = SafeParseCoords(CoordsXTextBox.Text, CoordsYTextBox.Text, errors);
                    var tileOrientation = SafeParseTileOrientation((string)((ComboBoxItem)TileOrientationComboBox.SelectedItem).Content, errors);
                    var tileId = TileIdTextBox.Text;

                    if (!errors.Any())
                    {
                        PlaceTileClicked(color, tileId, placeTileCoords, tileOrientation);
                    }
                    break;

                case 1:
                    var placeFollowerCoords = SafeParseCoords(CoordsXTextBox.Text, CoordsYTextBox.Text, errors);
                    var placeFollowerRegionId = SafeParseRegionId(RegionIdTextBox.Text, errors);

                    if (!errors.Any())
                    {
                        PlaceFollowerClicked(color, placeFollowerCoords, placeFollowerRegionId);
                    }
                    break;

                case 2:
                    var removeFollowerCoords = SafeParseCoords(CoordsXTextBox.Text, CoordsYTextBox.Text, errors);
                    var removeFollowerRegionId = SafeParseRegionId(RegionIdTextBox.Text, errors);

                    if (!errors.Any())
                    {
                        RemoveFollowerClicked(color, removeFollowerCoords, removeFollowerRegionId);
                    }
                    break;

                case 3:
                    if (!errors.Any())
                    {
                        PassMoveClicked.Invoke(color);
                    }
                    break;

                default:
                    errors.Add("Neznámá akce.");
                    break;
            }

            if (errors.Any())
            {
                MessageBox.Show(string.Join("\n", errors), "Chyba", MessageBoxButton.OK);
            }
        }

        #region Pomocné metody při parsování vstupu
        /// <summary>
        /// Načte hodnotu. Přidá chybovou hlášku, pokud je vstup neplatný.
        /// </summary>
        /// <param name="errors">Seznam chyb.</param>
        /// <param name="errorMsg">Chybová hláška.</param>
        /// <returns>Načtená hodnota. Výchozí hodnota, pokud je vstup neplatný.</returns>
        private Coords SafeParseCoords(string textX, string textY, List<string> errors, string errorMsg = "Špatný formát souřadnic.")
        {
            if (int.TryParse(textX, out int x) && int.TryParse(textY, out int y))
            {
                return new Coords(x, y);
            }

            errors.Add(errorMsg);

            return default;
        }

        /// <summary>
        /// Načte hodnotu. Přidá chybovou hlášku, pokud je vstup neplatný.
        /// </summary>
        /// <param name="text">Vstup.</param>
        /// <param name="errors">Seznam chyb.</param>
        /// <param name="errorMsg">Chybová hláška.</param>
        /// <returns>Načtená hodnota. Výchozí hodnota, pokud je vstup neplatný.</returns>
        private int SafeParseRegionId(string text, List<string> errors, string errorMsg = "Špatný formát identifikátoru regionu.")
        {
            if (int.TryParse(text, out int i) && i >= 0)
            {
                return i;
            }

            errors.Add(errorMsg);

            return default;
        }

        /// <summary>
        /// Načte hodnotu. Přidá chybovou hlášku, pokud je vstup neplatný.
        /// </summary>
        /// <param name="text">Vstup.</param>
        /// <param name="errors">Seznam chyb.</param>
        /// <param name="errorMsg">Chybová hláška.</param>
        /// <returns>Načtená hodnota. Výchozí hodnota, pokud je vstup neplatný.</returns>
        private TileOrientation SafeParseTileOrientation(string text, List<string> errors, string errorMsg = "Špatný formát orientace kartičky.")
        {
            switch (text)
            {
                case "N":
                    return TileOrientation.N;
                case "E":
                    return TileOrientation.E;
                case "S":
                    return TileOrientation.S;
                case "W":
                    return TileOrientation.W;
            }

            errors.Add(errorMsg);

            return default;
        }

        /// <summary>
        /// Načte hodnotu. Přidá chybovou hlášku, pokud je vstup neplatný.
        /// </summary>
        /// <param name="text">Vstup.</param>
        /// <param name="errors">Seznam chyb.</param>
        /// <param name="errorMsg">Chybová hláška.</param>
        /// <returns>Načtená hodnota. Výchozí hodnota, pokud je vstup neplatný.</returns>
        private PlayerColor SafeParsePlayerColor(string text, List<string> errors, string errorMsg = "Špatný formát barvy hráče.")
        {
            switch (text)
            {
                case "Red":
                    return PlayerColor.Red;
                case "Green":
                    return PlayerColor.Green;
                case "Blue":
                    return PlayerColor.Blue;
                case "Yellow":
                    return PlayerColor.Yellow;
                case "Black":
                    return PlayerColor.Black;
            }

            errors.Add(errorMsg);

            return default;
        }
        #endregion

        private void ActionTypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!IsInitialized)
            {
                return;
            }

            switch ((string)((ComboBoxItem)ActionTypeComboBox.SelectedItem).Content)
            {
                case "PlaceTile":
                    CoordsXTextBox.IsEnabled = true;
                    CoordsYTextBox.IsEnabled = true;
                    TileOrientationComboBox.IsEnabled = true;
                    RegionIdTextBox.IsEnabled = false;
                    TileIdTextBox.IsEnabled = true;
                    break;

                case "PlaceFollower":
                    CoordsXTextBox.IsEnabled = true;
                    CoordsYTextBox.IsEnabled = true;
                    TileOrientationComboBox.IsEnabled = false;
                    RegionIdTextBox.IsEnabled = true;
                    TileIdTextBox.IsEnabled = false;
                    break;

                case "RemoveFollower":
                    CoordsXTextBox.IsEnabled = true;
                    CoordsYTextBox.IsEnabled = true;
                    TileOrientationComboBox.IsEnabled = false;
                    RegionIdTextBox.IsEnabled = false;
                    TileIdTextBox.IsEnabled = false;
                    break;

                case "PassMove":
                    CoordsXTextBox.IsEnabled = false;
                    CoordsYTextBox.IsEnabled = false;
                    TileOrientationComboBox.IsEnabled = false;
                    RegionIdTextBox.IsEnabled = false;
                    TileIdTextBox.IsEnabled = false;
                    break;
            }
        }

        private void CurrentStateRefreshButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshGameDetailClicked();
        }

        private void RotateLeftButton_Click(object sender, RoutedEventArgs e)
        {
            TileOrientationComboBox.SelectedIndex = (TileOrientationComboBox.SelectedIndex + 3) % 4;
        }

        private void RotateRightButton_Click(object sender, RoutedEventArgs e)
        {
            TileOrientationComboBox.SelectedIndex = (TileOrientationComboBox.SelectedIndex + 1) % 4;
        }

        private void TileOrientationComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CurrentTileImage?.Source != null)
            {
                switch (SafeParseTileOrientation((string)((ComboBoxItem)TileOrientationComboBox.SelectedItem).Content, new List<string>()))
                {
                    case TileOrientation.N:
                        CurrentTileImage.LayoutTransform = new RotateTransform(0, CurrentTileImage.ActualWidth / 2, CurrentTileImage.ActualHeight / 2);
                        break;

                    case TileOrientation.E:
                        CurrentTileImage.LayoutTransform = new RotateTransform(90, CurrentTileImage.ActualWidth / 2, CurrentTileImage.ActualHeight / 2);
                        break;

                    case TileOrientation.S:
                        CurrentTileImage.LayoutTransform = new RotateTransform(180, CurrentTileImage.ActualWidth / 2, CurrentTileImage.ActualHeight / 2);
                        break;

                    case TileOrientation.W:
                        CurrentTileImage.LayoutTransform = new RotateTransform(270, CurrentTileImage.ActualWidth / 2, CurrentTileImage.ActualHeight / 2);
                        break;

                    default:
                        break;
                }
            }
        }

        #region Ovládání přehrávání tahů
        /// <summary>
        /// Změna zobrazení tahu.
        /// </summary>
        public event Action<int> GoToMoveClicked = (moveDirection) => { };

        /// <summary>
        /// Změna módu přehrávání.
        /// </summary>
        public event Action<bool> CurrentMoveModeChanged = (currentMoveEnabled) => { };

        /// <summary>
        /// Získá aktuální mód přehrávání.
        /// </summary>
        public bool CurrentMoveModeEnabled => CurrentMoveModeCheckBox.IsChecked ?? false;

        /// <summary>
        /// Změní zobrazení tačítek na ovládání tahů.
        /// </summary>
        /// <param name="pastMovesEnabled">Lze zobrazit tahy v minulosti?</param>
        /// <param name="futureMovesEnabled">Lze zobrazit tahy v budoucnosti?</param>
        /// <param name="currentMoveEnabled">Běží mód přehrávání aktuálních tahů?</param>
        public void SetMoveButtons(bool pastMovesEnabled, bool futureMovesEnabled, bool currentMoveEnabled)
        {
            Dispatcher.Invoke(() =>
            {
                FirstMoveButton.IsEnabled = pastMovesEnabled;
                PrevMoveButton.IsEnabled = pastMovesEnabled;
                NextMoveButton.IsEnabled = futureMovesEnabled;
                LastMoveButton.IsEnabled = futureMovesEnabled;
                CurrentMoveModeCheckBox.IsChecked = currentMoveEnabled;
            });
        }

        private void FirstMoveButton_Click(object sender, RoutedEventArgs e)
        {
            GoToMoveClicked.Invoke(-2);
        }

        private void PrevMoveButton_Click(object sender, RoutedEventArgs e)
        {
            GoToMoveClicked.Invoke(-1);
        }

        private void NextMoveButton_Click(object sender, RoutedEventArgs e)
        {
            GoToMoveClicked.Invoke(+1);
        }

        private void LastMoveButton_Click(object sender, RoutedEventArgs e)
        {
            GoToMoveClicked.Invoke(+2);
        }

        private void CurrentMoveButton_CheckedChanged(object sender, RoutedEventArgs e)
        {
            CurrentMoveModeChanged.Invoke(((CheckBox)sender).IsChecked ?? false);
        }
        #endregion
    }
}
