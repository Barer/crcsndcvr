﻿namespace CarcassonneDiscovery.Client.GuiProgram
{
    using CarcassonneDiscovery.Api;
    using CarcassonneDiscovery.Client.Base;
    using CarcassonneDiscovery.Entity;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// Hlavní okno GUI klienta.
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Služba pro komunikaci se serverem.
        /// </summary>
        protected ClientApiService Service;

        /// <summary>
        /// Identifikátor aktuální hry.
        /// </summary>
        protected int GameId;

        /// <summary>
        /// Vlákno se smyčkou pro získání herních akcí.
        /// </summary>
        protected Thread GameActionLoopThread;

        /// <summary>
        /// Správce tahů v aktuální hře.
        /// </summary>
        protected MoveOrganiser MoveOrganiser;


        public MainWindow()
        {
            InitializeComponent();

            GameBoard.CoordsSelected += GameBoard_CoordsSelected;
            GameBoard.RegionSelected += GameBoard_RegionSelected;
            GameBoard.FollowerSelected += GameBoard_FollowerSelected;

            ControlPanel.PlaceTileClicked += ControlPanel_PlaceTileClicked;
            ControlPanel.PlaceFollowerClicked += ControlPanel_PlaceFollowerClicked;
            ControlPanel.RemoveFollowerClicked += ControlPanel_RemoveFollowerClicked;
            ControlPanel.PassMoveClicked += ControlPanel_PassMoveClicked;
            ControlPanel.RefreshGameDetailClicked += ControlPanel_RefreshGameDetailClicked;
            ControlPanel.ChangeGameClicked += ControlPanel_ChangeGameClicked;
        }


        #region Obsluhy událostí GameBoard
        private void GameBoard_CoordsSelected(Coords coords)
        {
            ControlPanel.SetCoords(coords);
        }

        private void GameBoard_RegionSelected(Coords coords, int regionId)
        {
            ControlPanel.SetPlaceFollower(coords, regionId);
        }

        private void GameBoard_FollowerSelected(PlayerColor color, Coords coords, int regionId)
        {
            ControlPanel.SetRemoveFollower(color, coords, regionId);
        }
        #endregion

        #region Obsluhy událostí ControlPanel
        private async void ControlPanel_ChangeGameClicked(string baseUrl, int gameId)
        {
            var service = new ClientApiService($"{baseUrl.TrimEnd('/')}/Api/{{0}}/{{1}}");
            var request = new GameDetailRequest
            {
                GameId = gameId
            };

            GameDetailResponse response;

            try
            {
                response = await service.GameDetail(request);
            }
            catch
            {
                MessageBox.Show("Nelze se připojit ke hře.");
                return;
            }

            if (response.IsError)
            {
                MessageBox.Show("Chyba: " + response.ErrorMessage);
                return;
            }

            Service = service;
            GameId = gameId;
            MoveOrganiser = new MoveOrganiser(ControlPanel, GameBoard);

            GameBoard.Clear();

            ControlPanel.SetGameDetail(response);

            if (GameActionLoopThread != null)
            {
                GameActionLoopThread.Abort();
            }

            GameActionLoopThread = new Thread(() => GameActionLoop(gameId));
            GameActionLoopThread.Start();
        }

        private async void ControlPanel_RefreshGameDetailClicked()
        {
            if (Service == null)
            {
                MessageBox.Show("Žádná hra není v průběhu.");
                return;
            }

            var request = new GameDetailRequest
            {
                GameId = GameId
            };

            try
            {
                var response = await Service.GameDetail(request);

                if (response == null)
                {
                    MessageBox.Show("Chyba při odeslání žádosti.");
                }
                else if (response.IsError)
                {
                    MessageBox.Show("Chyba: " + response.ErrorMessage);
                }

                ControlPanel.SetGameDetail(response);
            }
            catch
            {
                MessageBox.Show("Chyba při odeslání žádosti.");
            }
        }

        private void ControlPanel_PassMoveClicked(PlayerColor color)
        {
            if (Service == null)
            {
                MessageBox.Show("Žádná hra není v průběhu.");
                return;
            }

            ResolveGameActionResponse(Service.PassMove(new PassMoveRequest
            {
                GameId = GameId,
                Color = color
            }));
        }

        private void ControlPanel_RemoveFollowerClicked(PlayerColor color, Coords coords, int regionId)
        {
            if (Service == null)
            {
                MessageBox.Show("Žádná hra není v průběhu.");
                return;
            }

            ResolveGameActionResponse(Service.RemoveFollower(new RemoveFollowerRequest
            {
                GameId = GameId,
                Color = color,
                Coords = coords,
                RegionId = regionId
            }));
        }

        private void ControlPanel_PlaceFollowerClicked(PlayerColor color, Coords coords, int regionId)
        {
            if (Service == null)
            {
                MessageBox.Show("Žádná hra není v průběhu.");
                return;
            }

            ResolveGameActionResponse(Service.PlaceFollower(new PlaceFollowerRequest
            {
                GameId = GameId,
                Color = color,
                Coords = coords,
                RegionId = regionId
            }));
        }

        private void ControlPanel_PlaceTileClicked(PlayerColor color, string tileId, Coords coords, TileOrientation orientation)
        {
            if (Service == null)
            {
                MessageBox.Show("Žádná hra není v průběhu.");
                return;
            }

            ResolveGameActionResponse(Service.PlaceTile(new PlaceTileRequest
            {
                GameId = GameId,
                Color = color,
                TileId = tileId,
                Coords = coords,
                Orientation = orientation
            }));
        }
        #endregion

        /// <summary>
        /// Práce smyčky pro získávání herních akcí.
        /// </summary>
        protected void GameActionLoop(int gameId)
        {
            var looper = new GameActionLooper(Service);

            foreach (var gameAction in looper.Run(gameId, 0))
            {
                MoveOrganiser.AddMove(gameAction);
                ControlPanel.SetGameAction(gameAction);

                ControlPanel_RefreshGameDetailClicked();
            }

            MessageBox.Show("Konec hry", "Konec hry", MessageBoxButton.OK);
        }

        /// <summary>
        /// Vyřídí odpověď na žádost o herní akci.
        /// </summary>
        /// <param name="responseTask">Task pro získání odpovědi.</param>
        protected async void ResolveGameActionResponse(Task<ResponseBase> responseTask)
        {
            try
            {
                var response = await responseTask;

                if (response == null)
                {
                    MessageBox.Show("Chyba při odeslání žádosti.");
                }
                else if (response.IsError)
                {
                    MessageBox.Show("Chyba: " + response.ErrorMessage);
                }
            }
            catch
            {
                MessageBox.Show("Chyba při odeslání žádosti.");
            }
        }
    }
}
