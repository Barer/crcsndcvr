﻿using CarcassonneDiscovery.Api;
using System.Collections.Generic;

namespace CarcassonneDiscovery.Client.GuiProgram
{
    /// <summary>
    /// Zajištění přehrávání tahů.
    /// </summary>
    public class MoveOrganiser
    {
        /// <summary>
        /// Panel pro ovládání hry.
        /// </summary>
        protected ControlPanel ControlPanel;

        /// <summary>
        /// Herní deska.
        /// </summary>
        protected GameBoard GameBoard;

        /// <summary>
        /// Aktuálně zobrazený tah.
        /// </summary>
        protected int CurrentMoveIndex;

        /// <summary>
        /// Je spuštěn mód zobrazení aktuálního tahu?
        /// </summary>
        protected bool CurrentMoveModeEnabled;

        /// <summary>
        /// Seznam tahů.
        /// </summary>
        protected IList<GameActionResponse> Moves;

        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="controlPanel">Panel pro ovládání hry.</param>
        /// <param name="gameBoard">Herní deska.</param>
        public MoveOrganiser(ControlPanel controlPanel, GameBoard gameBoard)
        {
            ControlPanel = controlPanel;
            GameBoard = gameBoard;

            controlPanel.GoToMoveClicked += GoToMove;
            controlPanel.CurrentMoveModeChanged += CurrentMoveModeChanged;

            CurrentMoveIndex = -1;
            CurrentMoveModeEnabled = controlPanel.CurrentMoveModeEnabled;
            Moves = new List<GameActionResponse>();
        }

        /// <summary>
        /// Přidá tah do 
        /// </summary>
        /// <param name="gameActionResponse">Odpověď s provedením tahu.</param>
        public void AddMove(GameActionResponse gameActionResponse)
        {
            lock(Moves)
            {
                Moves.Add(gameActionResponse);

                if (CurrentMoveModeEnabled)
                {
                    GoToMove(+2);
                }
            }
        }

        private void CurrentMoveModeChanged(bool currentMoveModeEnbled)
        {
            CurrentMoveModeEnabled = currentMoveModeEnbled;

            if (CurrentMoveModeEnabled)
            {
                GoToMove(+2);
            }
        }

        private void GoToMove(int moveDirection)
        {
            lock(Moves)
            {
                // Rozdělení podle minulosti a budoucnosti
                if (moveDirection < 0)
                {
                    var final = (moveDirection == -1) ? CurrentMoveIndex - 1 : -1;
                    if (final >= -1)
                    {
                        // Revertujeme od aktuálního
                        for (var i = CurrentMoveIndex; i > final; i--)
                        {
                            GameBoard.RevertAction(Moves[i]);
                        }

                        CurrentMoveIndex = final;
                    }
                }
                else if (moveDirection > 0)
                {
                    var final = (moveDirection == +1) ? CurrentMoveIndex + 1 : Moves.Count - 1;
                    if (final < Moves.Count)
                    {
                        // Aplikujeme od následujícího
                        for (var i = CurrentMoveIndex + 1; i <= final; i++)
                        {
                            GameBoard.ShowAction(Moves[i]);
                        }

                        CurrentMoveIndex = final;
                    }
                }

                ControlPanel.SetMoveButtons(CurrentMoveIndex > -1, CurrentMoveIndex < Moves.Count - 1, CurrentMoveModeEnabled);
            }
        }
    }
}
