﻿namespace CarcassonneDiscovery.Api
{
    /// <summary>
    /// Výsedek žádosti o zahájení nové hry.
    /// </summary>
    public class StartGameResponse : ResponseBase
    {
        /// <summary>
        /// Identifikátor hry.
        /// </summary>
        public int GameId { get; set; }
    }
}
