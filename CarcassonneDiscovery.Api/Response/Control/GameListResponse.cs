﻿namespace CarcassonneDiscovery.Api
{
    using System.Collections.Generic;

    /// <summary>
    /// Výsedek žádosti o seznam her.
    /// </summary>
    public class GameListResponse : ResponseBase
    {
        /// <summary>
        /// Identifikátory her v průběhu.
        /// </summary>
        public IList<int> InProgressIds { get; set; }

        /// <summary>
        /// Identifikátory ukončených her.
        /// </summary>
        public IList<int> FinishedIds { get; set; }
    }
}
