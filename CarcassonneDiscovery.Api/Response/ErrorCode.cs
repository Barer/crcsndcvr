﻿namespace CarcassonneDiscovery.Api
{
    /// <summary>
    /// Číselník chybových stavů s jejich kódy.
    /// </summary>
    public enum ErrorCode
    {
        /// <summary>
        /// Nedošlo k chybě.
        /// </summary>
        Ok,

        #region Společné
        /// <summary>
        /// Chybné parametry žádosti.
        /// </summary>
        InvalidParameters,

        /// <summary>
        /// Hra je již ukončena
        /// </summary>
        GameAlreadyFinished,
        #endregion

        #region Control API
        /// <summary>
        /// Příliš mnoho her v průběhu.
        /// </summary>
        TooManyGamesInProgress,
        #endregion

        #region Game API - Chyby herních úkonů
        /// <summary>
        /// Přeneseno z <see cref="ExecutionErrorCode"/>.
        /// </summary>
        AlreadyOccupied,

        /// <summary>
        /// Přeneseno z <see cref="ExecutionErrorCode"/>.
        /// </summary>
        InvalidGameAction,

        /// <summary>
        /// Přeneseno z <see cref="ExecutionErrorCode"/>.
        /// </summary>
        InvalidPlayerOnMove,

        /// <summary>
        /// Přeneseno z <see cref="ExecutionErrorCode"/>.
        /// </summary>
        NoFollowerLeft,

        /// <summary>
        /// Přeneseno z <see cref="ExecutionErrorCode"/>.
        /// </summary>
        NoFollowerOnCoords,

        /// <summary>
        /// Přeneseno z <see cref="ExecutionErrorCode"/>.
        /// </summary>
        NoPlaceableTileRemaining,

        /// <summary>
        /// Přeneseno z <see cref="ExecutionErrorCode"/>.
        /// </summary>
        NoSurroundings,

        /// <summary>
        /// Přeneseno z <see cref="ExecutionErrorCode"/>.
        /// </summary>
        NotEmptyCoords,

        /// <summary>
        /// Přeneseno z <see cref="ExecutionErrorCode"/>.
        /// </summary>
        NoTileRemaining,

        /// <summary>
        /// Přeneseno z <see cref="ExecutionErrorCode"/>.
        /// </summary>
        NotOwnFollower,

        /// <summary>
        /// Přeneseno z <see cref="ExecutionErrorCode"/>.
        /// </summary>
        NotSameCoords,

        /// <summary>
        /// Přeneseno z <see cref="ExecutionErrorCode"/>.
        /// </summary>
        NotSameTile,

        /// <summary>
        /// Přeneseno z <see cref="ExecutionErrorCode"/>.
        /// </summary>
        TileRemaining,

        /// <summary>
        /// Přeneseno z <see cref="ExecutionErrorCode"/>.
        /// </summary>
        WrongSurroundings,
        #endregion

        #region Game API - Ostatní
        /// <summary>
        /// Příliš vysoký identifikátor akce.
        /// </summary>
        TooHighGameActionId,

        /// <summary>
        /// Příliš douhé čekání na akci.
        /// </summary>
        WaitTimeout,

        /// <summary>
        /// Neočekávaná chyba.
        /// </summary>
        UnexpectedTimeout,
        #endregion
    }
}
