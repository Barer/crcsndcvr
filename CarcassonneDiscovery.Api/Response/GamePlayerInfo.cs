﻿namespace CarcassonneDiscovery.Api
{
    using CarcassonneDiscovery.Entity;

    /// <summary>
    /// Informace o hráči ve hře.
    /// </summary>
    public struct GamePlayerInfo
    {
        /// <summary>
        /// Pořadí hráče ve hře.
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// Jméno hráče.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Barva hráče.
        /// </summary>
        public PlayerColor Color { get; set; }

        /// <summary>
        /// Skóre hráče.
        /// </summary>
        public int Score { get; set; }
    }
}
