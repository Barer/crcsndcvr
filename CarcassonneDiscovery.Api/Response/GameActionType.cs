﻿namespace CarcassonneDiscovery.Api
{
    /// <summary>
    /// Typ herní akce.
    /// </summary>
    public enum GameActionType
    {
        /// <summary>
        /// Zahájení hry.
        /// </summary>
        StartGame,

        /// <summary>
        /// Zahájení tahu.
        /// </summary>
        StartMove,

        /// <summary>
        /// Položení kartičky.
        /// </summary>
        PlaceTile,

        /// <summary>
        /// Položení figurky.
        /// </summary>
        PlaceFollower,

        /// <summary>
        /// Odebrání figurky.
        /// </summary>
        RemoveFollower,

        /// <summary>
        /// Předání tahu.
        /// </summary>
        PassMove,

        /// <summary>
        /// Ukončení hry.
        /// </summary>
        GameEnd,

        /// <summary>
        /// Ukončení hry předčasně.
        /// </summary>
        GameQuit
    }
}
