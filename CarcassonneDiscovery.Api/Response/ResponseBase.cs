﻿namespace CarcassonneDiscovery.Api
{
    using System.Text.Json.Serialization;

    /// <summary>
    /// Základní třída pro odpovědi na žádosti.
    /// </summary>
    public class ResponseBase
    {
        /// <summary>
        /// Kód chyby.
        /// </summary>
        public ErrorCode ErrorCode { get; set; }

        /// <summary>
        /// Chybová hláška.
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Je odpověď chybou?
        /// </summary>
        [JsonIgnore]
        public bool IsError => ErrorCode != ErrorCode.Ok;
    }
}
