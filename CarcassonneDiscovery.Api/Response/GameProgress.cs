﻿namespace CarcassonneDiscovery.Api
{
    /// <summary>
    /// Stav průběhu hry.
    /// </summary>
    public enum GameProgress
    {
        /// <summary>
        /// Hra je v průběhu.
        /// </summary>
        InProgress,

        /// <summary>
        /// Hra je ukončena.
        /// </summary>
        Finished,

        /// <summary>
        /// Hra nemůže být dohrána.
        /// </summary>
        Dead,

        /// <summary>
        /// Jiný, neznámý stav.
        /// </summary>
        Other
    }
}
