﻿namespace CarcassonneDiscovery.Api
{
    using System.Collections.Generic;

    /// <summary>
    /// Výsledek žádosti o detail hry.
    /// </summary>
    public class GameDetailResponse : ResponseBase
    {
        /// <summary>
        /// Stav hry.
        /// </summary>
        public GameProgress Progress { get; set; }

        /// <summary>
        /// Identifikátor sady kartiček.
        /// </summary>
        public string TileSetId { get; set; }

        /// <summary>
        /// Počet figurek každého hráče.
        /// </summary>
        public int FollowerCount { get; set; }

        /// <summary>
        /// Informace o hráčích.
        /// </summary>
        public IList<GamePlayerInfo> Players { get; set; }
    }
}
