﻿namespace CarcassonneDiscovery.Api
{
    using CarcassonneDiscovery.Entity;
    using System.Text.Json.Serialization;

    /// <summary>
    /// Výsledek žádosti o výsledek herní akce.
    /// </summary>
    public class GameActionResponse : ResponseBase
    {
        /// <summary>
        /// Typ akce.
        /// </summary>
        public GameActionType Type { get; set; }

        /// <summary>
        /// Barva hráče na tahu.
        /// </summary>
        public PlayerColor? Color { get; set; }

        /// <summary>
        /// X-souřadnice kartičky nebo figurky.
        /// </summary>
        public int? CoordsX { get; set; }

        /// <summary>
        /// Y-souřadnice kartičky nebo figurky.
        /// </summary>
        public int? CoordsY { get; set; }

        /// <summary>
        /// Souřadnice kartičky nebo figurky.
        /// </summary>
        [JsonIgnore]
        public Coords? Coords
        {
            get
            {
                if (CoordsX.HasValue && CoordsY.HasValue)
                {
                    return new Coords(CoordsX.Value, CoordsY.Value);
                }

                return null;
            }

            set
            {
                CoordsX = value?.X;
                CoordsY = value?.Y;
            }
        }

        /// <summary>
        /// Id kartičky.
        /// </summary>
        public string TileId { get; set; }

        /// <summary>
        /// Orientace kartičky.
        /// </summary>
        public TileOrientation? TileOrientation { get; set; }

        /// <summary>
        /// Id regionu na kartičce.
        /// </summary>
        public int? RegionId { get; set; }

        /// <summary>
        /// Skóre za tah.
        /// </summary>
        public int? Score { get; set; }
    }
}
