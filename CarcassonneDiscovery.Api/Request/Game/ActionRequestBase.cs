﻿namespace CarcassonneDiscovery.Api
{
    using CarcassonneDiscovery.Entity;

    /// <summary>
    /// Základní třída pro žádosti o herní akce.
    /// </summary>
    public abstract class ActionRequestBase : IGameIdRequest
    {
        /// <summary>
        /// Identifikátor hry.
        /// </summary>
        public int GameId { get; set; }

        /// <summary>
        /// Barva hráče provádějícího tah.
        /// </summary>
        public PlayerColor Color { get; set; }
    }
}
