﻿namespace CarcassonneDiscovery.Api
{
    /// <summary>
    /// Žádost o výsledek herní akce.
    /// </summary>
    public class GameActionRequest : IGameIdRequest
    {
        /// <summary>
        /// Identifikátor hry.
        /// </summary>
        public int GameId { get; set; }

        /// <summary>
        /// Pořadové číslo akce.
        /// </summary>
        public int ActionId { get; set; }
    }
}
