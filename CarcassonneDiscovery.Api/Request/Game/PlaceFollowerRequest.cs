﻿namespace CarcassonneDiscovery.Api
{
    using CarcassonneDiscovery.Entity;
    using System.Text.Json.Serialization;

    /// <summary>
    /// Parametry položení figurky.
    /// </summary>
    public class PlaceFollowerRequest : ActionRequestBase
    {
        /// <summary>
        /// Identifikátor regionu na kartičce.
        /// </summary>
        public int RegionId { get; set; }

        /// <summary>
        /// X-souřadnice kartičky, na kterém se region nachází.
        /// </summary>
        public int CoordsX { get; set; }

        /// <summary>
        /// Y-souřadnice kartičky, na kterém se region nachází.
        /// </summary>
        public int CoordsY { get; set; }

        /// <summary>
        /// Souřadnice kartičky, na kterém se region nachází.
        /// </summary>
        [JsonIgnore]
        public Coords Coords
        {
            get
            {
                return new Coords(CoordsX, CoordsY);
            }

            set
            {
                CoordsX = value.X;
                CoordsY = value.Y;
            }
        }
    }
}
