﻿namespace CarcassonneDiscovery.Api
{
    using CarcassonneDiscovery.Entity;
    using System.Text.Json.Serialization;

    /// <summary>
    /// Parametry položení kartičky.
    /// </summary>
    public class PlaceTileRequest : ActionRequestBase
    {
        /// <summary>
        /// Identifikátor kartičky.
        /// </summary>
        public string TileId { get; set; }

        /// <summary>
        /// X-souřadnice kartičky.
        /// </summary>
        public int CoordsX { get; set; }

        /// <summary>
        /// Y-souřadnice kartičky.
        /// </summary>
        public int CoordsY { get; set; }

        /// <summary>
        /// Souřadnice kartičky.
        /// </summary>
        [JsonIgnore]
        public Coords Coords
        {
            get
            {
                return new Coords(CoordsX, CoordsY);
            }

            set
            {
                CoordsX = value.X;
                CoordsY = value.Y;
            }
        }

        /// <summary>
        /// Orientace kartičky.
        /// </summary>
        public TileOrientation Orientation { get; set; }
    }
}
