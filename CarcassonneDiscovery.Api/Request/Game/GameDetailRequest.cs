﻿namespace CarcassonneDiscovery.Api
{
    /// <summary>
    /// Žádost o detail hry.
    /// </summary>
    public class GameDetailRequest : IGameIdRequest
    {
        /// <summary>
        /// Identifikátor hry.
        /// </summary>
        public int GameId { get; set; }
    }
}
