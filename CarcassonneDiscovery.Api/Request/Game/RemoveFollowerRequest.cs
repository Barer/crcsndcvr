﻿namespace CarcassonneDiscovery.Api
{
    using CarcassonneDiscovery.Entity;
    using System.Text.Json.Serialization;

    /// <summary>
    /// Parametry odebrání figurky.
    /// </summary>
    public class RemoveFollowerRequest : ActionRequestBase
    {
        /// <summary>
        /// Identifikátor regionu, na kterém je figurka umístěna.
        /// </summary>
        public int RegionId { get; set; }

        /// <summary>
        /// X-souřadnice kartičky, na které je figurka umístěna.
        /// </summary>
        public int CoordsX { get; set; }

        /// <summary>
        /// Y-souřadnice kartičky, na které je figurka umístěna.
        /// </summary>
        public int CoordsY { get; set; }
        /// <summary>
        /// Souřadnice kartičky, na které je figurka umístěna.
        /// </summary>
        [JsonIgnore]
        public Coords Coords
        {
            get
            {
                return new Coords(CoordsX, CoordsY);
            }

            set
            {
                CoordsX = value.X;
                CoordsY = value.Y;
            }
        }
    }
}
