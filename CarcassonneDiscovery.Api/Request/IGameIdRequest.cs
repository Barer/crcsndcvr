﻿namespace CarcassonneDiscovery.Api
{
    /// <summary>
    /// Žádost, která obsahuje identifikátor hry.
    /// </summary>
    public interface IGameIdRequest
    {
        /// <summary>
        /// Identifikátor hry.
        /// </summary>
        int GameId { get; set; }
    }
}
