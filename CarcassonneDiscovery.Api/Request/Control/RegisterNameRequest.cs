﻿namespace CarcassonneDiscovery.Api
{
    using CarcassonneDiscovery.Entity;

    /// <summary>
    /// Žádost o registraci jména hráče.
    /// </summary>
    public class RegisterNameRequest : IGameIdRequest
    {
        /// <summary>
        /// Identifikátor hry.
        /// </summary>
        public int GameId { get; set; }

        /// <summary>
        /// Barva hráče.
        /// </summary>
        public PlayerColor Color { get; set; }

        /// <summary>
        /// Jméno hráče.
        /// </summary>
        public string Name { get; set; }
    }
}
