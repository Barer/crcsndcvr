﻿namespace CarcassonneDiscovery.Api
{
    using CarcassonneDiscovery.Entity;

    /// <summary>
    /// Žádost o zahájení nové hry.
    /// </summary>
    public class StartGameRequest
    {
        /// <summary>
        /// Počet figurek každého hráče.
        /// </summary>
        public int FollowerCount { get; set; }

        /// <summary>
        /// Pořadí hráčů ve hře.
        /// </summary>
        public PlayerColor[] PlayerOrder { get; set; }

        /// <summary>
        /// Identifikátor sady kartiček.
        /// </summary>
        public string TileSetId { get; set; }
    }
}
