﻿namespace CarcassonneDiscovery.Api
{
    /// <summary>
    /// Žádost o ukončení hry.
    /// </summary>
    public class StopGameRequest : IGameIdRequest
    {
        /// <summary>
        /// Identifikátor hry.
        /// </summary>
        public int GameId { get; set; }
    }
}
