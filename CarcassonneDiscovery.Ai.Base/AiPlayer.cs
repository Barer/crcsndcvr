﻿using CarcassonneDiscovery.Client.Base;
using CarcassonneDiscovery.Communication;
using CarcassonneDiscovery.Entity;
using CarcassonneDiscovery.Execution;
using CarcassonneDiscovery.StandardTileSet;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CarcassonneDiscovery.Client.Ai
{
    /// <summary>
    /// Hrající umělá inteligence.
    /// </summary>
    public abstract class AiPlayer : IPlayer
    {
        /// <inheritdoc />
        public PlayerColor Color { get; protected set; }

        /// <summary>
        /// Jméno hráče.
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// Stav hry.
        /// </summary>
        protected GameState State;

        /// <summary>
        /// Fronta nezpracovaných akcí.
        /// </summary>
        /// <remarks>
        /// Funguje v případě, že stav hry nebyl zatím nastaven.
        /// </remarks>
        protected IList<GameActionResponse> Actions;

        /// <summary>
        /// Seznam kartiček ve standardní sadě.
        /// </summary>
        protected IList<ITileScheme> StandardTileSet;

        /// <summary>
        /// Zámek proti úpravám stavu hry.
        /// </summary>
        protected object Lock;

        /// <inheritdoc />
        public event Action<PlaceTileRequest> PlaceTile;

        /// <inheritdoc />
        public event Action<PlaceFollowerRequest> PlaceFollower;

        /// <inheritdoc />
        public event Action<RemoveFollowerRequest> RemoveFollower;

        /// <inheritdoc />
        public event Action<PassMoveRequest> PassMove;

        /// <inheritdoc />
        public event Action<RegisterNameRequest> RegisterPlayerName;

        /// <inheritdoc />
        public event Action<GameDetailRequest> GameDetail;

        /// <inheritdoc />
        public event Action<StopGameRequest> StopGame;

        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="color">Barva hráče.</param>
        /// <param name="name">Jméno hráče.</param>
        public AiPlayer(PlayerColor color, string name)
        {
            Color = color;
            Name = name;
            Lock = new object();

            StandardTileSet = StandardTileGenerator.GenerateStandardTileSet();
        }

        /// <summary>
        /// Provede první část tahu.
        /// </summary>
        protected abstract void MakeMoveTilePart();

        /// <summary>
        /// Provede druhou část tahu.
        /// </summary>
        protected abstract void MakeMoveFollowerPart();

        /// <summary>
        /// Zavolej událost <see cref="PlaceTile"/>.
        /// </summary>
        /// <param name="request">Parametry.</param>
        protected void PlaceTileInvoke(PlaceTileRequest request)
        {
            PlaceTile.Invoke(request);
        }

        /// <summary>
        /// Zavolej událost <see cref="PlaceFollower"/>.
        /// </summary>
        /// <param name="request">Parametry.</param>
        protected void PlaceFollowerInvoke(PlaceFollowerRequest request)
        {
            PlaceFollower.Invoke(request);
        }

        /// <summary>
        /// Zavolej událost <see cref="RemoveFollower"/>.
        /// </summary>
        /// <param name="request">Parametry.</param>
        protected void RemoveFollowerInvoke(RemoveFollowerRequest request)
        {
            RemoveFollower.Invoke(request);
        }

        /// <summary>
        /// Zavolej událost <see cref="PassMove"/>.
        /// </summary>
        /// <param name="request">Parametry.</param>
        protected void PassMoveInvoke(PassMoveRequest request)
        {
            PassMove.Invoke(request);
        }

        /// <summary>
        /// Zavolej událost <see cref="StopGame"/>.
        /// </summary>
        /// <param name="request">Parametry.</param>
        protected void StopGameInvoke(StopGameRequest request)
        {
            StopGame.Invoke(request);
        }

        /// <inheritdoc />
        public virtual void ActionResponse(GameActionResponse gameAction)
        {
            lock (Lock)
            {
                if (State == null)
                {
                    if (Actions == null)
                    {
                        throw new InvalidOperationException("Both State and Actions cannot be null.");
                    }

                    Actions.Add(gameAction);
                    return;
                }

                if (Actions != null)
                {
                    throw new InvalidOperationException("Both State and Actions cannot be not null.");
                }

                switch (gameAction.Type)
                {
                    case GameActionType.StartGame:
                        GameExecutor.SetStartGame(State, StandardTileSet[int.Parse(gameAction.TileId)], gameAction.Coords.Value, gameAction.TileOrientation.Value);
                        break;

                    case GameActionType.StartMove:
                        GameExecutor.SetStartMove(State, StandardTileSet[int.Parse(gameAction.TileId)], gameAction.Color.Value);
                        if (gameAction.Color.Value == Color)
                        {
                            MakeMoveTilePart();
                        }
                        break;

                    case GameActionType.PlaceTile:
                        GameExecutor.SetPlaceTile(State, gameAction.Color.Value, StandardTileSet[int.Parse(gameAction.TileId)], gameAction.Coords.Value, gameAction.TileOrientation.Value);
                        if (gameAction.Color.Value == Color)
                        {
                            MakeMoveFollowerPart();
                        }
                        break;

                    case GameActionType.PlaceFollower:
                        GameExecutor.SetPlaceFollower(State, gameAction.Color.Value, gameAction.Coords.Value, gameAction.RegionId.Value);
                        break;

                    case GameActionType.RemoveFollower:
                        GameExecutor.SetRemoveFollower(State, gameAction.Color.Value, gameAction.Coords.Value, gameAction.Score.Value);
                        break;

                    case GameActionType.PassMove:
                        GameExecutor.SetPassMove(State, gameAction.Color.Value);
                        break;

                    case GameActionType.GameEnd:
                        GameExecutor.SetEndGame(State, new List<RemoveFollowerExecutionResult>());
                        break;
                }
            }

        }

        /// <inheritdoc />
        public virtual void GameDetailResponse(GameDetailResponse response)
        {
            lock (Lock)
            {
                if (State == null)
                {
                    State = new GameState
                    {
                        Params = new GameParams
                        {
                            FollowerCount = response.FollowerCount,
                            TileSetId = response.TileSetId,
                            PlayerOrder = response.Players.OrderBy(x => x.Order).Select(x => x.Color).ToArray()
                        }
                    };

                    if (Actions != null)
                    {
                        var actions = Actions;

                        Actions = null;

                        foreach (var action in actions)
                        {
                            ActionResponse(action);
                        }
                    }
                }
                else
                {
                    // Neočekávaná žádost
                    Console.Error.WriteLine("Unexpected GameDetailResponse.");
                }
            }
        }

        /// <inheritdoc />
        public virtual void PlaceTileResponse(ResponseBase response)
        {
            lock (Lock)
            {
                if (response.IsError)
                {
                    RageQuit("PlaceTile request was incorrect: " + response.ErrorMessage);
                }
            }
        }

        /// <inheritdoc />
        public virtual void PlaceFollowerResponse(ResponseBase response)
        {
            lock (Lock)
            {
                if (response.IsError)
                {
                    RageQuit("PlaceFollower request was incorrect: " + response.ErrorMessage);
                }
            }
        }

        /// <inheritdoc />
        public virtual void RemoveFollowerResponse(ResponseBase response)
        {
            lock (Lock)
            {
                if (response.IsError)
                {
                    RageQuit("RemoveFollower request was incorrect: " + response.ErrorMessage);
                }
            }
        }

        /// <inheritdoc />
        public virtual void PassMoveResponse(ResponseBase response)
        {
            if (response.IsError)
            {
                RageQuit("PassMove request was incorrect: " + response.ErrorMessage);
            }
        }

        /// <inheritdoc />
        public virtual void RegisterPlayerNameResponse(ResponseBase response)
        {
            lock (Lock)
            {
                if (response.IsError)
                {
                    RageQuit("RegisterPlayerName request was incorrect: " + response.ErrorMessage);
                }
            }
        }

        /// <inheritdoc />
        public virtual void StopGameResponse(ResponseBase response)
        {
            lock (Lock)
            {
                if (response.IsError)
                {
                    RageQuit("RegisterPlayerName request was incorrect: " + response.ErrorMessage);
                }
            }
        }

        /// <summary>
        /// Ukončí hru pro nesprávném požadavku na herní akci.
        /// </summary>
        /// <param name="msg">Chybová hláška.</param>
        protected virtual void RageQuit(string msg)
        {
            Console.WriteLine("RageQuit: " + msg);
            StopGameInvoke(new StopGameRequest());
        }

        /// <inheritdoc />
        public virtual void Start()
        {
            lock (Lock)
            {
                Actions = new List<GameActionResponse>();

                RegisterPlayerName.Invoke(new RegisterNameRequest
                {
                    Name = Name
                });
                GameDetail.Invoke(new GameDetailRequest());
            }
        }

        /// <inheritdoc />
        public virtual void Stop()
        {
            lock (Lock)
            {
                State = null;
                Actions = null;
            }
        }
    }
}
