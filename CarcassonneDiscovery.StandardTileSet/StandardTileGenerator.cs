﻿namespace CarcassonneDiscovery.StandardTileSet
{
    using CarcassonneDiscovery.Entity;
    using CarcassonneDiscovery.Tools;
    using System.Collections.Generic;

    /// <summary>
    /// Generátor kartiček podle daných vzorů.
    /// </summary>
    public static class StandardTileGenerator
    {
        /// <summary>
        /// Generates stadard tile set.
        /// </summary>
        /// <returns>Standard tile set.</returns>
        public static IList<ITileScheme> GenerateStandardTileSet()
        {
            return new List<ITileScheme>
            {
                // 161(3)
                Generate161("0", RegionType.Grassland, RegionType.Mountain, RegionType.Sea, city12: true),
                Generate161("1", RegionType.Mountain, RegionType.Grassland, RegionType.Sea, city12: true),
                Generate161("2", RegionType.Mountain, RegionType.Sea, RegionType.Grassland, city12: true),
                // 8
                Generate8("3", RegionType.Mountain),
                Generate8("4", RegionType.Grassland),
                Generate8("5", RegionType.Sea),
                // 17
                Generate17("6", RegionType.Mountain, RegionType.Grassland),
                Generate17("7", RegionType.Mountain, RegionType.Grassland),
                Generate17("8", RegionType.Mountain, RegionType.Grassland),
                Generate17("9", RegionType.Mountain, RegionType.Sea, city01: true),
                Generate17("10", RegionType.Mountain, RegionType.Sea, city01: true),
                Generate17("11", RegionType.Mountain, RegionType.Sea),
                Generate17("12", RegionType.Grassland, RegionType.Mountain),
                Generate17("13", RegionType.Grassland, RegionType.Mountain),
                Generate17("14", RegionType.Grassland, RegionType.Mountain),
                Generate17("15", RegionType.Grassland, RegionType.Sea, city01: true),
                Generate17("16", RegionType.Grassland, RegionType.Sea, city01: true),
                Generate17("17", RegionType.Grassland, RegionType.Sea),
                Generate17("18", RegionType.Sea, RegionType.Mountain, city01: true),
                Generate17("19", RegionType.Sea, RegionType.Mountain, city01: true),
                Generate17("20", RegionType.Sea, RegionType.Mountain),
                Generate17("21", RegionType.Sea, RegionType.Grassland, city01: true),
                Generate17("22", RegionType.Sea, RegionType.Grassland, city01: true),
                Generate17("23", RegionType.Sea, RegionType.Grassland),
                // 5111(2)
                Generate5111("24", RegionType.Mountain, RegionType.Grassland, RegionType.Grassland, RegionType.Grassland),
                Generate5111("25", RegionType.Mountain, RegionType.Grassland, RegionType.Grassland, RegionType.Grassland),
                Generate5111("26", RegionType.Mountain, RegionType.Sea, RegionType.Sea, RegionType.Sea),
                Generate5111("27", RegionType.Mountain, RegionType.Sea, RegionType.Sea, RegionType.Sea),
                Generate5111("28", RegionType.Grassland, RegionType.Mountain, RegionType.Mountain, RegionType.Mountain),
                Generate5111("29", RegionType.Grassland, RegionType.Mountain, RegionType.Mountain, RegionType.Mountain),
                Generate5111("30", RegionType.Grassland, RegionType.Sea, RegionType.Sea, RegionType.Sea, city02: true),
                Generate5111("31", RegionType.Grassland, RegionType.Sea, RegionType.Sea, RegionType.Sea, city02: true),
                Generate5111("32", RegionType.Sea, RegionType.Mountain, RegionType.Mountain, RegionType.Mountain, city02: true),
                Generate5111("33", RegionType.Sea, RegionType.Mountain, RegionType.Mountain, RegionType.Mountain),
                Generate5111("34", RegionType.Sea, RegionType.Grassland, RegionType.Grassland, RegionType.Grassland, city02: true),
                Generate5111("35", RegionType.Sea, RegionType.Grassland, RegionType.Grassland, RegionType.Grassland),
                // 44
                Generate44("36", RegionType.Mountain, RegionType.Grassland),
                Generate44("37", RegionType.Mountain, RegionType.Grassland),
                Generate44("38", RegionType.Mountain, RegionType.Sea, city01: true),
                Generate44("39", RegionType.Mountain, RegionType.Sea),
                Generate44("40", RegionType.Grassland, RegionType.Sea, city01: true),
                Generate44("41", RegionType.Grassland, RegionType.Sea),
                // 116(2)
                Generate116("42", RegionType.Mountain, RegionType.Mountain, RegionType.Grassland),
                Generate116("43", RegionType.Mountain, RegionType.Mountain, RegionType.Grassland),
                Generate116("44", RegionType.Mountain, RegionType.Mountain, RegionType.Sea, city12: true),
                Generate116("45", RegionType.Mountain, RegionType.Mountain, RegionType.Sea),
                Generate116("46", RegionType.Grassland, RegionType.Grassland, RegionType.Mountain),
                Generate116("47", RegionType.Grassland, RegionType.Grassland, RegionType.Mountain),
                Generate116("48", RegionType.Grassland, RegionType.Grassland, RegionType.Sea, city02: true),
                Generate116("49", RegionType.Grassland, RegionType.Grassland, RegionType.Sea),
                Generate116("50", RegionType.Sea, RegionType.Sea, RegionType.Mountain, city02: true),
                Generate116("51", RegionType.Sea, RegionType.Sea, RegionType.Mountain),
                Generate116("52", RegionType.Sea, RegionType.Sea, RegionType.Grassland, city12: true),
                Generate116("53", RegionType.Sea, RegionType.Sea, RegionType.Grassland),
                // 11114
                Generate11114("54", RegionType.Mountain, RegionType.Mountain, RegionType.Grassland, RegionType.Grassland, RegionType.Sea),
                // 161(2)
                Generate161("55", RegionType.Mountain, RegionType.Grassland, RegionType.Mountain),
                Generate161("56", RegionType.Mountain, RegionType.Grassland, RegionType.Mountain),
                Generate161("57", RegionType.Mountain, RegionType.Sea, RegionType.Mountain, city01: true),
                Generate161("58", RegionType.Mountain, RegionType.Sea, RegionType.Mountain, city12: true),
                Generate161("59", RegionType.Grassland, RegionType.Mountain, RegionType.Grassland),
                Generate161("60", RegionType.Grassland, RegionType.Mountain, RegionType.Grassland),
                Generate161("61", RegionType.Grassland, RegionType.Sea, RegionType.Grassland, city01: true, city12: true),
                Generate161("62", RegionType.Grassland, RegionType.Sea, RegionType.Grassland, city12: true),
                Generate161("63", RegionType.Sea, RegionType.Mountain, RegionType.Sea, city12: true),
                Generate161("64", RegionType.Sea, RegionType.Mountain, RegionType.Sea),
                Generate161("65", RegionType.Sea, RegionType.Grassland, RegionType.Sea, city01: true),
                Generate161("66", RegionType.Sea, RegionType.Grassland, RegionType.Sea),
                // 5111(3)
                Generate5111("67", RegionType.Mountain, RegionType.Grassland, RegionType.Sea, RegionType.Sea),
                Generate5111("68", RegionType.Mountain, RegionType.Sea, RegionType.Sea, RegionType.Grassland),
                Generate5111("69", RegionType.Grassland, RegionType.Sea, RegionType.Mountain, RegionType.Sea, city01: true),
                Generate5111("70", RegionType.Sea, RegionType.Mountain, RegionType.Grassland, RegionType.Mountain, city02: true),
                Generate5111("71", RegionType.Sea, RegionType.Grassland, RegionType.Mountain, RegionType.Grassland, city02: true),
                // 422
                Generate422("72", RegionType.Mountain, RegionType.Grassland, RegionType.Sea, city12: true),
                Generate422("73", RegionType.Mountain, RegionType.Sea, RegionType.Grassland),
                Generate422("74", RegionType.Grassland, RegionType.Mountain, RegionType.Sea, city02: true),
                Generate422("75", RegionType.Grassland, RegionType.Sea, RegionType.Mountain, city01: true),
                Generate422("76", RegionType.Sea, RegionType.Mountain, RegionType.Grassland),
                Generate422("77", RegionType.Sea, RegionType.Grassland, RegionType.Mountain, city01: true),
                // 2231
                Generate2231("78", RegionType.Mountain, RegionType.Grassland, RegionType.Sea, RegionType.Mountain),
                Generate2231("79", RegionType.Grassland, RegionType.Mountain, RegionType.Sea, RegionType.Grassland),
                Generate2231("80", RegionType.Sea, RegionType.Mountain, RegionType.Grassland, RegionType.Sea),
                // 2213
                Generate2213("81", RegionType.Mountain, RegionType.Grassland, RegionType.Grassland, RegionType.Sea, city03: true),
                Generate2213("82", RegionType.Mountain, RegionType.Sea, RegionType.Sea, RegionType.Grassland),
                Generate2213("83", RegionType.Grassland, RegionType.Mountain, RegionType.Mountain, RegionType.Sea, city03: true),
            };
        }

        /// <summary>
        /// Vygeneruje základ pro kartičku.
        /// </summary>
        /// <param name="id">Identifikátor kartičky.</param>
        /// <param name="types">Typy regionů.</param>
        /// <returns></returns>
        private static TileScheme GenerateBase(string id, params RegionType[] types)
        {
            var scheme = new TileScheme()
            {
                Id = id,
                CityCount = 0,
                Regions = new RegionInfo[types.Length]
            };

            for (int i = 0; i < types.Length; i++)
            {
                scheme.Regions[i] = new RegionInfo
                {
                    Id = i,
                    Type = types[i],
                    Borders = TileOrientation.None,
                    NeighboringCities = new List<int>(),
                    NeighboringRegions = new List<int>()
                };
            }

            return scheme;
        }

        /// <summary>
        /// Nastaví regiony na okrajích.
        /// </summary>
        /// <param name="scheme">Polotovar kartičky.</param>
        /// <param name="nId">Identifikátor regionu na N okraji.</param>
        /// <param name="eId">Identifikátor regionu na E okraji.</param>
        /// <param name="sId">Identifikátor regionu na S okraji.</param>
        /// <param name="wId">Identifikátor regionu na W okraji.</param>
        /// <returns>Kartička s nastavenými okrajemi.</returns>
        private static TileScheme AddBorderRegions(this TileScheme scheme, int nId, int eId, int sId, int wId)
        {
            scheme.RegionsOnBorders = new int[4] { nId, eId, sId, wId };

            scheme.Regions[nId].Borders |= TileOrientation.N;
            scheme.Regions[eId].Borders |= TileOrientation.E;
            scheme.Regions[sId].Borders |= TileOrientation.S;
            scheme.Regions[wId].Borders |= TileOrientation.W;

            return scheme;
        }

        /// <summary>
        /// Nastaví sousednost regionů.
        /// </summary>
        /// <param name="scheme">Polotovar kartičky.</param>
        /// <param name="rId1">Identifikátor regionu.</param>
        /// <param name="rId2">Identifikátor regionu.</param>
        /// <param name="withCity">Nachází se mezi regiony.</param>
        /// <returns>Kartička s přidanými sousednosmi.</returns>
        private static TileScheme AddNeighbouring(this TileScheme scheme, int rId1, int rId2, bool withCity = false)
        {
            scheme.Regions[rId1].NeighboringRegions.Add(rId2);
            scheme.Regions[rId2].NeighboringRegions.Add(rId1);

            if (withCity)
            {
                var cityId = scheme.CityCount++;

                scheme.Regions[rId1].NeighboringCities.Add(cityId);
                scheme.Regions[rId2].NeighboringCities.Add(cityId);
            }

            return scheme;
        }

        /// <summary>
        /// Vygeneruje kartičku formátu "8".
        /// </summary>
        /// <param name="id">Id kartičky.</param>
        /// <param name="type0">Typ regionu 0.</param>
        /// <returns>Kartička.</returns>
        public static ITileScheme Generate8(string id,
            RegionType type0)
        {
            return GenerateBase(id, type0)
                .AddBorderRegions(0, 0, 0, 0);
        }

        /// <summary>
        /// Vygeneruje kartičku formátu "17".
        /// </summary>
        /// <param name="id">Id kartičky.</param>
        /// <param name="type0">Typ regionu 0.</param>
        /// <param name="type1">Typ regionu 1.</param>
        /// <param name="city01">Je mezi regiony 0 a 1 město?</param>
        /// <returns>Kartička.</returns>
        public static ITileScheme Generate17(string id,
            RegionType type0, RegionType type1,
            bool city01 = false)
        {
            return GenerateBase(id, type0, type1)
                .AddBorderRegions(0, 1, 1, 1)
                .AddNeighbouring(0, 1, city01);
        }

        /// <summary>
        /// Vygeneruje kartičku formátu "5111".
        /// </summary>
        /// <param name="id">Id kartičky.</param>
        /// <param name="type0">Typ regionu 0.</param>
        /// <param name="type1">Typ regionu 1.</param>
        /// <param name="type2">Typ regionu 2.</param>
        /// <param name="type3">Typ regionu 3.</param>
        /// <param name="city01">Je mezi regiony 0 a 1 město?</param>
        /// <param name="city02">Je mezi regiony 0 a 2 město?</param>
        /// <param name="city03">Je mezi regiony 0 a 3 město?</param>
        /// <returns>Kartička.</returns>
        public static ITileScheme Generate5111(string id,
            RegionType type0, RegionType type1, RegionType type2, RegionType type3,
            bool city01 = false, bool city02 = false, bool city03 = false)
        {
            return GenerateBase(id, type0, type1, type2, type3)
                .AddBorderRegions(0, 1, 2, 3)
                .AddNeighbouring(0, 1, city01)
                .AddNeighbouring(0, 2, city02)
                .AddNeighbouring(0, 3, city03);
        }

        /// <summary>
        /// Vygeneruje kartičku formátu "44".
        /// </summary>
        /// <param name="id">Id kartičky.</param>
        /// <param name="type0">Typ regionu 0.</param>
        /// <param name="type1">Typ regionu 1.</param>
        /// <param name="city01">Je mezi regiony 0 a 1 město?</param>
        /// <returns>Kartička.</returns>
        public static ITileScheme Generate44(string id,
            RegionType type0, RegionType type1,
            bool city01 = false)
        {
            return GenerateBase(id, type0, type1)
                .AddBorderRegions(0, 0, 1, 1)
                .AddNeighbouring(0, 1, city01);
        }

        /// <summary>
        /// Vygeneruje kartičku formátu "116".
        /// </summary>
        /// <param name="id">Id kartičky.</param>
        /// <param name="type0">Typ regionu 0.</param>
        /// <param name="type1">Typ regionu 1.</param>
        /// <param name="type2">Typ regionu 2.</param>
        /// <param name="city02">Je mezi regiony 0 a 2 město?</param>
        /// <param name="city12">Je mezi regiony 1 a 2 město?</param>
        /// <returns>Kartička.</returns>
        public static ITileScheme Generate116(string id,
            RegionType type0, RegionType type1, RegionType type2,
            bool city02 = false, bool city12 = false)
        {
            return GenerateBase(id, type0, type1, type2)
                .AddBorderRegions(0, 1, 2, 2)
                .AddNeighbouring(0, 2, city02)
                .AddNeighbouring(1, 2, city12);
        }

        /// <summary>
        /// Vygeneruje kartičku formátu "11114".
        /// </summary>
        /// <param name="id">Id kartičky.</param>
        /// <param name="type0">Typ regionu 0.</param>
        /// <param name="type1">Typ regionu 1.</param>
        /// <param name="type2">Typ regionu 2.</param>
        /// <param name="type3">Typ regionu 3.</param>
        /// <param name="type4">Typ regionu 4.</param>
        /// <param name="city04">Je mezi regiony 0 a 4 město?</param>
        /// <param name="city14">Je mezi regiony 1 a 4 město?</param>
        /// <param name="city24">Je mezi regiony 2 a 4 město?</param>
        /// <param name="city34">Je mezi regiony 3 a 4 město?</param>
        /// <returns>Kartička.</returns>
        public static ITileScheme Generate11114(string id,
            RegionType type0, RegionType type1, RegionType type2, RegionType type3, RegionType type4,
            bool city04 = false, bool city14 = false, bool city24 = false, bool city34 = false)
        {
            return GenerateBase(id, type0, type1, type2, type3, type4)
                .AddBorderRegions(0, 1, 2, 3)
                .AddNeighbouring(0, 4, city04)
                .AddNeighbouring(1, 4, city14)
                .AddNeighbouring(2, 4, city24)
                .AddNeighbouring(3, 4, city34);
        }

        /// <summary>
        /// Vygeneruje kartičku formátu "422".
        /// </summary>
        /// <param name="id">Id kartičky.</param>
        /// <param name="type0">Typ regionu 0.</param>
        /// <param name="type1">Typ regionu 1.</param>
        /// <param name="type2">Typ regionu 2.</param>
        /// <param name="city01">Je mezi regiony 0 a 1 město?</param>
        /// <param name="city02">Je mezi regiony 0 a 2 město?</param>
        /// <param name="city12">Je mezi regiony 1 a 2 město?</param>
        /// <returns>Kartička.</returns>
        public static ITileScheme Generate422(string id,
            RegionType type0, RegionType type1, RegionType type2,
            bool city01 = false, bool city02 = false, bool city12 = false)
        {
            return GenerateBase(id, type0, type1, type2)
                .AddBorderRegions(0, 0, 1, 2)
                .AddNeighbouring(0, 1, city01)
                .AddNeighbouring(0, 2, city02)
                .AddNeighbouring(1, 2, city12);
        }

        /// <summary>
        /// Vygeneruje kartičku formátu "2231".
        /// </summary>
        /// <param name="id">Id kartičky.</param>
        /// <param name="type0">Typ regionu 0.</param>
        /// <param name="type1">Typ regionu 1.</param>
        /// <param name="type2">Typ regionu 2.</param>
        /// <param name="type3">Typ regionu 3.</param>
        /// <param name="city01">Je mezi regiony 0 a 1 město?</param>
        /// <param name="city02">Je mezi regiony 0 a 2 město?</param>
        /// <param name="city12">Je mezi regiony 1 a 2 město?</param>
        /// <param name="city23">Je mezi regiony 2 a 3 město?</param>
        /// <returns>Kartička.</returns>
        public static ITileScheme Generate2231(string id,
            RegionType type0, RegionType type1, RegionType type2, RegionType type3,
            bool city01 = false, bool city02 = false, bool city12 = false, bool city23 = false)
        {
            return GenerateBase(id, type0, type1, type2, type3)
                .AddBorderRegions(0, 1, 2, 3)
                .AddNeighbouring(0, 1, city01)
                .AddNeighbouring(0, 2, city02)
                .AddNeighbouring(1, 2, city12)
                .AddNeighbouring(2, 3, city23);
        }

        /// <summary>
        /// Vygeneruje kartičku formátu "2213".
        /// </summary>
        /// <param name="id">Id kartičky.</param>
        /// <param name="type0">Typ regionu 0.</param>
        /// <param name="type1">Typ regionu 1.</param>
        /// <param name="type2">Typ regionu 2.</param>
        /// <param name="type3">Typ regionu 3.</param>
        /// <param name="city01">Je mezi regiony 0 a 1 město?</param>
        /// <param name="city03">Je mezi regiony 0 a 3 město?</param>
        /// <param name="city13">Je mezi regiony 1 a 3 město?</param>
        /// <param name="city23">Je mezi regiony 2 a 3 město?</param>
        /// <returns>Kartička.</returns>
        public static ITileScheme Generate2213(string id,
            RegionType type0, RegionType type1, RegionType type2, RegionType type3,
            bool city01 = false, bool city03 = false, bool city13 = false, bool city23 = false)
        {
            return GenerateBase(id, type0, type1, type2, type3)
                .AddBorderRegions(0, 1, 2, 3)
                .AddNeighbouring(0, 1, city01)
                .AddNeighbouring(0, 3, city03)
                .AddNeighbouring(1, 3, city13)
                .AddNeighbouring(2, 3, city23);
        }

        /// <summary>
        /// Vygeneruje kartičku formátu "161".
        /// </summary>
        /// <param name="id">Id kartičky.</param>
        /// <param name="type0">Typ regionu 0.</param>
        /// <param name="type1">Typ regionu 1.</param>
        /// <param name="type2">Typ regionu 2.</param>
        /// <param name="city01">Je mezi regiony 0 a 1 město?</param>
        /// <param name="city12">Je mezi regiony 1 a 2 město?</param>
        /// <returns>Kartička.</returns>
        public static ITileScheme Generate161(string id,
            RegionType type0, RegionType type1, RegionType type2,
            bool city01 = false, bool city12 = false)
        {
            return GenerateBase(id, type0, type1, type2)
                .AddBorderRegions(0, 1, 2, 1)
                .AddNeighbouring(0, 1, city01)
                .AddNeighbouring(1, 2, city12);
        }
    }
}
