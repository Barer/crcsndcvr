﻿namespace CarcassonneDiscovery.StandardTileSet
{
    using CarcassonneDiscovery.Entity;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Poskytovatel standardní sady kartiček.
    /// </summary>
    public class StandardTileSetSupplier : ITileSupplier
    {
        /// <summary>
        /// Zbývající kartičky.
        /// </summary>
        private readonly IList<ITileScheme> Tiles;

        /// <summary>
        /// Generátor náhodnch čísel.
        /// </summary>
        private readonly Random Rnd;

        /// <inheritdoc />
        public int RemainingCount => Tiles.Count;

        /// <summary>
        /// Konstruktor.
        /// </summary>
        public StandardTileSetSupplier()
        {
            Tiles = StandardTileGenerator.GenerateStandardTileSet();
            Rnd = new Random();
        }

        /// <inheritdoc />
        public ITileScheme GetFirstTile()
        {
            if (Tiles.Count == 84)
            {
                return PopAt(0);
            }

            throw new InvalidOperationException("First tile was already selected.");
        }

        /// <inheritdoc />
        public ITileScheme GetNextTile()
        {
            if (Tiles.Any())
            {
                return PopAt(Rnd.Next(0, Tiles.Count));
            }

            throw new InvalidOperationException("No tile remaining.");
        }

        /// <summary>
        /// Odstraní kartičku ze sady.
        /// </summary>
        /// <param name="index">Index kartičky.</param>
        /// <returns>Odstraněná kartička.</returns>
        private ITileScheme PopAt(int index)
        {
            var tile = Tiles[index];

            Tiles.RemoveAt(index);

            return tile;
        }

        /// <inheritdoc />
        public void ReturnTile(ITileScheme tile)
        {
            Tiles.Add(tile);
        }
    }
}
