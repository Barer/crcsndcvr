﻿namespace CarcassonneDiscovery.Execution
{
    /// <summary>
    /// Chybový kód porušení pravidel.
    /// </summary>
    public enum ExecutionErrorCode
    {
        /// <summary>
        /// Nedošlo k porušení pravidel.
        /// </summary>
        Ok,

        /// <summary>
        /// Hráč není na tahu.
        /// </summary>
        InvalidPlayerOnMove,

        /// <summary>
        /// Je prováděna špatná akce.
        /// </summary>
        InvalidGameAction,

        #region StartMove
        /// <summary>
        /// Není k dispozici žádná další kartička.
        /// </summary>
        NoTileRemaining,

        /// <summary>
        /// Žádná zbývající kartička nelze umístit.
        /// </summary>
        NoPlaceableTileRemaining,
        #endregion

        #region PlaceTile
        /// <summary>
        /// Umisťovaná kartička není poslední vylosovanou.
        /// </summary>
        NotSameTile,

        /// <summary>
        /// Na daných souřadnicích se již nachází kartička.
        /// </summary>
        NotEmptyCoords,

        /// <summary>
        /// Kartička nesprávně navazuje na sousední kartičky.
        /// </summary>
        WrongSurroundings,

        /// <summary>
        /// Kartička nemá žádnou sousední kartičku.
        /// </summary>
        NoSurroundings,
        #endregion

        #region PlaceFollower
        /// <summary>
        /// Region je již obsazený
        /// </summary>
        AlreadyOccupied,

        /// <summary>
        /// Souřadnice figurky nejsou stejné jako souřadnice naposledy položené kartičky.
        /// </summary>
        NotSameCoords,

        /// <summary>
        /// Hráč nemá k dispozici žádné figurky.
        /// </summary>
        NoFollowerLeft,
        #endregion

        #region RemoveFollower
        /// <summary>
        /// Na daných souřadnicích se nenachází žádná figurka.
        /// </summary>
        NoFollowerOnCoords,

        /// <summary>
        /// Daná figurka nepatří hráči provádějící tah
        /// </summary>
        NotOwnFollower,
        #endregion

        #region EndGame
        /// <summary>
        /// Stále nebyly umístěny všechny kartičky.
        /// </summary>
        TileRemaining,
        #endregion
    }
}
