﻿namespace CarcassonneDiscovery.Execution
{
    using CarcassonneDiscovery.Entity;

    /// <summary>
    /// Výsledek akce položení kartičky.
    /// </summary>
    public class PlaceTileExecutionResult : BaseExecutionResult
    {
        /// <summary>
        /// Hráč provádějící tah.
        /// </summary>
        public PlayerColor Color { get; set; }

        /// <summary>
        /// Položená kartička.
        /// </summary>
        public ITileScheme Tile { get; set; }

        /// <summary>
        /// Souřadnice položené kartičky.
        /// </summary>
        public Coords Coords { get; set; }

        /// <summary>
        /// Orientace položené kartičky.
        /// </summary>
        public TileOrientation TileOrientation { get; set; }
    }
}
