﻿namespace CarcassonneDiscovery.Execution
{
    using CarcassonneDiscovery.Entity;

    /// <summary>
    /// Výsledek akce předání tahu.
    /// </summary>
    public class PassMoveExecutionResult : BaseExecutionResult
    {
        /// <summary>
        /// Hráč provádějící tah.
        /// </summary>
        public PlayerColor Color { get; set; }
    }
}
