﻿namespace CarcassonneDiscovery.Execution
{
    using CarcassonneDiscovery.Entity;

    /// <summary>
    /// Výsledek akce zahájení tahu.
    /// </summary>
    public class StartMoveExecutionResult : BaseExecutionResult
    {
        /// <summary>
        /// Hráč na tahu.
        /// </summary>
        public PlayerColor PlayerOnMove { get; set; }

        /// <summary>
        /// Vylosovaná kartička.
        /// </summary>
        public ITileScheme Tile { get; set; }
    }
}
