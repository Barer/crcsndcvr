﻿namespace CarcassonneDiscovery.Execution
{
    using CarcassonneDiscovery.Entity;

    /// <summary>
    /// Výsledek akce zahájení hry.
    /// </summary>
    public class StartGameExecutionResult : BaseExecutionResult
    {
        /// <summary>
        /// První položená kartička.
        /// </summary>
        public ITileScheme FirstTile { get; set; }

        /// <summary>
        /// Souřadnice první položené kartičky.
        /// </summary>
        public Coords FirstTileCoords { get; set; }

        /// <summary>
        /// Orientace první položené kartičky.
        /// </summary>
        public TileOrientation FirstTileOrientation { get; set; }
    }
}
