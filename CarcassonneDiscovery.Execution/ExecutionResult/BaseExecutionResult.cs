﻿namespace CarcassonneDiscovery.Execution
{
    using CarcassonneDiscovery.Entity;

    /// <summary>
    /// Základ pro výsledek akce.
    /// </summary>
    public abstract class BaseExecutionResult
    {
        /// <summary>
        /// Výsledný stav po vykonání akce.
        /// </summary>
        public GameState State { get; set; }

        /// <summary>
        /// Kód chyby při vykonání akce.
        /// </summary>
        public ExecutionErrorCode ErrorCode { get; set; }
    }
}
