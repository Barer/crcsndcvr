﻿namespace CarcassonneDiscovery.Execution
{
    using CarcassonneDiscovery.Entity;

    /// <summary>
    /// Výsledek akce položení figurky.
    /// </summary>
    public class PlaceFollowerExecutionResult : BaseExecutionResult
    {
        /// <summary>
        /// Hráč provádějící tah.
        /// </summary>
        public PlayerColor Color { get; set; }

        /// <summary>
        /// Souřadnice položené figurky.
        /// </summary>
        public Coords Coords { get; set; }

        /// <summary>
        /// Identifikátor regionu, kde byla položena figurka.
        /// </summary>
        public int RegionId { get; set; }
    }
}
