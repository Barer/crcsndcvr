﻿namespace CarcassonneDiscovery.Execution
{
    using System.Collections.Generic;

    /// <summary>
    /// Výsledek akce ukončení hry.
    /// </summary>
    public class EndGameExecutionResult : BaseExecutionResult
    {
        /// <summary>
        /// Odebrané figukry a získané body hráčů.
        /// </summary>
        public IList<RemoveFollowerExecutionResult> RemovedFollowers { get; set; }
    }
}
