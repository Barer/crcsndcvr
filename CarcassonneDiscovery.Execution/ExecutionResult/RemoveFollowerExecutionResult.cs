﻿namespace CarcassonneDiscovery.Execution
{
    using CarcassonneDiscovery.Entity;

    /// <summary>
    /// Výsledek akce odebrání figurky.
    /// </summary>
    public class RemoveFollowerExecutionResult : BaseExecutionResult
    {
        /// <summary>
        /// Hráč provádějící tah.
        /// </summary>
        public PlayerColor Color { get; set; }

        /// <summary>
        /// Souřadnice odebrané figurky.
        /// </summary>
        public Coords Coords { get; set; }

        /// <summary>
        /// Identifikátor regionu.
        /// </summary>
        public int RegionId { get; set; }

        /// <summary>
        /// Počet získaných bodů.
        /// </summary>
        public int Score { get; set; }
    }
}
