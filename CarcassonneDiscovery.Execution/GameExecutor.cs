﻿namespace CarcassonneDiscovery.Execution
{
    using CarcassonneDiscovery.Entity;
    using CarcassonneDiscovery.StandardTileSet;
    using CarcassonneDiscovery.Tools;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Vykonavatel herních akcí.
    /// </summary>
    public static class GameExecutor
    {
        /// <summary>
        /// Seznam známých poskytovatelů kartiček.
        /// </summary>
        static IDictionary<string, Func<ITileSupplier>> TileSetBank { get; set; }

        /// <summary>
        /// Statický konstruktor.
        /// </summary>
        static GameExecutor()
        {
            TileSetBank = new Dictionary<string, Func<ITileSupplier>>
            {
                { "Standard", () => new StandardTileSetSupplier() }
            };
        }

        /// <summary>
        /// Zahájí hru.
        /// </summary>
        /// <param name="state">Stav hry.</param>
        /// <returns>Výsledek akce.</returns>
        public static StartGameExecutionResult StartGame(GameState state)
        {
            CheckNotNull(state);
            CheckNotNull(state.Params);
            CheckNotNull(state.Params.PlayerOrder);
            CheckNotNull(state.Params.TileSetId);

            if (state.Phase != GamePhase.BeforeStart)
            {
                return new StartGameExecutionResult
                {
                    ErrorCode = ExecutionErrorCode.InvalidGameAction
                };
            }

            if (!state.Params.PlayerOrder.Any())
            {
                throw new ArgumentException("At least one color is needed to start a game.");
            }

            if (!TileSetBank.TryGetValue(state.Params.TileSetId, out var tileSupplierBuilder))
            {
                throw new ArgumentException("Unknown tile set id.");
            }

            // Nastavujeme zde, u Set* funkcí nepotřebujeme TileSupplier vůbec
            state.TileSupplier = tileSupplierBuilder.Invoke();

            var firstTile = state.TileSupplier.GetFirstTile();
            var firstTileCoords = new Coords(0, 0);
            var firstTileOrientation = TileOrientation.N;

            SetStartGame(state, firstTile, firstTileCoords, firstTileOrientation, true);

            return new StartGameExecutionResult
            {
                State = state,
                ErrorCode = ExecutionErrorCode.Ok,
                FirstTile = firstTile,
                FirstTileCoords = firstTileCoords,
                FirstTileOrientation = firstTileOrientation
            };
        }

        /// <summary>
        /// Nastaví začátek hry.
        /// </summary>
        /// <param name="state">Stav hry.</param>
        /// <param name="firstTile">Počáteční kartička.</param>
        /// <param name="firstTileCoords">Souřadnice počáteční kartičky.</param>
        /// <param name="firstTileOrientation">Orientace počáteční kartičky.</param>
        /// <param name="skipCheck">Přeskočit kontrolu konzistence?</param>
        public static void SetStartGame(GameState state, ITileScheme firstTile, Coords firstTileCoords, TileOrientation firstTileOrientation, bool skipCheck = false)
        {
            if (!skipCheck)
            {
                CheckNotNull(state);
                CheckNotNull(state.Params);
                CheckNotNull(state.Params.PlayerOrder);
            }

            state.PlayerOnMove = -1;
            state.Grid = new Dictionary<Coords, TilePlacement>
            {
                {
                    firstTileCoords,
                    new TilePlacement
                    {
                        Coords = firstTileCoords,
                        Orientation = firstTileOrientation,
                        Tile = firstTile,
                        FollowerPlacement = null
                    }
                }
            };
            state.PlacedFollowers = state.Params.PlayerOrder.ToDictionary(x => x, x => (IList<FollowerPlacement>)new List<FollowerPlacement>());
            state.Score = state.Params.PlayerOrder.ToDictionary(x => x, x => 0);

            state.Phase = GamePhase.GameStarted;
        }

        /// <summary>
        /// Zahájí další tah.
        /// </summary>
        /// <param name="state">Stav hry.</param>
        /// <returns>Výsledek akce.</returns>
        public static StartMoveExecutionResult StartMove(GameState state)
        {
            CheckNotNull(state);
            CheckNotNull(state.Params);
            CheckNotNull(state.Params.PlayerOrder);
            CheckNotNull(state.TileSupplier);
            CheckNotNull(state.Grid);

            if (state.Phase != GamePhase.MoveEnded && state.Phase != GamePhase.GameStarted)
            {
                return new StartMoveExecutionResult
                {
                    ErrorCode = ExecutionErrorCode.InvalidGameAction
                };
            }

            var supplier = state.TileSupplier;

            ITileScheme nextTile;
            var discarded = new List<ITileScheme>();

            while (true)
            {
                if (supplier.RemainingCount == 0)
                {
                    return new StartMoveExecutionResult
                    {
                        ErrorCode = discarded.Any() ? ExecutionErrorCode.NoPlaceableTileRemaining : ExecutionErrorCode.NoTileRemaining
                    };
                }

                nextTile = supplier.GetNextTile();

                if (GridSearch.IsTilePlaceable(state.Grid, nextTile))
                {
                    break;
                }
            }

            foreach (var tile in discarded)
            {
                supplier.ReturnTile(tile);
            }

            var order = state.Params.PlayerOrder;
            var nextPlayer = order[(state.PlayerOnMove + 1) % order.Length];

            SetStartMove(state, nextTile, nextPlayer, true);

            return new StartMoveExecutionResult
            {
                State = state,
                ErrorCode = ExecutionErrorCode.Ok,
                Tile = nextTile,
                PlayerOnMove = nextPlayer,
            };
        }

        /// <summary>
        /// Nastaví další tah.
        /// </summary>
        /// <param name="state">Stav hry.</param>
        /// <param name="tile">Kartička k umístění.</param>
        /// <param name="color">Barva hráče na tahu.</param>
        /// <param name="skipCheck">Přeskočit kontrolu konzistence?</param>
        public static void SetStartMove(GameState state, ITileScheme tile, PlayerColor color, bool skipCheck = false)
        {
            if (!skipCheck)
            {
                CheckNotNull(state);
                CheckNotNull(state.Params);
                CheckNotNull(state.Params.PlayerOrder);
                CheckNotNull(state.Grid);
            }

            state.Tile = tile;
            state.PlayerOnMove = Array.IndexOf(state.Params.PlayerOrder, color);

            state.Phase = GamePhase.TileSelected;
        }

        /// <summary>
        /// Umístí kartičku.
        /// </summary>
        /// <param name="state">Stav hry.</param>
        /// <param name="color">Barva hráče na tahu.</param>
        /// <param name="tile">Kartička k umístění.</param>
        /// <param name="coords">Souřadnice kartičky.</param>
        /// <param name="orientation">Otočení kartičky.</param>
        public static PlaceTileExecutionResult PlaceTile(GameState state, PlayerColor color, ITileScheme tile, Coords coords, TileOrientation orientation)
        {
            CheckNotNull(state);
            CheckNotNull(state.Grid);
            CheckNotNull(state.Tile);
            CheckNotNull(state.Params);
            CheckNotNull(state.Params.PlayerOrder);
            CheckNotNull(tile);

            if (state.Phase != GamePhase.TileSelected)
            {
                return new PlaceTileExecutionResult
                {
                    ErrorCode = ExecutionErrorCode.InvalidGameAction
                };
            }

            ExecutionErrorCode errorCode;

            if (!CheckPlayerOnMove(state, color))
            {
                errorCode = ExecutionErrorCode.InvalidPlayerOnMove;
            }
            else if (!tile.TileEquals(state.Tile))
            {
                errorCode = ExecutionErrorCode.NotSameTile;
            }
            else
            {
                errorCode = (GridSearch.CheckTilePlacement(state.Grid, tile, coords, orientation)) switch
                {
                    PlacementRuleViolation.NotEmptyCoords => ExecutionErrorCode.NotEmptyCoords,
                    PlacementRuleViolation.NoNeighboringTile => ExecutionErrorCode.NoSurroundings,
                    PlacementRuleViolation.IncompatibleSurroundings => ExecutionErrorCode.WrongSurroundings,
                    PlacementRuleViolation.Ok => ExecutionErrorCode.Ok,
                    _ => throw new InvalidOperationException("Unexpected GridSearch result."),
                };
            }

            if (errorCode != ExecutionErrorCode.Ok)
            {
                return new PlaceTileExecutionResult
                {
                    ErrorCode = errorCode
                };
            }

            SetPlaceTile(state, color, tile, coords, orientation, true);

            return new PlaceTileExecutionResult
            {
                State = state,
                ErrorCode = ExecutionErrorCode.Ok,
                Color = color,
                Tile = tile,
                Coords = coords,
                TileOrientation = orientation
            };
        }

        /// <summary>
        /// Nastaví umístění kartičky.
        /// </summary>
        /// <param name="state">Stav hry.</param>
        /// <param name="color">Barva hráče na tahu.</param>
        /// <param name="tile">Kartička k umístění.</param>
        /// <param name="coords">Souřadnice kartičky.</param>
        /// <param name="orientation">Otočení kartičky.</param>
        /// <param name="skipCheck">Přeskočit kontrolu konzistence?</param>
        public static void SetPlaceTile(GameState state, PlayerColor color, ITileScheme tile, Coords coords, TileOrientation orientation, bool skipCheck = false)
        {
            if (!skipCheck)
            {
                CheckNotNull(state);
                CheckNotNull(state.Grid);
                CheckNotNull(tile);
            }

            state.Grid.Add(coords, new TilePlacement
            {
                Tile = tile,
                Coords = coords,
                Orientation = orientation,
                FollowerPlacement = null
            });

            state.Coords = coords;

            state.Phase = GamePhase.TilePlaced;
        }

        /// <summary>
        /// Umístí figurku.
        /// </summary>
        /// <param name="state">Stav hry.</param>
        /// <param name="color">Barva hráče na tahu.</param>
        /// <param name="coords">Souřadnice figurky.</param>
        /// <param name="regionId">Identifikátor regionu, kde byla položena figurka.</param>
        public static PlaceFollowerExecutionResult PlaceFollower(GameState state, PlayerColor color, Coords coords, int regionId)
        {
            CheckNotNull(state);
            CheckNotNull(state.Grid);
            CheckNotNull(state.Params);
            CheckNotNull(state.Params.PlayerOrder);
            CheckNotNull(state.PlacedFollowers);
            state.PlacedFollowers.TryGetValue(color, out var placedFollowers);
            CheckNotNull(placedFollowers);

            if (state.Phase != GamePhase.TilePlaced)
            {
                return new PlaceFollowerExecutionResult
                {
                    ErrorCode = ExecutionErrorCode.InvalidGameAction
                };
            }

            ExecutionErrorCode errorCode = ExecutionErrorCode.Ok;

            if (!CheckPlayerOnMove(state, color))
            {
                errorCode = ExecutionErrorCode.InvalidPlayerOnMove;
            }
            else if (state.Coords != coords)
            {
                errorCode = ExecutionErrorCode.NotSameCoords;
            }
            else if (placedFollowers.Count >= state.Params.FollowerCount)
            {
                errorCode = ExecutionErrorCode.NoFollowerLeft;
            }
            else if (!state.Grid.TryGetValue(coords, out var placement) || placement.Tile.RegionCount <= regionId || regionId < 0)
            {
                errorCode = ExecutionErrorCode.NotSameCoords;
            }
            else if (SafeExecute<bool, InvalidOperationException>(() => GridSearch.IsRegionOccupied(state.Grid, coords, regionId), true))
            {
                errorCode = ExecutionErrorCode.AlreadyOccupied;
            }

            if (errorCode != ExecutionErrorCode.Ok)
            {
                return new PlaceFollowerExecutionResult
                {
                    ErrorCode = errorCode
                };
            }

            SetPlaceFollower(state, color, coords, regionId, true);

            return new PlaceFollowerExecutionResult
            {
                State = state,
                ErrorCode = ExecutionErrorCode.Ok,
                Color = color,
                Coords = coords,
                RegionId = regionId
            };
        }

        /// <summary>
        /// Nastaví umístění figurky.
        /// </summary>
        /// <param name="state">Stav hry.</param>
        /// <param name="color">Barva hráče na tahu.</param>
        /// <param name="coords">Souřadnice figurky.</param>
        /// <param name="regionId">Identifikátor regionu, kde byla položena figurka.</param>
        /// <param name="skipCheck">Přeskočit kontrolu konzistence?</param>
        public static void SetPlaceFollower(GameState state, PlayerColor color, Coords coords, int regionId, bool skipCheck = false)
        {
            if (!skipCheck)
            {
                CheckNotNull(state);
                CheckNotNull(state.Grid);
                CheckNotNull(state.PlacedFollowers);
            }

            var followerPlacement = new FollowerPlacement
            {
                Color = color,
                Coords = coords,
                RegionId = regionId
            };

            if (state.PlacedFollowers.TryGetValue(color, out var placedFollowers))
            {
                placedFollowers.Add(followerPlacement);
            }

            if (state.Grid.TryGetValue(coords, out var tilePlacement))
            {
                tilePlacement.FollowerPlacement = followerPlacement;
            }

            state.Phase = GamePhase.MoveEnded;
        }

        /// <summary>
        /// Odstraní figurku.
        /// </summary>
        /// <param name="state">Stav hry.</param>
        /// <param name="color">Barva hráče na tahu.</param>
        /// <param name="coords">Souřadnice figurky.</param>
        /// <param name="regionId">Identifikátor regionu, kde byla položena figurka.</param>
        public static RemoveFollowerExecutionResult RemoveFollower(GameState state, PlayerColor color, Coords coords, int regionId)
        {
            CheckNotNull(state);
            CheckNotNull(state.Grid);
            CheckNotNull(state.Params);
            CheckNotNull(state.Params.PlayerOrder);
            CheckNotNull(state.Score);
            CheckNotNull(state.PlacedFollowers);
            state.PlacedFollowers.TryGetValue(color, out var placedFollowers);
            CheckNotNull(placedFollowers);

            if (state.Phase != GamePhase.TilePlaced)
            {
                return new RemoveFollowerExecutionResult
                {
                    ErrorCode = ExecutionErrorCode.InvalidGameAction
                };
            }

            var errorCode = ExecutionErrorCode.Ok;

            if (!CheckPlayerOnMove(state, color))
            {
                errorCode = ExecutionErrorCode.InvalidPlayerOnMove;
            }
            else if (!state.Grid.TryGetValue(coords, out var tilePlacement))
            {
                errorCode = ExecutionErrorCode.NoFollowerOnCoords;
            }
            else
            {
                var followerPlacementForPlayer = placedFollowers.FirstOrDefault(x => x.Coords == coords);
                var followerPlacementFromGrid = tilePlacement.FollowerPlacement;


                if (followerPlacementFromGrid == null || followerPlacementFromGrid.RegionId != regionId)
                {
                    errorCode = ExecutionErrorCode.NoFollowerOnCoords;
                }
                else if (followerPlacementForPlayer == null || followerPlacementFromGrid.Color != color || followerPlacementForPlayer.RegionId != regionId)
                {
                    errorCode = ExecutionErrorCode.NotOwnFollower;
                }
            }

            if (errorCode != ExecutionErrorCode.Ok)
            {
                return new RemoveFollowerExecutionResult
                {
                    ErrorCode = errorCode
                };
            }

            var score = GridSearch.GetScoreForFollower(state.Grid, coords, regionId);

            SetRemoveFollower(state, color, coords, regionId, score, true);

            return new RemoveFollowerExecutionResult
            {
                State = state,
                ErrorCode = ExecutionErrorCode.Ok,
                Color = color,
                Coords = coords,
                RegionId = regionId,
                Score = score
            };
        }

        /// <summary>
        /// Nastaví odebrání figurky.
        /// </summary>
        /// <param name="state">Stav hry.</param>
        /// <param name="color">Barva hráče na tahu.</param>
        /// <param name="coords">Souřadnice figurky.</param>
        /// <param name="regionId">Identifikátor regionu, kde byla položena figurka.</param>
        /// <param name="score">Počet obdržených bodů.</param>
        /// <param name="skipCheck">Přeskočit kontrolu konzistence?</param>
        public static void SetRemoveFollower(GameState state, PlayerColor color, Coords coords, int regionId, int score, bool skipCheck = false)
        {
            if (!skipCheck)
            {
                CheckNotNull(state);
                CheckNotNull(state.Grid);
                CheckNotNull(state.PlacedFollowers);
                CheckNotNull(state.Score);
            }

            if (state.PlacedFollowers.TryGetValue(color, out var placedFollowers))
            {
                state.PlacedFollowers[color] = placedFollowers.Where(x => x.Coords != coords).ToList();
            }

            if (state.Grid.TryGetValue(coords, out var tilePlacement))
            {
                tilePlacement.FollowerPlacement = null;
            }

            if (state.Score.ContainsKey(color))
            {
                state.Score[color] += score;
            }

            state.Phase = GamePhase.MoveEnded;
        }

        /// <summary>
        /// Ukončí tah hráče.
        /// </summary>
        /// <param name="state">Stav hry.</param>
        /// <param name="color">Barva hráče na tahu.</param>
        public static PassMoveExecutionResult PassMove(GameState state, PlayerColor color)
        {
            CheckNotNull(state);
            CheckNotNull(state.Grid);
            CheckNotNull(state.Params);
            CheckNotNull(state.Params.PlayerOrder);

            if (state.Phase != GamePhase.TilePlaced)
            {
                return new PassMoveExecutionResult
                {
                    ErrorCode = ExecutionErrorCode.InvalidGameAction
                };
            }

            if (!CheckPlayerOnMove(state, color))
            {
                return new PassMoveExecutionResult
                {
                    ErrorCode = ExecutionErrorCode.InvalidPlayerOnMove
                };
            }

            SetPassMove(state, color, true);

            return new PassMoveExecutionResult
            {
                State = state,
                ErrorCode = ExecutionErrorCode.Ok,
                Color = color
            };
        }

        /// <summary>
        /// Nastaví předání tahu.
        /// </summary>
        /// <param name="state">Stav hry.</param>
        /// <param name="color">Barva hráče na tahu.</param>
        /// <param name="skipCheck">Přeskočit kontrolu konzistence?</param>
        public static void SetPassMove(GameState state, PlayerColor color, bool skipCheck = false)
        {
            if (!skipCheck)
            {
                CheckNotNull(state);
            }

            state.Phase = GamePhase.MoveEnded;
        }

        /// <summary>
        /// Ukončí hru.
        /// </summary>
        /// <param name="state">Stav hry.</param>
        public static EndGameExecutionResult GameEnd(GameState state)
        {
            CheckNotNull(state);
            CheckNotNull(state.Grid);
            CheckNotNull(state.Score);
            CheckNotNull(state.PlacedFollowers);
            CheckNotNull(state.TileSupplier);

            if (state.Phase != GamePhase.MoveEnded)
            {
                return new EndGameExecutionResult
                {
                    ErrorCode = ExecutionErrorCode.InvalidGameAction
                };
            }

            if (state.TileSupplier.RemainingCount > 0)
            {
                return new EndGameExecutionResult
                {
                    ErrorCode = ExecutionErrorCode.TileRemaining
                };
            }

            var removedFollowers = new List<RemoveFollowerExecutionResult>();

            foreach (var followerPlacement in state.PlacedFollowers.SelectMany(x => x.Value))
            {
                var score = GridSearch.GetScoreForFollower(state.Grid, followerPlacement.Coords, followerPlacement.RegionId);

                removedFollowers.Add(new RemoveFollowerExecutionResult
                {
                    State = null,
                    ErrorCode = ExecutionErrorCode.Ok,
                    Color = followerPlacement.Color,
                    Coords = followerPlacement.Coords,
                    Score = score
                });
            }

            SetEndGame(state, removedFollowers, true);

            return new EndGameExecutionResult
            {
                State = state,
                ErrorCode = ExecutionErrorCode.Ok,
                RemovedFollowers = removedFollowers
            };
        }

        /// <summary>
        /// Nastaví konec hry.
        /// </summary>
        /// <param name="state">Stav hry.</param>
        /// <param name="removedFollowers">Seznam odstraněných figurek a bodů.</param>
        /// <param name="skipCheck">Přeskočit kontrolu konzistence?</param>
        public static void SetEndGame(GameState state, IEnumerable<RemoveFollowerExecutionResult> removedFollowers, bool skipCheck = true)
        {
            if (!skipCheck)
            {
                CheckNotNull(state);
                CheckNotNull(state.Score);
            }

            foreach (var removedFollower in removedFollowers)
            {
                if (state.Score.ContainsKey(removedFollower.Color))
                {
                    state.Score[removedFollower.Color] += removedFollower.Score;
                }
            }

            state.Phase = GamePhase.GameEnded;
        }

        #region Pomocné metody
        /// <summary>
        /// Kontola nenullovosti argumentu.
        /// </summary>
        /// <param name="obj">Objekt.</param>
        private static void CheckNotNull<T>(T obj)
            where T : class
        {
            if (obj == null)
            {
                throw new ArgumentException();
            }
        }

        /// <summary>
        /// Kontrola hráče na tahu.
        /// </summary>
        /// <param name="state">Stav hry.</param>
        /// <param name="actualPlayer">Hráč provádějící tah.</param>
        /// <returns>Je daný hráč na tahu?</returns>
        private static bool CheckPlayerOnMove(GameState state, PlayerColor actualPlayer)
        {
            if (state.PlayerOnMove < 0 || state.PlayerOnMove >= state.Params.PlayerOrder.Length)
            {
                throw new ArgumentException("Invalid color on move index.");
            }

            return state.Params.PlayerOrder[state.PlayerOnMove] == actualPlayer;
        }

        /// <summary>
        /// Bezpečně zavolá funkci. Pokud při běhu funkce dojde k vyhození výjimky, vrátí výchozí hodnotu. 
        /// </summary>
        /// <typeparam name="T">Typ návratové hodnoty.</typeparam>
        /// <typeparam name="E">Typ výjimky.</typeparam>
        /// <param name="function">Volaná funkce.</param>
        /// <param name="defaultValue">Výchozí návratová hodnota.</param>
        /// <returns></returns>
        private static T SafeExecute<T, E>(Func<T> function, T defaultValue = default)
            where E : Exception
        {
            try
            {
                return function.Invoke();
            }
            catch (E)
            {
                return defaultValue;
            }
        }
        #endregion
    }
}
