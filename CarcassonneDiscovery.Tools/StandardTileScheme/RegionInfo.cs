﻿namespace CarcassonneDiscovery.Tools
{
    using CarcassonneDiscovery.Entity;
    using System.Collections.Generic;

    /// <summary>
    /// Informace o regionu na kartičce.
    /// </summary>
    public class RegionInfo
    {
        /// <summary>
        /// Identifikátor regionu.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Typ regionu.
        /// </summary>
        public RegionType Type { get; set; }

        /// <summary>
        /// Hranice kartičky, na kterých se nachází region.
        /// </summary>
        public TileOrientation Borders { get; set; }

        /// <summary>
        /// Seznam identifikátorů měst, které se nachází na kartičce.
        /// </summary>
        public IList<int> NeighboringCities { get; set; } = new List<int>();

        /// <summary>
        /// Seznam identifikátorů regionů, které sousedí s daným regionem.
        /// </summary>
        public IList<int> NeighboringRegions { get; set; } = new List<int>();
    }
}
