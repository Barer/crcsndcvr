﻿namespace CarcassonneDiscovery.Tools
{
    using CarcassonneDiscovery.Entity;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Standarní implementace <see cref="ITileScheme"/>.
    /// </summary>
    public class TileScheme : ITileScheme
    {
        /// <inheritdoc />
        public string Id { get; set; }

        /// <inheritdoc />
        public int CityCount { get; set; }

        /// <inheritdoc />
        public int RegionCount => Regions?.Length ?? 0;

        /// <summary>
        /// Informace o regionech.
        /// </summary>
        public RegionInfo[] Regions { get; set; } = null;

        /// <summary>
        /// Regiony na hranicích kartičky.
        /// </summary>
        public int[] RegionsOnBorders { get; set; } = new int[4];

        /// <inheritdoc />
        public TileOrientation GetRegionBorders(int id)
        {
            RegionIdRangeCheck(id);

            return Regions[id].Borders;
        }

        /// <inheritdoc />
        public IEnumerable<int> GetRegionCities(int id)
        {
            RegionIdRangeCheck(id);

            return Regions[id].NeighboringCities;
        }

        /// <inheritdoc />
        public IEnumerable<int> GetRegionNeighbors(int id)
        {
            RegionIdRangeCheck(id);

            return Regions[id].NeighboringRegions;
        }

        /// <inheritdoc />
        public int GetRegionOnBorder(TileOrientation border)
        {
            switch (border)
            {
                case TileOrientation.N:
                    return RegionsOnBorders[0];
                case TileOrientation.E:
                    return RegionsOnBorders[1];
                case TileOrientation.S:
                    return RegionsOnBorders[2];
                case TileOrientation.W:
                    return RegionsOnBorders[3];
            }

            throw new ArgumentOutOfRangeException("Invalid border specified. Expected cardinal direction.");
        }

        /// <inheritdoc />
        public RegionType GetRegionType(int id)
        {
            RegionIdRangeCheck(id);

            return Regions[id].Type;
        }

        /// <summary>
        /// Provede kontrolu identifikátoru regionu.
        /// </summary>
        /// <param name="id">Identifikátor regionu.</param>
        protected void RegionIdRangeCheck(int id)
        {
            if (id < 0 || id >= RegionCount)
            {
                throw new ArgumentOutOfRangeException("Invalid identifier of region.");
            }

        }
    }
}
