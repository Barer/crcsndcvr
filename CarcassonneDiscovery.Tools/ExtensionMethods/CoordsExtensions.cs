﻿namespace CarcassonneDiscovery.Tools
{
    using CarcassonneDiscovery.Entity;
    using System;

    /// <summary>
    /// Extension metody pro <see cref="Coords"/>.
    /// </summary>
    public static class CoordsExtensions
    {
        /// <summary>
        /// Vypočítá souřadnice sousední kartičky.
        /// </summary>
        /// <param name="coords">Souřadnice kartičky.</param>
        /// <param name="direction">Směr, ve kterém se nachází sousední kartička.
        /// <returns>Souřadnice sousední kartičky.</returns>
        public static Coords GetNeighboringCoords(this Coords coords, TileOrientation direction)
        {
            switch (direction)
            {
                case TileOrientation.N:
                    return new Coords(coords.X, coords.Y - 1);
                case TileOrientation.E:
                    return new Coords(coords.X + 1, coords.Y);
                case TileOrientation.S:
                    return new Coords(coords.X, coords.Y + 1);
                case TileOrientation.W:
                    return new Coords(coords.X - 1, coords.Y);
            }

            throw new ArgumentException("Invalid direction.");
        }
    }
}
