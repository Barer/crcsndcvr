﻿namespace CarcassonneDiscovery.Tools
{
    using CarcassonneDiscovery.Entity;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Extension metody pro <see cref="ITileScheme"/>.
    /// </summary>
    public static class ITileSchemeExtensions
    {
        /// <summary>
        /// Světové strany.
        /// </summary>
        private static readonly TileOrientation[] CardinalDirections = new TileOrientation[] { TileOrientation.N, TileOrientation.E, TileOrientation.S, TileOrientation.W };

        /// <summary>
        /// Porovná dvě schémata kartiček na sémantickou rovnost.
        /// </summary>
        /// <param name="scheme">Schéma kartičky.</param>
        /// <param name="other">Schéma jiné kartičky.</param>
        /// <returns>Jsou obě kartičky sémanticky shodné?</returns>
        public static bool TileEquals(this ITileScheme scheme, ITileScheme other)
        {
            if (scheme.CityCount != other.CityCount)
            {
                return false;
            }

            var regions = scheme.RegionCount;

            if (regions != other.RegionCount)
            {
                return false;
            }

            try
            {
                for (int rId = 0; rId < regions; rId++)
                {
                    if (!SetEquality(scheme.GetRegionCities(rId), other.GetRegionCities(rId)) ||
                        !SetEquality(scheme.GetRegionNeighbors(rId), other.GetRegionNeighbors(rId)) ||
                        (scheme.GetRegionBorders(rId) != other.GetRegionBorders(rId)) ||
                        (scheme.GetRegionType(rId) != other.GetRegionType(rId)))
                    {
                        return false;
                    }
                }

                foreach (var border in CardinalDirections)
                {
                    if (scheme.GetRegionOnBorder(border) != other.GetRegionOnBorder(border))
                    {
                        return false;
                    }
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                throw new InvalidOperationException("Tile scheme is not consistent.");
            }

            return true;
        }

        /// <summary>
        /// Vytvoří kopii schématu kartičky, ve standardní reprezentaci.
        /// </summary>
        /// <param name="scheme">Schéma kartičky.</param>
        /// <returns>Kopie schématu kartičky, ve standardní reprezentaci.</returns>
        public static TileScheme Copy(this ITileScheme scheme)
        {
            var result = new TileScheme
            {
                Id = scheme.Id,
                CityCount = scheme.CityCount,
                Regions = new RegionInfo[scheme.RegionCount],
                RegionsOnBorders = new int[4]
                {
                    scheme.GetRegionOnBorder(TileOrientation.N),
                    scheme.GetRegionOnBorder(TileOrientation.E),
                    scheme.GetRegionOnBorder(TileOrientation.S),
                    scheme.GetRegionOnBorder(TileOrientation.W)
                }
            };

            for (int rId = 0; rId < result.RegionCount; rId++)
            {
                result.Regions[rId] = new RegionInfo
                {
                    Id = rId,
                    Type = scheme.GetRegionType(rId),
                    Borders = scheme.GetRegionBorders(rId),
                    NeighboringCities = scheme.GetRegionCities(rId).ToList(),
                    NeighboringRegions = scheme.GetRegionNeighbors(rId).ToList()
                };
            }

            return result;
        }

        /// <summary>
        /// Zjistí konzistenci schématu kartičky.
        /// </summary>
        /// <param name="scheme">Schéma kartičky.</param>
        /// <returns>Je schéma kartičky konzistentní?</returns>
        public static bool IsConsistent(this ITileScheme scheme)
        {
            int cities = scheme.CityCount;
            int regions = scheme.RegionCount;

            // Konistence hranic
            var checkedBorders = TileOrientation.None;

            // Konistence sousednosti regionů
            var neighbouring = new bool[cities, cities];

            // Try-Catch: Regiony nemusí být dostupné
            try
            {
                for (int rId = 0; rId < regions; rId++)
                {
                    foreach (var nId in scheme.GetRegionNeighbors(rId))
                    {
                        if (nId < 0 || nId >= regions)
                        {
                            return false;
                        }

                        neighbouring[rId, nId] = true;
                    }

                    foreach (var cId in scheme.GetRegionCities(rId))
                    {
                        if (cId < 0 || cId >= cities)
                        {
                            return false;
                        }
                    }

                    var regionBorders = scheme.GetRegionBorders(rId);

                    foreach (var border in CardinalDirections)
                    {
                        // Pokud je region na dané hraně
                        if ((regionBorders & border) == border)
                        {
                            // Pokud jsme již danou hranu zkontrolovali
                            if ((checkedBorders & border) != TileOrientation.None)
                            {
                                return false;
                            }

                            checkedBorders |= border;

                            if (scheme.GetRegionOnBorder(border) != rId)
                            {
                                return false;
                            }
                        }
                    }
                }
            }
            catch (ArgumentOutOfRangeException)
            {
                return false;
            }

            for (int i = 0; i < regions - 1; i++)
            {
                if (neighbouring[i, i])
                {
                    return false;
                }

                for (int j = i + 1; j < regions - 1; j++)
                {
                    if (neighbouring[i, j] != neighbouring[j, i])
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Porovná dvě (multi)množiny na rovnost.
        /// </summary>
        /// <typeparam name="T">Typ objektu.</typeparam>
        /// <param name="a">Množina.</param>
        /// <param name="b">Množina.</param>
        /// <returns>Jsou obě množiny navzájem rovné?</returns>
        private static bool SetEquality<T>(IEnumerable<T> a, IEnumerable<T> b)
            where T : IComparable
        {
            var aAr = a.OrderBy(x => x).ToArray();
            var bAr = b.OrderBy(x => x).ToArray();

            var length = aAr.Length;

            if (bAr.Length != length)
            {
                return false;
            }

            for (int i = 0; i < length; i++)
            {
                if (!aAr[i].Equals(bAr[i]))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
