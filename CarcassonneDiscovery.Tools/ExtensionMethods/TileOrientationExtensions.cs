﻿namespace CarcassonneDiscovery.Tools
{
    using CarcassonneDiscovery.Entity;
    using System;

    /// <summary>
    /// Extension metody pro <see cref="TileOrientation"/>
    /// </summary>
    public static class TileOrientationExtensions
    {
        /// <summary>
        /// Otočí orientaci kartičky o daný úhel.
        /// </summary>
        /// <param name="orientation">Orientace kartičky.</param>
        /// <param name="angle">Úhel otočení.</param>
        /// <returns>Otočená orientace kartičky.</returns>
        public static TileOrientation Rotate(this TileOrientation orientation, TileOrientation angle)
        {
            int shift;

            switch (angle)
            {
                case TileOrientation.N:
                    shift = 0;
                    break;
                case TileOrientation.E:
                    shift = 1;
                    break;
                case TileOrientation.S:
                    shift = 2;
                    break;
                case TileOrientation.W:
                    shift = 3;
                    break;
                default:
                    throw new ArgumentException("Invalid angle of rotation.");
            }

            var rotated = (int)orientation << shift;
            var result = (((rotated & 0b1111000) >> 4) | rotated) & 0b1111;

            return (TileOrientation)result;
        }

        /// <summary>
        /// Zjistí orientaci kartičky před otočením o daný úhel.
        /// </summary>
        /// <param name="orientation">Orientace kartičky před otočením.</param>
        /// <param name="angle">Úhel otočení.</param>
        /// <returns>Orientace kartičky před otočením.</returns>
        public static TileOrientation Derotate(this TileOrientation orientation, TileOrientation angle)
        {
            int shift;

            switch (angle)
            {
                case TileOrientation.N:
                    shift = 0;
                    break;
                case TileOrientation.E:
                    shift = 3;
                    break;
                case TileOrientation.S:
                    shift = 2;
                    break;
                case TileOrientation.W:
                    shift = 1;
                    break;
                default:
                    throw new ArgumentException("Invalid angle of rotation.");
            }

            var rotated = (int)orientation << shift;
            var result = (((rotated & 0b1111000) >> 4) | rotated) & 0b1111;

            return (TileOrientation)result;
        }
    }
}
