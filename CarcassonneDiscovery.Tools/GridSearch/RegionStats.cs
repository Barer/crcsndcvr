﻿namespace CarcassonneDiscovery.Tools
{
    using CarcassonneDiscovery.Entity;
    using System.Collections.Generic;

    /// <summary>
    /// Výsledek prohledávání při výpočtu skóre.
    /// </summary>
    public class RegionStats
    {
        /// <summary>
        /// Typ regionu.
        /// </summary>
        public RegionType Type { get; set; }

        /// <summary>
        /// Všchny souřadnice regionů, které jsou součástí regionu.
        /// </summary>
        public IList<RegionCoords> RegionCoords { get; set; }

        /// <summary>
        /// Počet měst na regionu.
        /// </summary>
        /// <remarks>
        /// Pokud se jedná o nížinu, obsahuje defaultní hodnotu.
        /// Pokud se jedná o hory, jsou zde započítaná i města na nížinách.
        /// </remarks>
        public int CityCount { get; set; }

        /// <summary>
        /// Počet měst na přilehlých nížinách.
        /// </summary>
        /// <remarks>
        /// Pokud se nejedná o hory, obsahuje 0.
        /// </remarks>
        public int GrasslandCityCount { get; set; }

        /// <summary>
        /// Počet kartiček regionu.
        /// </summary>
        /// <remarks>
        /// Pokud se jedná o hory, obsahuje defaultní hodnotu.
        /// </remarks>
        public int TileCount { get; set; }

        /// <summary>
        /// Skóre regionu.
        /// </summary>
        public int Score { get; set; }

        /// <summary>
        /// Otevřené souřadnice regionu.
        /// </summary>
        public IList<Coords> OpenCoords { get; set; }

        /// <summary>
        /// Otevřené souřadnice sousedních nížin.
        /// </summary>
        /// <remarks>
        /// Pokud se nejedná o hory, obsahuje defaultní hodnotu.
        /// </remarks>
        public IList<Coords> OpenGrasslandCoords { get; set; }

        /// <summary>
        /// Počet figurek na regionu.
        /// </summary>
        public int FollowerCount { get; set; }
    }
}
