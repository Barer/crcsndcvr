﻿namespace CarcassonneDiscovery.Tools
{
    using CarcassonneDiscovery.Entity;
    using System.Collections.Generic;

    /// <summary>
    /// Výsledek hledání <see cref="GridSearch"/>.
    /// </summary>
    internal struct GridSearchResult
    {
        /// <summary>
        /// Informace o regionu.
        /// </summary>
        public IDictionary<Coords, TileRegionInfo> TileRegionInformation { get; set; }

        /// <summary>
        /// Seznam souřadnic, na kterých se nachází figurky na daném regionu.
        /// </summary>
        public ISet<Coords> FollowerCoords { get; set; }

        /// <summary>
        /// Seznam otevřených souřadnic regionu.
        /// </summary>
        public ISet<Coords> OpenCoords { get; set; }
    }
}
