﻿using CarcassonneDiscovery.Entity;

namespace CarcassonneDiscovery.Tools
{
    /// <summary>
    /// Souřadnice regionu.
    /// </summary>
    public struct RegionCoords
    {
        /// <summary>
        /// Souřadnice kartičky.
        /// </summary>
        public Coords Coords;

        /// <summary>
        /// Identifikátor regionu na kartičce.
        /// </summary>
        public int RegionId;

        /// <inheritdoc />
        public override int GetHashCode()
        {
            return Coords.GetHashCode() & (RegionId << 16);
        }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            if (obj is RegionCoords)
            {
                var other = (RegionCoords)obj;
                return other.Coords == Coords && other.RegionId == RegionId;
            }

            return false;
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return $"[{Coords.X},{Coords.Y};{RegionId}]";
        }
    }
}
