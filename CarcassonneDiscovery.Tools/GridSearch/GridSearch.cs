﻿namespace CarcassonneDiscovery.Tools
{
    using CarcassonneDiscovery.Entity;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Grid = System.Collections.Generic.IDictionary<CarcassonneDiscovery.Entity.Coords, CarcassonneDiscovery.Entity.TilePlacement>;

    /// <summary>
    /// Sada algoritmů pro procházení mapy umístěných kartiček.
    /// </summary>
    public static class GridSearch
    {
        /// <summary>
        /// Seznam světových stran.
        /// </summary>
        private static readonly TileOrientation[] CardinalDirections = new TileOrientation[] { TileOrientation.N, TileOrientation.E, TileOrientation.S, TileOrientation.W };

        /// <summary>
        /// Zjistí umístitelnost kartičky do mapy.
        /// </summary>
        /// <param name="grid">Mapa kartiček.</param>
        /// <param name="tile">Kartička k umístění.</param>
        /// <returns>Lze danou kartičku umístit do mapy?</returns>
        public static bool IsTilePlaceable(Grid grid, ITileScheme tile)
        {
            return GetAllTilePlacements(grid, tile).Any();
        }

        /// <summary>
        /// Vrátí všechny umístitelné pozice dané kartičky.
        /// </summary>
        /// <param name="grid">Mapa kartiček.</param>
        /// <param name="tile">Kartička k umístění.</param>
        /// <returns>Enumerátor nad všemi pozicemi, kam lze kartičku umístit.</returns>
        public static IEnumerable<TilePlacement> GetAllTilePlacements(Grid grid, ITileScheme tile)
        {
            var emptyCoordsList = new HashSet<Coords>();

            foreach (var coordsWithTile in grid.Keys)
            {
                foreach (var neighCoordsDirection in CardinalDirections)
                {
                    var neighCoords = coordsWithTile.GetNeighboringCoords(neighCoordsDirection);

                    if (!grid.ContainsKey(neighCoords))
                    {
                        emptyCoordsList.Add(neighCoords);
                    }
                }
            }

            foreach (var coords in emptyCoordsList)
            {
                foreach (var orientation in CardinalDirections)
                {
                    if (CheckTilePlacement(grid, tile, coords, orientation) == PlacementRuleViolation.Ok)
                    {
                        yield return new TilePlacement
                        {
                            Coords = coords,
                            Tile = tile,
                            Orientation = orientation
                        };
                    }
                }
            }
        }

        /// <summary>
        /// Zjistí, zda je umístění kartičky v souladu s pravidly.
        /// </summary>
        /// <param name="grid">Mapa kartiček.</param>
        /// <param name="tile">Kartička k umístění.</param>
        /// <param name="coords">Souřadnice kartičky.</param>
        /// <param name="orientation">Orientace kartičky.</param>
        /// <returns><see cref="PlacementRuleViolation.Ok"/> pokud je umístění kartičky v pořádku, jinak druh porušení pravidel dle <see cref="PlacementRuleViolation"/>.</returns>
        public static PlacementRuleViolation CheckTilePlacement(Grid grid, ITileScheme tile, Coords coords, TileOrientation orientation)
        {
            if (grid.ContainsKey(coords))
            {
                return PlacementRuleViolation.NotEmptyCoords;
            }

            bool hasNeighbor = false;

            foreach (var border in CardinalDirections)
            {
                var neighCoords = coords.GetNeighboringCoords(border);

                if (grid.TryGetValue(neighCoords, out var neighTilePlacement))
                {
                    hasNeighbor = true;

                    var neighTile = neighTilePlacement.Tile;

                    var borderOnThisScheme = border.Derotate(orientation);
                    var borderOnNeighScheme = border.Rotate(TileOrientation.S).Derotate(neighTilePlacement.Orientation);

                    var thisRegionId = tile.GetRegionOnBorder(borderOnThisScheme);
                    var neighRegionId = neighTile.GetRegionOnBorder(borderOnNeighScheme);

                    var thisType = tile.GetRegionType(thisRegionId);
                    var neighType = neighTile.GetRegionType(neighRegionId);

                    if (thisType != neighType)
                    {
                        return PlacementRuleViolation.IncompatibleSurroundings;
                    }
                }
            }

            return hasNeighbor ? PlacementRuleViolation.Ok : PlacementRuleViolation.NoNeighboringTile;
        }

        /// <summary>
        /// Zjistí, zda je region obsazený nějakou figurkou.
        /// </summary>
        /// <param name="grid">Mapa kartiček.</param>
        /// <param name="coords">Souřadnice kartičky, kde se region nachází.</param>
        /// <param name="regionId">Identifikátor regionu.</param>
        /// <returns>Je region obsazený?</returns>
        public static bool IsRegionOccupied(Grid grid, Coords coords, int regionId)
        {
            var searchResult = Search(grid, coords, regionId, stopWhenOccupied: true);

            return searchResult.FollowerCoords.Count > 0;
        }

        /// <summary>
        /// Zjistí, zda je region uzavřený.
        /// </summary>
        /// <param name="grid">Mapa kartiček.</param>
        /// <param name="coords">Souřadnice kartičky, kde se region nachází.</param>
        /// <param name="regionId">Identifikátor regionu.</param>
        /// <returns>Je region uzavřený?</returns>
        public static bool IsRegionClosed(Grid grid, Coords coords, int regionId)
        {
            var searchResult = Search(grid, coords, regionId, stopWhenOpen: true);

            return searchResult.OpenCoords.Count == 0;
        }

        /// <summary>
        /// Zjistí počet bodů pro danou figurku.
        /// </summary>
        /// <param name="grid">Mapa kartiček.</param>
        /// <param name="coords">Souřadnice kartičky, kde se figurka nachází.</param>
        /// <param name="regionId">Identifikátor regionu, kde se figurka nachází.</param>
        /// <param name="forceOpen">Počítat body, jako by byl region uzavřený?</param>
        /// <returns>Pčet bodů za danou figurku.</returns>
        public static int GetScoreForFollower(Grid grid, Coords coords, int regionId, bool forceOpen = false)
        {
            var stats = GetRegionStats(grid, coords, regionId, forceOpen);
            return stats.Score;
        }

        /// <summary>
        /// Zjistí skóre regionu a další pomocné informace.
        /// </summary>
        /// <param name="grid">Mapa kartiček.</param>
        /// <param name="coords">Souřadnice kartičky, kde se figurka nachází.</param>
        /// <param name="regionId">Identifikátor regionu, kde se figurka nachází.</param>
        /// <param name="forceOpen">Počítat body, jako by byl region uzavřený?</param>
        /// <returns>Statistiky o regionu.</returns>
        public static RegionStats GetRegionStats(Grid grid, Coords coords, int regionId, bool forceOpen = false)
        {
            var search = Search(grid, coords, regionId);

            var tileCount = search.TileRegionInformation.Count;
            var isOpen = search.OpenCoords.Count > 0 || tileCount <= 2 || forceOpen;

            var stats = new RegionStats
            {
                Type = grid[coords].Tile.GetRegionType(regionId),
                TileCount = tileCount,
                RegionCoords = search.TileRegionInformation.SelectMany(kv => kv.Value.RegionIds.Select(regionId => new RegionCoords
                {
                    Coords = kv.Key,
                    RegionId = regionId
                })).ToList(),
                OpenCoords = search.OpenCoords.ToList(),
                FollowerCount = search.FollowerCoords.Count
            };

            switch (stats.Type)
            {
                case RegionType.Grassland:
                    stats.Score = isOpen ? tileCount : tileCount * 2;
                    return stats;

                case RegionType.Sea:
                    stats.CityCount = search.TileRegionInformation.Sum(x => x.Value.CitiesIds.Count);
                    stats.Score = isOpen ? stats.CityCount : stats.CityCount + tileCount;
                    return stats;

                case RegionType.Mountain:
                    var mountainCities = new Dictionary<Coords, SortedSet<int>>();
                    var grasslandCities = new Dictionary<Coords, SortedSet<int>>();
                    var openCoordsOnMountainGrasslands = new HashSet<Coords>();

                    // Projdi všechny
                    foreach (var tileInfo in search.TileRegionInformation)
                    {
                        // Přidej města na horách
                        if (mountainCities.TryGetValue(tileInfo.Key, out var citiesOnTileM))
                        {
                            foreach (var cId in tileInfo.Value.CitiesIds)
                            {
                                citiesOnTileM.Add(cId);
                            }
                        }
                        else
                        {
                            mountainCities.Add(tileInfo.Key, new SortedSet<int>(tileInfo.Value.CitiesIds));
                        }

                        // Projdi i okolní nížiny
                        var tileInfoTile = grid[tileInfo.Key].Tile;
                        foreach (var nId in tileInfo.Value.NeighboringRegionIds)
                        {
                            if (tileInfoTile.GetRegionType(nId) == RegionType.Grassland)
                            {
                                var nSearch = Search(grid, tileInfo.Key, nId);

                                // Přidej města z kartiček
                                foreach (var nTileInfo in nSearch.TileRegionInformation)
                                {
                                    if (!grasslandCities.TryGetValue(nTileInfo.Key, out var citiesOnTileG))
                                    {
                                        citiesOnTileG = new SortedSet<int>();
                                        grasslandCities.Add(nTileInfo.Key, citiesOnTileG);
                                    }

                                    foreach (var cId in nTileInfo.Value.CitiesIds)
                                    {
                                        // Pokud je město už na horách, nebudeme ho znovu připočítávat
                                        if (!mountainCities.TryGetValue(nTileInfo.Key, out var mountainCityIds) || !mountainCityIds.Contains(cId))
                                        {
                                            citiesOnTileG.Add(cId);
                                        }
                                    }
                                }

                                // Přidej souřadnice na okolních nížinách
                                foreach (var nCoords in nSearch.OpenCoords)
                                {
                                    openCoordsOnMountainGrasslands.Add(nCoords);
                                }
                            }
                        }
                    }
                    stats.CityCount = mountainCities.Sum(x => x.Value.Count);
                    stats.GrasslandCityCount = grasslandCities.Sum(x => x.Value.Count);
                    stats.OpenGrasslandCoords = openCoordsOnMountainGrasslands.ToList();
                    stats.Score = isOpen ? (stats.CityCount + stats.GrasslandCityCount): (stats.CityCount + stats.GrasslandCityCount) * 2;
                    return stats;
            }

            throw new InvalidOperationException("Invalid region type");
        }

        /// <summary>
        /// Pomocí DFS v mapě kartiček zjistí informace o daném regionu.
        /// </summary>
        /// <param name="grid">Mapa kartiček.</param>
        /// <param name="initialCoords">Souřadnice počátečního regionu.</param>
        /// <param name="initialRegionId">Identifikátor počátečního regionu.</param>
        /// <param name="stopWhenOccupied">Skonči hledání, pokud je region obsazený?</param>
        /// <param name="stopWhenOpen">Skonči hledání, pokud je region otevřený?</param>
        /// <returns>Výsledek hledání.</returns>
        /// <remarks>
        /// Pokud hledání skončí na základě příznaku "stopWhen*", není zaručena konzistence informací o regionech, je pouze zaručeno nastavení daného příznaku ve výsedku.
        /// </remarks>
        internal static GridSearchResult Search(Grid grid, Coords initialCoords, int initialRegionId, bool stopWhenOccupied = false, bool stopWhenOpen = false)
        {
            var result = new GridSearchResult
            {
                TileRegionInformation = new Dictionary<Coords, TileRegionInfo>(),
                FollowerCoords = new HashSet<Coords>(),
                OpenCoords = new HashSet<Coords>()
            };

            var tileRegionInfo = result.TileRegionInformation;

            var stack = new Stack<RegionCoords>();

            // Počáteční krok
            stack.Push(new RegionCoords
            {
                Coords = initialCoords,
                RegionId = initialRegionId
            });

            while (stack.Count > 0)
            {
                var currentRegion = stack.Pop();
                var currentCoords = currentRegion.Coords;
                var currentRegionId = currentRegion.RegionId;

                // Pojistka pro počíteční krok
                if (!grid.TryGetValue(currentCoords, out var currentTilePlacement))
                {
                    throw new InvalidOperationException("Invalid coordinates of region.");
                }

                if (tileRegionInfo.TryGetValue(currentCoords, out var currentTileInfo))
                {
                    // Pokud jsme již navštívili totožný region, není třeba prohedávat znovu
                    // Jinak navštěvujeme stejnou kartičku, ale jinou část
                    if (currentTileInfo.RegionIds.Contains(currentRegionId))
                    {
                        continue;
                    }
                }
                else
                {
                    // Jinak procházíme novou kartičku
                    currentTileInfo = new TileRegionInfo
                    {
                        RegionIds = new SortedSet<int>(),
                        NeighboringRegionIds = new SortedSet<int>(),
                        CitiesIds = new SortedSet<int>()
                    };

                    tileRegionInfo.Add(currentCoords, currentTileInfo);
                }

                // Brzce zkontrolujeme figurku
                if (currentTilePlacement.FollowerPlacement != null && currentTilePlacement.FollowerPlacement.RegionId == currentRegionId)
                {
                    result.FollowerCoords.Add(currentCoords);

                    if (stopWhenOccupied)
                    {
                        return result;
                    }
                }

                // Přidáme informace o kartičce
                currentTileInfo.RegionIds.Add(currentRegionId);

                var currentOrientation = currentTilePlacement.Orientation;
                var currentScheme = currentTilePlacement.Tile;

                foreach (var cId in currentScheme.GetRegionCities(currentRegionId))
                {
                    currentTileInfo.CitiesIds.Add(cId);
                }

                foreach (var rId in currentScheme.GetRegionNeighbors(currentRegionId))
                {
                    currentTileInfo.NeighboringRegionIds.Add(rId);
                }

                // Pokračujeme na sousedních kartičkách
                var borders = currentScheme.GetRegionBorders(currentRegionId);
                foreach (var border in CardinalDirections)
                {
                    // Pokud kartička nepokračuje touto stranou, tak nepokračujeme
                    if ((border & borders) != border)
                    {
                        continue;
                    }

                    // Jinak vezmeme sousední pozici
                    var neighDir = border.Rotate(currentOrientation);
                    var neighCoords = currentCoords.GetNeighboringCoords(neighDir);

                    // Jestli na sousední pozici je katička, přidáme na zásobník
                    if (grid.TryGetValue(neighCoords, out var neighTilePlacement))
                    {
                        var neighTileBorder = neighDir.Rotate(TileOrientation.S).Derotate(neighTilePlacement.Orientation);

                        var neighRegionId = neighTilePlacement.Tile.GetRegionOnBorder(neighTileBorder);

                        stack.Push(new RegionCoords
                        {
                            Coords = neighCoords,
                            RegionId = neighRegionId
                        });
                    }
                    else
                    {
                        // Jinak není, přidáme mezi otevřené souřadnice
                        result.OpenCoords.Add(neighCoords);

                        if (stopWhenOpen)
                        {
                            return result;
                        }
                    }
                }
            }

            return result;
        }
    }
}