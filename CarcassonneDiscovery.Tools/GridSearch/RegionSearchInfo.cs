﻿namespace CarcassonneDiscovery.Tools
{
    using System.Collections.Generic;

    /// <summary>
    /// Informace o regionu na kartičce.
    /// </summary>
    public class TileRegionInfo
    {
        /// <summary>
        /// Identifikátory regionů na kartičce, které jsou součástí daného regionu.
        /// </summary>
        public SortedSet<int> RegionIds { get; set; }

        /// <summary>
        /// Identifikátory regionů na kartičce, které sousedí s daným regionem.
        /// </summary>
        public SortedSet<int> NeighboringRegionIds { get; set; }

        /// <summary>
        /// Identifikátory měst na kartičce, které se nachází na daném regionu.
        /// </summary>
        public SortedSet<int> CitiesIds { get; set; }
    }
}