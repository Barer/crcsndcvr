﻿namespace CarcassonneDiscovery.Tools
{
    /// <summary>
    /// Porušení pravidel na mapě.
    /// </summary>
    public enum PlacementRuleViolation
    {
        /// <summary>
        /// Bez porušení pravidel.
        /// </summary>
        Ok,

        /// <summary>
        /// Na daných souřadnicích se již nachází kartička.
        /// </summary>
        NotEmptyCoords,

        /// <summary>
        /// Typy regionů na sousedních kartičkách nejsou kompatibilní.
        /// </summary>
        IncompatibleSurroundings,

        /// <summary>
        /// Kartička nemá žádnou sousední kartičku.
        /// </summary>
        NoNeighboringTile,
    }
}
