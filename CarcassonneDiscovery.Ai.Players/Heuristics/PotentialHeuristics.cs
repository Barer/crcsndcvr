﻿namespace CarcassonneDiscovery.Ai.Players
{
    using CarcassonneDiscovery.Entity;
    using CarcassonneDiscovery.Tools;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Heursitika zhodnocující potenciální skóre figurky.
    /// </summary>
    public class PotentialHeuristics : IHeuristics
    {
        /// <inheritdoc />
        public string Name => "Potential";

        /// <summary>
        /// Seznam světových stran.
        /// </summary>
        protected static readonly TileOrientation[] CardinalDirections = new TileOrientation[4] { TileOrientation.N, TileOrientation.E, TileOrientation.S, TileOrientation.W };

        /// <summary>
        /// Seznam zbývajících kartiček v průběhu tahu.
        /// </summary>
        protected IList<ITileScheme> RemainingTiles;

        /// <summary>
        /// Informace o regionech na začátku tahu (po výběru kartičky).
        /// </summary>
        protected IDictionary<RegionCoords, RegionStats> InitialStats;

        /// <summary>
        /// Otevřené souřadnice pro regiony na začátku tahu (po výběru kartičky).
        /// </summary>
        protected IDictionary<Coords, HashSet<RegionStats>> InitialOpenCoords;

        /// <summary>
        /// Potenciálové ohodnocení figurek na začátku tahu (po výběru kartičky).
        /// </summary>
        protected IDictionary<Coords, FollowerPlacementScore> InitialFollowers;

        /// <summary>
        /// Potenciálové ohodnocení obsazených a obsaditelných regionů na začátku tahu (po výběru kartičky).
        /// </summary>
        protected IDictionary<RegionStats, Tuple<double, double>> InitialRelevantRegionScoringPotentials;

        /// <summary>
        /// Informace o regionech, které byly ovlivněny umístěnou kartičkou.
        /// </summary>
        protected IDictionary<RegionCoords, RegionStats> ModifiedStats;

        /// <summary>
        /// Potenciálové ohodnocení figurek, které byly ovlivněny umístěnou kartičkou.
        /// </summary>
        protected IDictionary<Coords, double> ModifiedFollowers;

        /// <summary>
        /// Potenciálové ohodnocení obsazených a obsaditelných regionů po umístění kartičky.
        /// </summary>
        protected IDictionary<RegionStats, Tuple<double, double>> FinalRelevantRegionScoringPotentials;

        /// <summary>
        /// Páska pro použití potenciálové funkce.
        /// </summary>
        protected PotentialFormulaTape FormulaTape;

        /// <inheritdoc />
        public void Init(int gameId, PlayerColor color)
        {
            FormulaTape = PotentialFormulaTape.GetOrCreate(gameId, color);
        }

        /// <inheritdoc />
        public void InitState(GameState state)
        {
            FormulaTape.ResetBranches();

            RemainingTiles = StandardTileSet.StandardTileGenerator.GenerateStandardTileSet()
                .Where(x => state.Grid.All(kv => kv.Value.Tile.Id != x.Id) && state.Tile.Id != x.Id).ToList();

            InitialStats = new Dictionary<RegionCoords, RegionStats>();
            InitialOpenCoords = new Dictionary<Coords, HashSet<RegionStats>>();
            InitialFollowers = new Dictionary<Coords, FollowerPlacementScore>();
            InitialRelevantRegionScoringPotentials = new Dictionary<RegionStats, Tuple<double, double>>();

            // Předpočítej všechny informace z mapy kartiček
            foreach (var tilePlacement in state.Grid.Values)
            {
                var coords = tilePlacement.Coords;
                var regionCount = tilePlacement.Tile.RegionCount;

                for (var regionId = 0; regionId < regionCount; regionId++)
                {
                    var regionCoords = new RegionCoords
                    {
                        Coords = coords,
                        RegionId = regionId
                    };

                    // Pokud jsme region zatím nenavštívili
                    if (!InitialStats.TryGetValue(regionCoords, out var coordsStats))
                    {
                        coordsStats = GridSearch.GetRegionStats(state.Grid, coords, regionId);

                        // Přidáme informace pro všechny části regionu
                        foreach (var statsRegionCoords in coordsStats.RegionCoords)
                        {
                            InitialStats.Add(statsRegionCoords, coordsStats);
                        }

                        // Pokud je region obsaditelný nebo obsazený, zaregistruj jeho potenciál
                        if (coordsStats.OpenCoords.Any() || coordsStats.FollowerCount > 0)
                        {
                            var potential = ApplyFormula(state, coordsStats);
                            var scoringPotential = potential * PlacingProbability(state.Grid, RemainingTiles, coordsStats.OpenCoords);
                            InitialRelevantRegionScoringPotentials.Add(coordsStats, new Tuple<double, double>(potential, scoringPotential));
                        }

                        // Předpočítej volné souřadnice regionu
                        IEnumerable<Coords> allOpenCoords = coordsStats.OpenCoords;
                        if (coordsStats.Type == RegionType.Mountain)
                        {
                            allOpenCoords = allOpenCoords.Union(coordsStats.OpenGrasslandCoords);
                        }
                        foreach (var openCoords in allOpenCoords)
                        {
                            if (!InitialOpenCoords.TryGetValue(openCoords, out var regionStatsSet))
                            {
                                regionStatsSet = new HashSet<RegionStats>();
                                InitialOpenCoords.Add(openCoords, regionStatsSet);
                            }

                            regionStatsSet.Add(coordsStats);
                        }
                    }
                }
            }

            // Předpočítej všechny informace o figurkách
            foreach (var follower in state.PlacedFollowers.Values.SelectMany(x => x))
            {
                var regionCoords = new RegionCoords
                {
                    Coords = follower.Coords,
                    RegionId = follower.RegionId
                };
                var regionStats = InitialStats[regionCoords];
                var regionPotential = InitialRelevantRegionScoringPotentials[regionStats].Item1;

                InitialFollowers.Add(follower.Coords, new FollowerPlacementScore
                {
                    Color = follower.Color,
                    Coords = follower.Coords,
                    RegionId = follower.RegionId,
                    Score = regionStats.Score,
                    Potential = regionPotential,
                    OpenCoords = regionStats.OpenCoords
                });
            }
        }

        /// <summary>
        /// Aplikuje potenciálovou funkci na region.
        /// </summary>
        /// <param name="state">Aktuální stav hry.</param>
        /// <param name="stats">Statistiky regionu.</param>
        /// <returns>Potenciálová hodnota regionu.</returns>
        protected double ApplyFormula(GameState state, RegionStats stats)
        {
            var input = new PotentialFormulaInput
            {
                Coords = stats.RegionCoords[0].Coords,
                RegionId = stats.RegionCoords[0].RegionId,
                RegionType = stats.Type,
                TileCount = stats.TileCount,
                CityCount = stats.CityCount,
                OpenCoordsPossibilities = OpenCoordsRemainingTileCount(state.Grid, RemainingTiles, stats.OpenCoords),
                GrasslandCityCount = (stats.Type == RegionType.Mountain) ? stats.GrasslandCityCount : 0,
                GrasslandOpenCoordsPossibilities = (stats.Type == RegionType.Mountain) ? OpenCoordsRemainingTileCount(state.Grid, RemainingTiles, stats.OpenGrasslandCoords) : null,
                PlayerCount = state.Params.PlayerOrder.Length,
                RemainingTiles = RemainingTiles.Count
            };

            return FormulaTape.Compute(input);
        }

        /// <summary>
        /// Vrátí potenciální skóre za obsaditelné regiony.
        /// </summary>
        /// <param name="state">Aktuální stav hry.</param>
        /// <param name="include">Přidej region, který by byl obvykle zařazen.</param>
        /// <param name="includeScoringPotential">Potenciál přidaného regionu.</param>
        /// <param name="exclude">Vyřaď region, který by byl obvykle zařazen.</param>
        protected IDictionary<PlayerColor, double> UnoccupiedRegionsPotentials(GameState state, RegionStats include = null, double includeScoringPotential = 0, RegionStats exclude = null)
        {
            // Získej aktuální seznam obsaditelných regionů
            IEnumerable<KeyValuePair<RegionStats, Tuple<double, double>>> unoccupiedRegions = FinalRelevantRegionScoringPotentials.Where(x => x.Key.OpenCoords.Count > 0 && x.Key.FollowerCount == 0);

            if (include != null)
            {
                unoccupiedRegions = unoccupiedRegions.Union(new KeyValuePair<RegionStats, Tuple<double, double>>[] { new KeyValuePair<RegionStats, Tuple<double, double>>(include, new Tuple<double, double>(0, includeScoringPotential)) });
            }

            if (exclude != null)
            {
                unoccupiedRegions = unoccupiedRegions.Where(x => x.Key != exclude);
            }

            var players = state.Params.PlayerOrder;
            var playerCount = players.Length;

            var enumerator = unoccupiedRegions.Select(x => new Tuple<string, Tuple<double, double>>(string.Join("+", x.Key.RegionCoords), x.Value)).OrderByDescending(x => x.Item2.Item2).GetEnumerator();

            var followers = players.ToDictionary(x => x, x => state.Params.FollowerCount - state.PlacedFollowers[x].Count);

            var potentials = players.ToDictionary(x => x, x => 0.0);

            for (int i = (state.PlayerOnMove + 1) % playerCount, remainingTiles = RemainingTiles.Count - 1; remainingTiles > 0; i = (i + 1) % playerCount, remainingTiles--)
            {
                var onMove = players[i];

                // Pokud by hráč neměl figurku, přidej ji - jinak odeber figurku a přidej potenciál
                if (--followers[onMove] == -1)
                {
                    followers[onMove] = 1;
                    continue;
                }

                // Pokud již není obsaditelný region, ukonči
                if (!enumerator.MoveNext())
                {
                    break;
                }

                potentials[onMove] += enumerator.Current.Item2.Item2;
            }
            return potentials;
        }

        /// <inheritdoc />
        public void InitTileMove(GameState modifiedState, TileMove tileMove)
        {
            FormulaTape.InitBranch(tileMove.Coords, tileMove.Orientation);

            FinalRelevantRegionScoringPotentials = InitialRelevantRegionScoringPotentials.ToDictionary(kv => kv.Key, kv => kv.Value);
            ModifiedStats = new Dictionary<RegionCoords, RegionStats>();

            // Přepočítej všechny regiony na aktuální a okolních kartičkách
            var coordsToRecompute = CardinalDirections.Select(or => tileMove.Coords.GetNeighboringCoords(or)).Union(new Coords[] { tileMove.Coords });
            foreach (var coords in coordsToRecompute)
            {
                // Na okoní souřadnici není katička
                if (!modifiedState.Grid.TryGetValue(coords, out var tilePlacement))
                {
                    continue;
                }

                var tileScheme = tilePlacement.Tile;

                for (var rId = 0; rId < tileScheme.RegionCount; rId++)
                {
                    var newStats = GridSearch.GetRegionStats(modifiedState.Grid, coords, rId);

                    // Pokud jsme region zatím nenavštívili v novém přehledu, přidáme ho pro všechny souřadnice
                    if (!ModifiedStats.ContainsKey(newStats.RegionCoords[0]))
                    {
                        var potential = ApplyFormula(modifiedState, newStats);
                        var scoringPotential = potential * PlacingProbability(modifiedState.Grid, RemainingTiles, newStats.OpenCoords);
                        var scoringPotentialTuple = new Tuple<double, double>(potential, scoringPotential);

                        foreach (var regionCoords in newStats.RegionCoords)
                        {
                            ModifiedStats.Add(regionCoords, newStats);

                            // A zároveň přepíšeme všechny dřívější (i opakovaně, protože mohlo dojít ke sloučení regionů)
                            if (InitialStats.TryGetValue(regionCoords, out var oldStats))
                            {
                                FinalRelevantRegionScoringPotentials.Remove(oldStats);
                            }
                        }

                        FinalRelevantRegionScoringPotentials.TryAdd(newStats, scoringPotentialTuple);
                    }
                }
            }

            ModifiedFollowers = new Dictionary<Coords, double>();

            foreach (var follower in InitialFollowers.Values)
            {
                if (follower.OpenCoords.Contains(tileMove.Coords))
                {
                    var followerRegionCoords = new RegionCoords
                    {
                        Coords = follower.Coords,
                        RegionId = follower.RegionId
                    };

                    var potentialBefore = follower.Potential;
                    var potentialAfter = FinalRelevantRegionScoringPotentials[ModifiedStats[followerRegionCoords]].Item1;

                    ModifiedFollowers.Add(follower.Coords, potentialAfter);
                }
            }
        }

        /// <inheritdoc />
        public HeuristicValue GetValue(GameState modifiedState, TileMove tileMove, FollowerMove followerMove)
        {
            switch (followerMove.Type)
            {
                case FollowerMoveType.Place:
                    return GetValueForPlaceFollower(modifiedState, tileMove, followerMove);
                case FollowerMoveType.Remove:
                    return GetValueForRemoveFollower(modifiedState, tileMove, followerMove);
                case FollowerMoveType.Pass:
                    return GetValueForPassMove(modifiedState, tileMove, followerMove);
            }

            return new HeuristicValue();
        }

        /// <summary>
        /// Získá heuristickou hodnotu stavu po položení figurky.
        /// </summary>
        /// <param name="modifiedState">Modifikovaný stav hry.</param>
        /// <param name="tileMove">Část tahu s kartičku.</param>
        /// <param name="followerMove">Část tahu s figurkou.</param>
        /// <returns>Heuristická hodnota tahu pro jednotlivé hráče.</returns>
        public HeuristicValue GetValueForPlaceFollower(GameState modifiedState, TileMove tileMove, FollowerMove followerMove)
        {
            var regionCoords = new RegionCoords
            {
                Coords = followerMove.Coords,
                RegionId = followerMove.RegionId
            };

            var stats = ModifiedStats[regionCoords];

            // Vezmi základní skóre
            var values = modifiedState.Score.ToDictionary(kv => kv.Key, kv => (double)kv.Value);

            // Přičti bonus za obsaditelné regiony
            var unoccupiedRegionsPotential = UnoccupiedRegionsPotentials(modifiedState, exclude: stats);
            foreach (var kv in unoccupiedRegionsPotential)
            {
                values[kv.Key] += kv.Value;
            }

            // Přičti bonus za umístěné figurky
            foreach (var follower in InitialFollowers.Values)
            {
                if (!ModifiedFollowers.TryGetValue(follower.Coords, out var modifiedPotential))
                {
                    modifiedPotential = follower.Potential;
                }

                values[follower.Color] += modifiedPotential;
            }

            // Přičti aktuální figurku
            values[tileMove.Color] += FinalRelevantRegionScoringPotentials[stats].Item1;
            
            return HeuristicValue.FromScores(values);
        }

        /// <summary>
        /// Získá heuristickou hodnotu stavu po odebrání figurky.
        /// </summary>
        /// <param name="modifiedState">Modifikovaný stav hry.</param>
        /// <param name="tileMove">Část tahu s kartičku.</param>
        /// <param name="followerMove">Část tahu s figurkou.</param>
        /// <returns>Heuristická hodnota tahu pro jednotlivé hráče.</returns>
        public HeuristicValue GetValueForRemoveFollower(GameState modifiedState, TileMove tileMove, FollowerMove followerMove)
        {
            var regionCoords = new RegionCoords
            {
                Coords = followerMove.Coords,
                RegionId = followerMove.RegionId
            };

            // Získej region, ze kterého byla figurka odebrána
            if (!ModifiedStats.TryGetValue(regionCoords, out var include))
            {
                include = InitialStats[regionCoords];
            }

            IDictionary<PlayerColor, double> unoccupiedRegionsPotential;
            // Pokud po odebrání figurky zůstane obsaditelný region, přidej ho do soupisu
            if (include.FollowerCount == 1 || include.OpenCoords.Count > 0)
            {
                var includePotential = FinalRelevantRegionScoringPotentials[include].Item2;
                unoccupiedRegionsPotential = UnoccupiedRegionsPotentials(modifiedState, include, includePotential);
            }
            else
            {
                unoccupiedRegionsPotential = UnoccupiedRegionsPotentials(modifiedState);
            }

            // Vezmi základní skóre
            var values = modifiedState.Score.ToDictionary(kv => kv.Key, kv => (double)kv.Value);

            // Přičti bonus za obsaditelné regiony
            foreach (var kv in unoccupiedRegionsPotential)
            {
                values[kv.Key] += kv.Value;
            }

            // Přičti bonus za umístěné figurky
            foreach (var follower in InitialFollowers.Values)
            {
                // Přeskoč odebranou figurku
                if (follower.Color == followerMove.Color && follower.Coords == followerMove.Coords)
                {
                    continue;
                }

                if (!ModifiedFollowers.TryGetValue(follower.Coords, out var modifiedPotential))
                {
                    modifiedPotential = follower.Potential;
                }

                values[follower.Color] += modifiedPotential;
            }

            return HeuristicValue.FromScores(values);
        }

        /// <summary>
        /// Získá heuristickou hodnotu stavu po předání tahu.
        /// </summary>
        /// <param name="modifiedState">Modifikovaný stav hry.</param>
        /// <param name="tileMove">Část tahu s kartičku.</param>
        /// <param name="followerMove">Část tahu s figurkou.</param>
        /// <returns>Heuristická hodnota tahu pro jednotlivé hráče.</returns>
        public HeuristicValue GetValueForPassMove(GameState modifiedState, TileMove tileMove, FollowerMove followerMove)
        {
            // Vezmi základní skóre
            var values = modifiedState.Score.ToDictionary(kv => kv.Key, kv => (double)kv.Value);

            // Přičti bonus za obsaditelné regiony
            var potentialsAfter = UnoccupiedRegionsPotentials(modifiedState);
            foreach (var kv in potentialsAfter)
            {
                values[kv.Key] += kv.Value;
            }

            // Přičti bonus za umístěné figurky
            foreach (var follower in InitialFollowers.Values)
            {
                if (!ModifiedFollowers.TryGetValue(follower.Coords, out var modifiedPotential))
                {
                    modifiedPotential = follower.Potential;
                }

                values[follower.Color] += modifiedPotential;
            }

            return HeuristicValue.FromScores(values);
        }

        /// <summary>
        /// Vypočítá počet zbývajících kartiček umístitelných na dané pozice.
        /// </summary>
        /// <param name="grid">Mapa umístěných kartiček.</param>
        /// <param name="remainingTiles">Zbývající kartičky.</param>
        /// <param name="openCoords">Souřadnice.</param>
        /// <returns>Počet zbývajících kartiček umístitelných na dané pozici.</returns>
        protected static IList<int> OpenCoordsRemainingTileCount(IDictionary<Coords, TilePlacement> grid, IEnumerable<ITileScheme> remainingTiles, IEnumerable<Coords> openCoords)
        {
            return openCoords.Select(coords => remainingTiles.Where(tile =>
            {
                foreach (var orientation in CardinalDirections)
                {
                    if (GridSearch.CheckTilePlacement(grid, tile, coords, orientation) == PlacementRuleViolation.Ok)
                    {
                        return true;
                    }
                }
                return false;
            }).Count()).ToList();
        }

        /// <summary>
        /// Vypočítá šanci na obsazení obsaditelného regionu pomocí jedné kartičky.
        /// </summary>
        /// <param name="grid">Mapa umístěných kartiček.</param>
        /// <param name="remainingTiles">Zbývající kartičky.</param>
        /// <param name="openCoords">Souřadnice volných míst regionu.</param>
        /// <returns>Šance na obsazení obsaditelného regionu.</returns>
        protected double PlacingProbability(IDictionary<Coords, TilePlacement> grid, IList<ITileScheme> remainingTiles, IEnumerable<Coords> openCoords)
        {
            return remainingTiles.Count == 0 ? 0 : (double)openCoords.SelectMany(coords => remainingTiles.Where(tile =>
            {
                foreach (var orientation in CardinalDirections)
                {
                    if (GridSearch.CheckTilePlacement(grid, tile, coords, orientation) == PlacementRuleViolation.Ok)
                    {
                        return true;
                    }
                }
                return false;
            })).Distinct().Count() / remainingTiles.Count;
        }
    }
}
