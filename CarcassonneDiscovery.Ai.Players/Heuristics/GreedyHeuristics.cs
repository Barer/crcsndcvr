﻿namespace CarcassonneDiscovery.Ai.Players
{
    using CarcassonneDiscovery.Entity;
    using CarcassonneDiscovery.Tools;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Heuristika, který ze všech dostupných tahů vybere ten, po kterém bude mít hráč nejlepší skóre.
    /// </summary>
    public class GreedyHeuristics : IHeuristics
    {
        /// <inheritdoc />
        public string Name => "Greedy";

        /// <summary>
        /// Bodové ohodnocení hráčů na začátku tahu.
        /// </summary>
        protected IDictionary<PlayerColor, double> InitialScores;

        /// <summary>
        /// Bodové ohodnocení hráčů po položení kartičky.
        /// </summary>
        protected IDictionary<PlayerColor, double> ModifiedScores;

        /// <summary>
        /// Bodové ohodnocení figurek.
        /// </summary>
        protected IDictionary<Coords, FollowerPlacementScore> Followers;

        /// <inheritdoc />
        public void Init(int gameId, PlayerColor color)
        {
	        // Nic není třeba
        }

		/// <inheritdoc />
		public void InitState(GameState state)
        {
            InitialScores = state.Score.ToDictionary(kv => kv.Key, kv => (double)kv.Value);
            ModifiedScores = null;
            Followers = state.PlacedFollowers.Values.SelectMany(x => x).ToDictionary(fp => fp.Coords, fp =>
                {
                    var regionScore = GridSearch.GetRegionStats(state.Grid, fp.Coords, fp.RegionId, true);

                    return new FollowerPlacementScore
                    {
                        Color = fp.Color,
                        Coords = fp.Coords,
                        RegionId = fp.RegionId,
                        Score = regionScore.Score,
                        OpenCoords = regionScore.OpenCoords
                    };
                });

            foreach (var follower in Followers.Values)
            {
                InitialScores[follower.Color] += follower.Score;
            }
        }

        /// <inheritdoc />
        public void InitTileMove(GameState modifiedState, TileMove tileMove)
		{
			ModifiedScores = InitialScores.ToDictionary(kv => kv.Key, kv => kv.Value);

			foreach (var follower in Followers.Values)
			{
				if (follower.OpenCoords.Contains(tileMove.Coords))
				{
					ModifiedScores[follower.Color] -= (int)follower.Score;
					ModifiedScores[follower.Color] += GridSearch.GetRegionStats(modifiedState.Grid, follower.Coords, follower.RegionId, true).Score;
				}
			}
		}

        /// <inheritdoc />
        public HeuristicValue GetValue(GameState modifiedState, TileMove tileMove, FollowerMove followerMove)
        {
            var scores = ModifiedScores.ToDictionary(kv => kv.Key, kv => (double)kv.Value);

			switch (followerMove.Type)
            {
				case FollowerMoveType.Place:
					scores[followerMove.Color] += GridSearch.GetRegionStats(modifiedState.Grid, followerMove.Coords, followerMove.RegionId, true).Score;
					break;

				case FollowerMoveType.Remove:
					scores[followerMove.Color] -= GridSearch.GetRegionStats(modifiedState.Grid, followerMove.Coords, followerMove.RegionId, true).Score;
					scores[followerMove.Color] += GridSearch.GetRegionStats(modifiedState.Grid, followerMove.Coords, followerMove.RegionId, false).Score;
					break;
			}

			return HeuristicValue.FromScores(scores);
        }
    }
}
