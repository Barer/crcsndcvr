﻿namespace CarcassonneDiscovery.Ai.Players
{
    using CarcassonneDiscovery.Entity;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Heuristika, který vybírá náhodné hodnoty.
    /// </summary>
    public class RandomHeuristics : IHeuristics
    {
        /// <summary>
        /// Generátor náhodných čísel.
        /// </summary>
        protected Random Rnd;

        /// <inheritdoc />
        public string Name => "Random";

        /// <inheritdoc />
        public void Init(int gameId, PlayerColor color)
        {
            Rnd = new Random();
        }

		/// <inheritdoc />
		public void InitState(GameState state)
        {
	        // Není třeba
        }

		/// <inheritdoc />
		public void InitTileMove(GameState modifiedState, TileMove tileMove)
		{
			// Není třeba
		}

		/// <inheritdoc />
		public HeuristicValue GetValue(GameState modifiedState, TileMove tileMove, FollowerMove followerMove)
		{
            return GetMoveValue(modifiedState.Params.PlayerOrder);
        }

        /// <summary>
        /// Vrát náhodnou heuristickou hodnotu stavu.
        /// </summary>
        /// <param name="players">Hráči ve hře.</param>
        /// <returns>Heuristická hodnota stavu pro jednotlivé hráče.</returns>
        protected HeuristicValue GetMoveValue(IEnumerable<PlayerColor> players)
        {
            return HeuristicValue.FromValues(players.ToDictionary(x => x, x => Rnd.NextDouble()));
        }
    }
}
