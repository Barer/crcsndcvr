﻿namespace CarcassonneDiscovery.Ai.Players
{
    using CarcassonneDiscovery.Entity;

    /// <summary>
    /// Heuristika pro AI se stavem výpočtu.
    /// </summary>
    public interface IHeuristics
    {
        /// <summary>
        /// Název heuristiky.
        /// </summary>
        string Name { get; }

		/// <summary>
		/// Inicializuje výpočet pro hráče.
		/// </summary>
		/// <param name="gameId">Identifikátor hry.</param>
		/// <param name="color">Barva hráče.</param>
        void Init(int gameId, PlayerColor color);

		/// <summary>
        /// Inicializuje výpočet pro stav hry.
        /// </summary>
        /// <param name="state">Stav hry.</param>
        void InitState(GameState state);

		/// <summary>
		/// Inicializuje výpočet pro položenou kartičku.
		/// </summary>
		/// <param name="modifiedState">Modifikovaný stav hry.</param>
		/// <param name="tileMove">Část tahu s kartičku.</param>
		void InitTileMove(GameState modifiedState, TileMove tileMove);

        /// <summary>
        /// Získá heuristickou hodnotu stavu podle provedeného tahu.
        /// </summary>
        /// <param name="modifiedState">Modifikovaný stav hry.</param>
        /// <param name="tileMove">Část tahu s kartičku.</param>
        /// <param name="followerMove">Část tahu s figurkou.</param>
        /// <returns>Heuristická hodnota tahu pro jednotlivé hráče.</returns>
        HeuristicValue GetValue(GameState modifiedState, TileMove tileMove, FollowerMove followerMove);
    }
}
