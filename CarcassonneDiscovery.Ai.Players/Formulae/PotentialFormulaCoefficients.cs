﻿using System;

namespace CarcassonneDiscovery.Ai.Players
{
    /// <summary>
    /// Koeficienty potenciálové funkce.
    /// </summary>
    public class PotentialFormulaCoefficients
    {
        /// <summary>
        /// Konstantní člen levé části pro velikost regionu.
        /// </summary>
        public double Av { get; set; }

        /// <summary>
        /// Lineární člen levé části pro velikost regionu.
        /// </summary>
        public double Bv { get; set; }

        /// <summary>
        /// Normalizce pravé části pro velikost regionu.
        /// </summary>
        public double Cv { get; set; }

        /// <summary>
        /// Konstantní člen levé části pro počet měst na regionu.
        /// </summary>
        public double Am { get; set; }

        /// <summary>
        /// Lineární člen levé části pro počet měst na regionu.
        /// </summary>
        public double Bm { get; set; }

        /// <summary>
        /// Normalizce pravé části pro počet měst na regionu.
        /// </summary>
        public double Cm { get; set; }

        /// <summary>
        /// Konstantní člen levé části pro počet měst na přilehlých nížinách.
        /// </summary>
        public double An { get; set; }

        /// <summary>
        /// Lineární člen levé části pro počet měst na přilehlých nížinách.
        /// </summary>
        public double Bn { get; set; }

        /// <summary>
        /// Normalizce pravé části pro počet měst na přilehlých nížinách.
        /// </summary>
        public double Cn { get; set; }

        /// <summary>
        /// Koeficient výskytu měst na regionu.
        /// </summary>
        public double Hm { get; set; }

        /// <summary>
        /// Konstantní člen normalizace indikátoru velikosti.
        /// </summary>
        public double Ai { get; set; }

        /// <summary>
        /// Lineární člen normalizace indikátoru velikosti.
        /// </summary>
        public double Bi { get; set; }

        /// <summary>
        /// Konstantní člen normalizace pravděpodobnosti uzavření regionu.
        /// </summary>
        public double Apu { get; set; }

        /// <summary>
        /// Lineární člen normalizace pravděpodobnosti uzavření regionu.
        /// </summary>
        public double Bpu { get; set; }

        /// <summary>
        /// Konstantní člen normalizace střední hodnoty rozšíření regionu.
        /// </summary>
        public double Apr { get; set; }

        /// <summary>
        /// Lineární člen normalizace střední hodnoty rozšíření regionu.
        /// </summary>
        public double Bpr { get; set; }

        /// <summary>
        /// Konstantní člen normalizace střední hodnoty rozšíření přilehlé nížiny.
        /// </summary>
        public double Apg { get; set; }

        /// <summary>
        /// Lineární člen normalizace střední hodnoty rozšíření přilehlé nížiny.
        /// </summary>
        public double Bpg { get; set; }

        /// <summary>
        /// Defaultní kontruktor.
        /// </summary>
        public PotentialFormulaCoefficients()
        {
            // Prázdné
        }

        /// <summary>
        /// Konstruktor z pole koeficientů.
        /// </summary>
        /// <param name="coeff">Pole koeficientů</param>
        public PotentialFormulaCoefficients(double[] coeff)
        {
            if (coeff == null || coeff.Length != 18)
            {
                throw new ArgumentException("Invalid coefficient array.");
            }

            Av = coeff[0];
            Bv = coeff[1];
            Cv = coeff[2];
            Am = coeff[3];
            Bm = coeff[4];
            Cm = coeff[5];
            An = coeff[6];
            Bn = coeff[7];
            Cn = coeff[8];
            Hm = coeff[9];
            Ai = coeff[10];
            Bi = coeff[11];
            Apu = coeff[12];
            Bpu = coeff[13];
            Apr = coeff[14];
            Bpr = coeff[15];
            Apg = coeff[16];
            Bpg = coeff[17];
        }

        /// <summary>
        /// Převede koeficienty na pole.
        /// </summary>
        /// <returns>Pole koeficientů.</returns>
        public double[] ToArray()
        {
            return new double[18]
            {
                Av, Bv, Cv,
                Am, Bm, Cm,
                An, Bn, Cn,
                Hm,
                Ai, Bi,
                Apu, Bpu,
                Apr, Bpr,
                Apg, Bpg
            };
        }

        /// <summary>
        /// Vytvoří kopii koeficientů.
        /// </summary>
        public PotentialFormulaCoefficients Copy()
        {
            return Apply<PotentialFormulaCoefficients>(this, Zero, (l, r) => l);
        }
        /// <summary>
        /// Aplikuje funkci na všechny členy dvojic koeficientů.
        /// </summary>
        /// <typeparam name="Tl">Typ vstupního koeficientu.</typeparam>
        /// <typeparam name="Tr">Typ vstupního koeficientu.</typeparam>
        /// <typeparam name="Tout">Typ výstupního koeficientu.</typeparam>
        /// <param name="left">Levé operandy.</param>
        /// <param name="right">Pravé operandy.</param>
        /// <param name="func">Funkce.</param>
        public static Tout Apply<Tout>(PotentialFormulaCoefficients left, PotentialFormulaCoefficients right, Func<double, double, double> func)
            where Tout : PotentialFormulaCoefficients, new()
        {
            return new Tout
            {
                Av = func.Invoke(left.Av, right.Av),
                Bv = func.Invoke(left.Bv, right.Bv),
                Cv = func.Invoke(left.Cv, right.Cv),
                Am = func.Invoke(left.Am, right.Am),
                Bm = func.Invoke(left.Bm, right.Bm),
                Cm = func.Invoke(left.Cm, right.Cm),
                An = func.Invoke(left.An, right.An),
                Bn = func.Invoke(left.Bn, right.Bn),
                Cn = func.Invoke(left.Cn, right.Cn),
                Hm = func.Invoke(left.Hm, right.Hm),
                Ai = func.Invoke(left.Ai, right.Ai),
                Bi = func.Invoke(left.Bi, right.Bi),
                Apu = func.Invoke(left.Apu, right.Apu),
                Bpu = func.Invoke(left.Bpu, right.Bpu),
                Apr = func.Invoke(left.Apr, right.Apr),
                Bpr = func.Invoke(left.Bpr, right.Bpr),
                Apg = func.Invoke(left.Apg, right.Apg),
                Bpg = func.Invoke(left.Bpg, right.Bpg)
            };
        }

        /// <summary>
        /// Nulové koeficienty.
        /// </summary>
        public static PotentialFormulaCoefficients Zero { get; } = new PotentialFormulaCoefficients();
    }
}
