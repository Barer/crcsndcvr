﻿namespace CarcassonneDiscovery.Ai.Players
{
    /// <summary>
    /// Vzorce pro potenciálovou heuristiku.
    /// </summary>
    public class PotentialFormulaEvaluationParts
    {
        
/// <summary>
/// Indikátor velikosti regionu.
/// </summary>
public double RawI { get; set; }

        /// <summary>
        /// Pravděpodobnost uzavření regionu.
        /// </summary>
public double RawPu { get; set; }

        /// <summary>
        /// Střední hodnota rozšíření regionu.
        /// </summary>
public double RawPr { get; set; }

        /// <summary>
        /// Střední hodnota rozšíření přilehlé nížiny.
        /// </summary>
        public double RawPg { get; set; }

        /// <summary>
        /// Normalizovaný indikátor velikosti regionu.
        /// </summary>
        public double I { get; set; }

        /// <summary>
        /// Normalizovaná pravděpodobnost uzavření regionu.
        /// </summary>
        public double Pu { get; set; }

        /// <summary>
        /// Normalizovaná střední hodnota rozšíření přilehlé nížiny.
        /// </summary>
        public double Pr { get; set; }


        /// <summary>
        /// Normalizovaná střední hodnota rozšíření přilehlé nížiny.
        /// </summary>
        public double Pg { get; set; }

        /// <summary>
        /// Součet indikátoru velikosti a pravděpodobnosti uzavření.
        /// </summary>
        public double IPu => I + Pu;

        /// <summary>
        /// Levá strana části pro velikost regionu.
        /// </summary>
        public double Vl { get; set; }

        /// <summary>
        /// Pravá strana části pro velikost regionu.
        /// </summary>
        public double Vr { get; set; }

        /// <summary>
        /// Levá strana části pro města na regionu.
        /// </summary>
        public double Ml { get; set; }

        /// <summary>
        /// Pravá strana části pro města na regionu.
        /// </summary>
        public double Mr { get; set; }

        /// <summary>
        /// Levá strana části pro města přilehlých nížinách.
        /// </summary>
        public double Nl { get; set; }

        /// <summary>
        /// Pravá strana části pro města na přilehlých nížinách.
        /// </summary>
        public double Nr { get; set; }

        /// <summary>
        /// Výsledný potenciál.
        /// </summary>
        public double Potential { get; set; }
    }
}
