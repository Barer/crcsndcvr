﻿using CarcassonneDiscovery.Entity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CarcassonneDiscovery.Ai.Players
{
    /// <summary>
    /// Vzorce pro potenciálovou heuristiku.
    /// </summary>
    public class PotentialFormula
    {
        /// <summary>
        /// Pomocené pole pro výpočet faktoriálu.
        /// </summary>
        protected static List<int> FactorialArray;

        /// <summary>
        /// Vypočítá faktoriál čísla.
        /// </summary>
        /// <param name="i">Číslo.</param>
        /// <returns>Faktoriál čísla.</returns>
        protected static int Factorial(int i)
        {
            for (var j = FactorialArray.Count; j <= i; i++)
            {
                FactorialArray[j] = FactorialArray[j - 1] * j;
            }

            return FactorialArray[i];
        }

        /// <summary>
        /// Aktuálně používané koeficienty.
        /// </summary>
        public IDictionary<RegionType, PotentialFormulaCoefficients> Coefficients { get; set; }

        /// <summary>
        /// Statický konstruktor.
        /// </summary>
        static PotentialFormula()
        {
            FactorialArray = new List<int> { 1 };
        }

        /// <summary>
        /// Vytvoří výchozí hodnotu koeficentů.
        /// </summary>
        public void UseDefaultCoefficients()
        {
            Coefficients = new Dictionary<RegionType, PotentialFormulaCoefficients>();

            var mountainCoeff = new PotentialFormulaCoefficients
            {
                // Velikost regionu 
                Av = 0,
                Bv = 0,
                Cv = 0,
                // Počet měst
                Am = 1,
                Bm = 1,
                Cm = 1,
                // Počet měst na přilehlých nížinách
                An = 1,
                Bn = 1,
                Cn = 1,
                // Výskyt měst na kartičkách
                Hm = 0.33,
                // Normalizace indikátoru velikosti (zde žádná, použití v učení)
                Ai = 0,
                Bi = 1,
                // Normalizace pravděpodobnost uzavření (zde žádná, použití v učení)
                Apu = 0,
                Bpu = 1,
                // Normalizace střední hodnoty rozšíření regionu (zde žádná, použití v učení)
                Apr = 0,
                Bpr = 1,
                // Normalizace střední hodnoty rozšíření přilehlých nížin (zde žádná, použití v učení)
                Apg = 0,
                Bpg = 1
            };
            Coefficients.Add(RegionType.Mountain, mountainCoeff);

            var grasslandCoeff = new PotentialFormulaCoefficients
            {
                // Velikost regionu 
                Av = 1,
                Bv = 1,
                Cv = 1,
                // Počet měst
                Am = 0,
                Bm = 0,
                Cm = 0,
                // Počet měst na přilehlých nížinách
                An = 0,
                Bn = 0,
                Cn = 0,
                // Výskyt měst na kartičkách
                Hm = 0.33,
                // Normalizace indikátoru velikosti (zde žádná, použití v učení)
                Ai = 0,
                Bi = 1,
                // Normalizace pravděpodobnost uzavření (zde žádná, použití v učení)
                Apu = 0,
                Bpu = 1,
                // Normalizace střední hodnoty rozšíření regionu (zde žádná, použití v učení)
                Apr = 0,
                Bpr = 1,
                // Normalizace střední hodnoty rozšíření přilehlých nížin (zde žádná, použití v učení)
                Apg = 0,
                Bpg = 1
            };
            Coefficients.Add(RegionType.Grassland, grasslandCoeff);

            var seaCoeff = new PotentialFormulaCoefficients
            {
                // Velikost regionu 
                Av = 0,
                Bv = 1,
                Cv = 1,
                // Počet měst
                Am = 1,
                Bm = 0,
                Cm = 1,
                // Počet měst na přilehlých nížinách
                An = 0,
                Bn = 0,
                Cn = 0,
                // Výskyt měst na kartičkách
                Hm = 0.50,
                // Normalizace indikátoru velikosti (zde žádná, použití v učení)
                Ai = 0,
                Bi = 1,
                // Normalizace pravděpodobnost uzavření (zde žádná, použití v učení)
                Apu = 0,
                Bpu = 1,
                // Normalizace střední hodnoty rozšíření regionu (zde žádná, použití v učení)
                Apr = 0,
                Bpr = 1,
                // Normalizace střední hodnoty rozšíření přilehlých nížin (zde žádná, použití v učení)
                Apg = 0,
                Bpg = 1
            };
            Coefficients.Add(RegionType.Sea, seaCoeff);
        }

        /// <summary>
        /// Vytvoří výchozí hodnotu koeficentů pomocí náhodného rozdělení.
        /// </summary>
        /// <param name="lower">Spodní hranice koeficientu.</param>
        /// <param name="upper">Horní hranice koeficientu.</param>
        public void UseRandomCoefficients(double lower, double upper)
        {
            Coefficients = new Dictionary<RegionType, PotentialFormulaCoefficients>();

            var rnd = new Random();

            var len = upper - lower;

            for (var r = 0; r < 3; r++)
            {
                var array = new double[18];

                for (var i = 0; i < 18; i++)
                {
                    array[i] = lower + rnd.NextDouble() * len;
                }

                Coefficients.Add((RegionType)r, new PotentialFormulaCoefficients(array));
            }
        }


        /// <summary>
        /// Načte koeficienty ze vstupního souboru.
        /// </summary>
        /// <param name="sr">Čtení vstupu.</param>
        public void LoadCoefficients(StreamReader sr)
        {
            Coefficients = new Dictionary<RegionType, PotentialFormulaCoefficients>();

            for (var i = 0; i < 3; i++)
            {
                var coeff = sr.ReadLine().Split(';').Select(x => double.Parse(x)).ToArray();

                Coefficients.Add((RegionType)i, new PotentialFormulaCoefficients(coeff));
            }
        }

        /// <summary>
        /// Uloží výchozí hodnoty koeficientů na výstup.
        /// </summary>
        /// <param name="sw">Zápis výstupu.</param>
        public void SaveCoefficients(StreamWriter sw)
        {
            for (var i = 0; i < 3; i++)
            {
                var coeff = string.Join(';', Coefficients[(RegionType)i].ToArray());
                sw.WriteLine(coeff);
            }
        }

        /// <summary>
        /// Vypočtá hodnotu poteciálové funkce a vrátí jednotlivé částené výpočty.
        /// </summary>
        /// <param name="input">Vstup funkce.</param>
        /// <returns>Částečné výpočty.</returns>
        public PotentialFormulaEvaluationParts ComputeParts(PotentialFormulaInput input)
        {
            var result = new PotentialFormulaEvaluationParts();

            var coeff = Coefficients[input.RegionType];

            // Zpracuj vstup statickými vzorci
            result.RawI = SizeIndicator(input);
            result.RawPu = ClosingProbability(input);
            result.RawPr = ExpectedExpandingCount(input, false);
            result.RawPg = ExpectedExpandingCount(input, true);

            // Normalizuj je pomocí koeficientů
            result.I = coeff.Ai + result.RawI * coeff.Bi;
            result.Pu = coeff.Apu + result.RawPu * coeff.Bpu;
            result.Pr = coeff.Apr + result.RawPr * coeff.Bpr;
            result.Pg = coeff.Apg + result.RawPg * coeff.Bpg;

            // Jednotlivé části
            result.Vl = coeff.Av + coeff.Bv * result.IPu;
            result.Vr = coeff.Cv * result.Pr + input.TileCount;
            result.Ml = coeff.Am + coeff.Bm * result.IPu;
            result.Mr = coeff.Cm * result.Pr * coeff.Hm + input.CityCount;
            result.Nl = coeff.An + coeff.Bn * result.IPu;
            result.Nr = coeff.Cn * result.Pg * Coefficients[RegionType.Grassland].Hm + input.GrasslandCityCount;

            // Výsledek
            result.Potential = result.Vl * result.Vr + result.Ml * result.Mr + result.Nl * result.Nr;

            return result;
        }

        /// <summary>
        /// Indikátor velikosti.
        /// </summary>
        /// <param name="input">Vstupní parametry funkce.</param>
        /// <returns>Hodnota indikátoru velikosti.</returns>
        protected virtual double SizeIndicator(PotentialFormulaInput input)
        {
            var big = input.TileCount > 2;
            var closed = input.OpenCoordsPossibilities.Count == 0;

            return big && closed ? 1 : 0;
        }

        /// <summary>
        /// Odhadne pravděpodobnost uzavření regionu.
        /// </summary>
        /// <param name="input">Vstupní parametry funkce.</param>
        /// <returns>Pravděpodobnost uzavření regionu.</returns>
        protected virtual double ClosingProbability(PotentialFormulaInput input)
        {
            if (input.OpenCoordsPossibilities.Count == 0 || input.RemainingTiles == 0)
            {
                return 0;
            }

            // Výpočet provedeme jako doplněk

            var k = input.RemainingTiles / input.PlayerCount;

            var p = Math.Pow(input.OpenCoordsPossibilities.Aggregate(1, (i, j) => i * j, i => i), 1.0 / input.OpenCoordsPossibilities.Count) / input.RemainingTiles;
            var _p = 1 - p;
            var step = p / _p;

            if (_p == 0 || double.IsNaN(step))
            {
                return 1.0;
            }

            var total = 1.0;

            var currentProbability = Math.Pow(_p, k);
            var currentBinom = 1;

            for (var i = 0; i <= input.OpenCoordsPossibilities.Count - 1; i++)
            {
                total -= currentBinom * currentProbability;

                currentProbability *= step;
                currentBinom *= k - i;
                currentBinom /= i + 1;
            }

            return total;
        }

        /// <summary>
        /// Odhadne pravděpodobnost připojení k regionu.
        /// </summary>
        /// <param name="input">Vstupní parametry funkce.</param>
        /// <param name="grassland">Výpočet pro přilehlé nížiny?</param>
        /// <returns>Pravděpodobnost připojení k regionu.</returns>
        public virtual double ExpectedExpandingCount(PotentialFormulaInput input, bool grassland = false)
        {
            if (input.RemainingTiles == 0)
            {
                return 0;
            }

            var tilesOnPerson = input.RemainingTiles / input.PlayerCount;

            var total = 0.0;

            foreach (var tiles in (grassland ? (input.GrasslandOpenCoordsPossibilities ?? new int[0]) : input.OpenCoordsPossibilities))
            {
                var tileUnProbability = 1 - (double)tiles / input.RemainingTiles;
                total += 1 - Math.Pow(tileUnProbability, tilesOnPerson);
            }

            return total;
        }
    }
}
