﻿using CarcassonneDiscovery.Entity;
using System.Collections.Generic;
using System.Linq;

namespace CarcassonneDiscovery.Ai.Players
{
    /// <summary>
    /// Vstup potenciálové funkce.
    /// </summary>
    public class PotentialFormulaInput
    {
        /// <summary>
        /// Souřadnice regionu.
        /// </summary>
        public Coords Coords { get; set; }

        /// <summary>
        /// Identifikátor regionu.
        /// </summary>
        public int RegionId { get; set; }

        /// <summary>
        /// Typ regionu.
        /// </summary>
        public RegionType RegionType { get; set; }

        /// <summary>
        /// Velikost regionu.
        /// </summary>
        public int TileCount { get; set; }

        /// <summary>
        /// Počet měst na regionu.
        /// </summary>
        public int CityCount { get; set; }

        /// <summary>
        /// Počet možností pro umístění kartiček na otevřené souřadnice.
        /// </summary>
        public IList<int> OpenCoordsPossibilities { get; set; }

        /// <summary>
        /// Počet měst na přilehlých nížinách.
        /// </summary>
        public int GrasslandCityCount { get; set; }

        /// <summary>
        /// Počet možností pro umístění kartiček na otevřené souřadnice.
        /// </summary>
        public IList<int> GrasslandOpenCoordsPossibilities { get; set; }

        /// <summary>
        /// Počet hráčů ve hře.
        /// </summary>
        public int PlayerCount { get; set; }

        /// <summary>
        /// Zbývající počet kartiček ve hře.
        /// </summary>
        public int RemainingTiles { get; set; }

        /// <summary>
        /// Vytvoří kopii vstupu.
        /// </summary>
        /// <returns>Kopie vstupu.</returns>
        public PotentialFormulaInput Copy()
        {
            return new PotentialFormulaInput
            {
                Coords = Coords,
                RegionId = RegionId,
                RegionType = RegionType,
                TileCount = TileCount,
                CityCount = CityCount,
                OpenCoordsPossibilities = OpenCoordsPossibilities.ToList(),
                GrasslandCityCount = GrasslandCityCount,
                GrasslandOpenCoordsPossibilities = GrasslandOpenCoordsPossibilities?.ToList() ?? new List<int>(),
                PlayerCount = PlayerCount,
                RemainingTiles = RemainingTiles,
            };
        }
    }
}
