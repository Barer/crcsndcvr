﻿using CarcassonneDiscovery.Entity;
using CarcassonneDiscovery.Tools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CarcassonneDiscovery.Ai.Players
{
    /// <summary>
    /// Páska pro uchování vzorce.
    /// </summary>
    public class PotentialFormulaTape
    {
        /// <summary>
        /// Spodní hranice pro náhodné koeficienty.
        /// </summary>
        protected const double RndCoeffLo = -1;

        /// <summary>
        /// Horní hranice pro náhodné koeficienty.
        /// </summary>
        protected const double RndCoeffHi = 2;

        /// <summary>
        /// Všechny aktivní pásky.
        /// </summary>
        protected static IDictionary<string, PotentialFormulaTape> Tapes;

        /// <summary>
        /// Statický konstruktor.
        /// </summary>
        static PotentialFormulaTape()
        {
            Tapes = new Dictionary<string, PotentialFormulaTape>();
        }

        /// <summary>
        /// Vypočítá klíč pásky.
        /// </summary>
        /// <param name="gameId">Identifikátor hry.</param>
        /// <param name="color">Barva hráče.</param>
        /// <returns>Klíč pásky.</returns>
        protected static string TapeKey(int gameId, PlayerColor color)
        {
            return $"{gameId}-{color}";
        }

        /// <summary>
        /// Načte nebo vytvoří pásku.
        /// </summary>
        /// <param name="gameId">Identifikátor hry.</param>
        /// <param name="color">Barva hráče.</param>
        /// <param name="path">Soubor pro čtení koeficientů; null pokud se mají použít defaultní koeficienty.</param>
        /// <param name="useRandom">Použít náhodné koeficienty?</param>
        /// <returns>Páska.</returns>
        public static PotentialFormulaTape GetOrCreate(int gameId, PlayerColor color, string path = null, bool useRandom = false)
        {
            var key = TapeKey(gameId, color);

            lock (Tapes)
            {
                if (!Tapes.TryGetValue(key, out var tape))
                {
                    tape = new PotentialFormulaTape();
                    Tapes.Add(key, tape);

                    if (path != null)
                    {
                        try
                        {
                            using (var sr = new StreamReader(path))
                            {
                                tape.Formula.LoadCoefficients(sr);
                            }
                        }
                        catch (Exception ex)
                        {
                            if (useRandom)
                            {
                                tape.Formula.UseRandomCoefficients(RndCoeffLo, RndCoeffHi);
                            }
                            else
                            {
                                tape.Formula.UseDefaultCoefficients();
                            }

                            Console.WriteLine("Could not load coefficients:");
                            Console.WriteLine(ex.Message);
                            Console.WriteLine($"Using {(useRandom ? "random" : "default")} coefficients.");
                        }
                    }
                    else
                    {
                        if (useRandom)
                        {
                            tape.Formula.UseRandomCoefficients(-1, 2);
                        }
                        else
                        {
                            tape.Formula.UseDefaultCoefficients();
                        }
                    }
                }

                return tape;
            }
        }

        /// <summary>
        /// Odstraní pásku.
        /// </summary>
        /// <param name="gameId">Identifikátor hry.</param>
        /// <param name="color">Barva hráče.</param>
        public static void Remove(int gameId, PlayerColor color)
        {
            lock (Tapes)
            {
                Tapes.Remove(TapeKey(gameId, color));
            }
        }

        /// <summary>
        /// Je zapnuto ukládání vstupů?
        /// </summary>
        public bool Enabled { get; set; }

        /// <summary>
        /// Vzorec pro výpočty.
        /// </summary>
        protected PotentialFormula Formula;

        /// <summary>
        /// Vstupy.
        /// </summary>
        protected IList<PotentialFormulaInput> Inputs;

        /// <summary>
        /// Větve výpočtů.
        /// </summary>
        protected IDictionary<string, IList<PotentialFormulaInput>> Branches;

        /// <summary>
        /// Aktuální větev.
        /// </summary>
        protected IList<PotentialFormulaInput> CurrentBranch;

        /// <summary>
        /// Konstruktor.
        /// </summary>
        public PotentialFormulaTape()
        {
            Formula = new PotentialFormula();
            Inputs = new List<PotentialFormulaInput>();
            Branches = new Dictionary<string, IList<PotentialFormulaInput>>();
            CurrentBranch = new List<PotentialFormulaInput>();
            Enabled = false;
        }


        /// <summary>
        /// Vypočítá klíč větve.
        /// </summary>
        /// <param name="coords">Souřadnice umístěné kartičky.</param>
        /// <param name="orientation">Orientace kartičky.</param>
        /// <returns>Klíč větve.</returns>
        protected string ComputeKey(Coords coords, TileOrientation orientation)
        {
            return $"{coords.X},{coords.Y},{orientation}";
        }

        /// <summary>
        /// Odebere všechny větve.
        /// </summary>
        public void ResetBranches()
        {
	        if (!Enabled)
	        {
		        return;
	        }

            Branches.Clear();
            CurrentBranch = new List<PotentialFormulaInput>();
        }

        /// <summary>
        /// Zahájí výpočet v nové větvi.
        /// </summary>
        /// <param name="coords">Souřadnice umístěné kartičky.</param>
        /// <param name="orientation">Orientace kartičky.</param>
        public void InitBranch(Coords coords, TileOrientation orientation)
        {
	        if (!Enabled)
	        {
		        return;
	        }

            var key = ComputeKey(coords, orientation);

            if (!Branches.TryGetValue(key, out CurrentBranch))
            {
                CurrentBranch = new List<PotentialFormulaInput>();
                Branches.Add(key, CurrentBranch);
            }
        }

        /// <summary>
        /// Vybere větev podle skutečného umístění figurky.
        /// </summary>
        /// <param name="coords"></param>
        /// <param name="orientation"></param>
        public void SelectBranch(Coords coords, TileOrientation orientation)
        {
	        if (!Enabled)
	        {
		        return;
	        }

            foreach (var input in Branches[ComputeKey(coords, orientation)])
            {
                Inputs.Add(input);
            }

            CurrentBranch = null;
            Branches = new Dictionary<string, IList<PotentialFormulaInput>>();
        }

        /// <summary>
        /// Vypočítá hodnotu funkce.
        /// </summary>
        /// <param name="input">Vstupní parametry funkce.</param>
        /// <returns>Potenciál regionu.</returns>
        public double Compute(PotentialFormulaInput input)
        {
            CurrentBranch.Add(input);
            return Formula.ComputeParts(input).Potential;
        }

        /// <summary>
        /// Vypočítá gradienty keoficientů podle vstupů a výsledného skóre a modifikuje koeficienty.
        /// </summary>
        /// <param name="finalGrid">Výsledná mapa umístěných kartiček.</param>
        /// <param name="learningRate">Koeficient učení.</param>
        /// <returns>Průměrná chyba.</returns>
        public double LearningSteps(IDictionary<Coords, TilePlacement> finalGrid, double learningRate)
        {
	        if (!Enabled)
	        {
		        return 0;
	        }

            var computedScores = new Dictionary<RegionCoords, double>();

            var totalLoss = 0.0;

            var gradients = new Dictionary<RegionType, IList<PotentialFormulaGradient>>
            {
                { RegionType.Mountain, new List<PotentialFormulaGradient>() },
                { RegionType.Grassland, new List<PotentialFormulaGradient>() },
                { RegionType.Sea, new List<PotentialFormulaGradient>() }
            };

            foreach (var input in Inputs)
            {
                var regionCoords = new RegionCoords { Coords = input.Coords, RegionId = input.RegionId };

                if (!computedScores.TryGetValue(regionCoords, out double tgt))
                {
                    var regionStats = GridSearch.GetRegionStats(finalGrid, input.Coords, input.RegionId, false);
                    tgt = regionStats.Score;
                    foreach (var coords in regionStats.RegionCoords)
                    {
                        computedScores.Add(coords, tgt);
                    }
                }

                var gradient = ComputeGradient(input, tgt, out var loss);
                totalLoss += loss;
                gradients[input.RegionType].Add(gradient);
            }

            var meanGradients = gradients.ToDictionary(kv => kv.Key, kv => PotentialFormulaGradient.Mean(kv.Value));
            meanGradients[RegionType.Grassland].Hm += meanGradients[RegionType.Mountain].HmG + meanGradients[RegionType.Grassland].HmG + meanGradients[RegionType.Sea].HmG;

            var rnd = new Random();

            Func<double, double, double> applyAndRestrict = (l, r) =>
            {
                return Math.Clamp(l - learningRate * r, RndCoeffLo, RndCoeffHi);
            };

            Formula.Coefficients[RegionType.Mountain] = PotentialFormulaCoefficients.Apply<PotentialFormulaCoefficients>(Formula.Coefficients[RegionType.Mountain], meanGradients[RegionType.Mountain], applyAndRestrict);
            Formula.Coefficients[RegionType.Grassland] = PotentialFormulaCoefficients.Apply<PotentialFormulaCoefficients>(Formula.Coefficients[RegionType.Grassland], meanGradients[RegionType.Grassland], applyAndRestrict);
            Formula.Coefficients[RegionType.Sea] = PotentialFormulaCoefficients.Apply<PotentialFormulaCoefficients>(Formula.Coefficients[RegionType.Sea], meanGradients[RegionType.Sea], applyAndRestrict);

            return totalLoss / Inputs.Count;
        }

        /// <summary>
        /// Vypočítá gradient pro daný vstup.
        /// </summary>
        /// <param name="input">Vstup.</param>
        /// <param name="tgt">Cílové skóre.</param>
        /// <param name="loss">Chyba.</param>
        /// <returns>Gradient pro daný vstup.</returns>
        protected PotentialFormulaGradient ComputeGradient(PotentialFormulaInput input, double tgt, out double loss)
        {
            var parts = Formula.ComputeParts(input);

            var coeff = Formula.Coefficients[input.RegionType];
            var cG = Formula.Coefficients[RegionType.Grassland].Hm;

            // Spočítej parciální derivace
            var dPr = parts.Vl * coeff.Cv + parts.Ml * coeff.Cm * coeff.Hm;
            var dPg = parts.Nl * coeff.Cn * cG;
            var dIPu = parts.Vr * coeff.Bv + parts.Mr * coeff.Bm + parts.Nr * coeff.Bn;

            var partialDiff = new PotentialFormulaCoefficients
            {
                Av = parts.Vr,
                Bv = parts.Vr * parts.IPu,
                Cv = parts.Vl * parts.Pr,
                Am = parts.Mr,
                Bm = parts.Mr * parts.IPu,
                Cm = parts.Ml * parts.Pr * coeff.Hm,
                An = parts.Nr,
                Bn = parts.Nr * parts.IPu,
                Cn = parts.Nl * parts.Pg * cG,
                Hm = parts.Ml * parts.Pr * coeff.Cm,
                Ai = dIPu,
                Bi = dIPu * parts.I,
                Apu = dIPu,
                Bpu = dIPu * parts.Pu,
                Apr = dPr,
                Bpr = dPr * parts.Pr,
                Apg = dPg,
                Bpg = dPg * parts.Pg
            };

            loss = (parts.Potential - tgt) * (parts.Potential - tgt);
            var dLoss = 2 * (parts.Potential - tgt); // MSE

            var gradient = PotentialFormulaCoefficients.Apply<PotentialFormulaGradient>(partialDiff, PotentialFormulaCoefficients.Zero, (l, r) => l * dLoss);
            gradient.HmG = parts.Nl * coeff.Cn * parts.Pg * dLoss; // Nížina je řešena zvlášť

            return gradient;
        }

        /// <summary>
        /// Uloží koeficienty potenciálové funkce do souboru.
        /// </summary>
        /// <param name="path">Cesta k souboru.</param>
        public void SaveCoefficients(string path)
        {
            try
            {
                using (var sw = new StreamWriter(path))
                {
                    Formula.SaveCoefficients(sw);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Coefficients could not be saved:");
                Console.WriteLine(ex.Message);
            }
        }
    }
}
