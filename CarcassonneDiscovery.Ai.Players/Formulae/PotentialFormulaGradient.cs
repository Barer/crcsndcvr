﻿using System.Collections.Generic;

namespace CarcassonneDiscovery.Ai.Players
{
    /// <summary>
    /// Koeficienty potenciálové funkce.
    /// </summary>
    public class PotentialFormulaGradient : PotentialFormulaCoefficients
    {
        /// <summary>
        /// Koeficient výskytu měst na nížině..
        /// </summary>
        public double HmG { get; set; }

        /// <iheritdoc />
        public PotentialFormulaGradient() : base()
        {
            // Prázdné
        }

        /// <iheritdoc />
        public PotentialFormulaGradient(double[] coeff) : base(coeff)
        {
            // Prázdné
        }

        /// <summary>
        /// Vytvoří kopii koeficientů.
        /// </summary>
        public new PotentialFormulaGradient Copy()
        {
            var copy = Apply<PotentialFormulaGradient>(this, Zero, (l, r) => l);
            copy.HmG = HmG;

            return copy;
        }

        /// <summary>
        /// Vypočítá aritmentický průměr jednotlivých členů v gradientech.
        /// </summary>
        /// <param name="gradients">Gradienty.</param>
        /// <returns>Atritmetický průměr gradientů.</returns>
        public static PotentialFormulaGradient Mean(IEnumerable<PotentialFormulaGradient> gradients)
        {
            double tempHmG;
            int total = 0;

            PotentialFormulaGradient result = null;

            foreach (var gradient in gradients)
            {
                total++;

                if (result == null)
                {
                    result = gradient;
                }
                else
                {
                    tempHmG = result.HmG + gradient.HmG;
                    result = Apply<PotentialFormulaGradient>(result, gradient, (l, r) => l + r);
                    result.HmG = tempHmG;
                }
            }

            tempHmG = result.HmG / total;
            result = Apply<PotentialFormulaGradient>(result, Zero, (l, r) => l / total);
            result.HmG = tempHmG;

            return result;
        }
    }
}
