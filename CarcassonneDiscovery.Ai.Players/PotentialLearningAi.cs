﻿using CarcassonneDiscovery.Api;
using CarcassonneDiscovery.Client.Base;
using CarcassonneDiscovery.Entity;
using System.IO;

namespace CarcassonneDiscovery.Ai.Players
{
    /// <summary>
    /// Umělá inteligence učící se potenciálovou funkci.
    /// </summary>
    public class PotentialLearningAi : HeuristicsAi<PotentialHeuristics>
    {
        /// <inheritdoc />
        public override string Name => $"PotentialLearningAi-FromDefault-{LearningStep}";

        /// <summary>
        /// Cesta k souboru pro uložení koeficientů.
        /// </summary>
        protected string SavePath;

        /// <summary>
        /// Páska pro potenciálovou funkci.
        /// </summary>
        protected PotentialFormulaTape FormulaTape;

        /// <summary>
        /// Učící koeficient.
        /// </summary>
        protected double LearningStep;

        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="color">Barva hráče.</param>
        /// <param name="gameId">Identifikátor hry.</param>
        /// <param name="service">Služba pro komunikaci se serverem.</param>
        /// <param name="loadPath">Cesta k souboru pro načtení koeficientů.</param>
        /// <param name="savePath">Ceska k souboru pro uložení koeficientů.</param>
        /// <param name="learningStep">Učící koeficient.</param>
        public PotentialLearningAi(PlayerColor color, int gameId, PlayerApiService service, string loadPath = null, string savePath = null, double learningStep = 0.001) : base(color, gameId, service)
        {
            FormulaTape = PotentialFormulaTape.GetOrCreate(gameId, color, loadPath, useRandom: false);
            FormulaTape.Enabled = true;

            LearningStep = learningStep;

            if (savePath != null)
            {
                SavePath = savePath;
                FormulaTape.SaveCoefficients(savePath);
                FormulaTape.SaveCoefficients($"{SavePath}-{gameId}-{SavePath}-pregame.csv");
            }
        }

        /// <inheritdoc />
        protected override void ExecuteAction(GameActionResponse gameAction)
        {
            if (gameAction.Type == GameActionType.PlaceTile && gameAction.Color == Color)
            {
                FormulaTape.SelectBranch(gameAction.Coords.Value, gameAction.TileOrientation.Value);
            }
            else if (gameAction.Type == GameActionType.GameEnd)
            {
                var loss = FormulaTape.LearningSteps(State.Grid, LearningStep);

                if (SavePath != null)
                {
                    FormulaTape.SaveCoefficients(SavePath);

                    var timestepPath = $"{SavePath}-{GameId}-{SavePath}";

                    FormulaTape.SaveCoefficients(timestepPath);
                    
                    using (var sw = new StreamWriter($"{timestepPath}-loss.csv"))
                    {
                        sw.WriteLine(loss);
                    }
                }

                FormulaTape = null;
                PotentialFormulaTape.Remove(GameId, Color);
            }

            base.ExecuteAction(gameAction);
        }
    }
}
