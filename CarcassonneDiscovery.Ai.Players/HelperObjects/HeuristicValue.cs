﻿using CarcassonneDiscovery.Entity;
using System.Collections.Generic;
using System.Linq;

namespace CarcassonneDiscovery.Ai.Players
{
    /// <summary>
    /// Heuristická hodnota stavu.
    /// </summary>
    public class HeuristicValue
    {
        /// <summary>
        /// Heurstické hodnoty jednotlivých hráčů.
        /// </summary>
        protected IDictionary<PlayerColor, double> Values { get; set; }

        /// <summary>
        /// Indexer nad heuristickými hodnotami hráčů.
        /// </summary>
        /// <param name="color">Barva hráče.</param>
        /// <returns>Heuristická hodnota pro hráče.</returns>
        public double this[PlayerColor color]
        {
            get => Values[color];
            set => Values[color] = value;
        }

        /// <summary>
        /// Vytvoří kopii heuristické hodnoty.
        /// </summary>
        /// <returns>Kopie.</returns>
        public HeuristicValue Copy()
        {
	        return FromValues(Values.ToDictionary(kv => kv.Key, kv => kv.Value));
        }

        /// <summary>
        /// Konstruktor pro iniciální hodnotu.
        /// </summary>
        /// <param name="playerOnMove">Hráč na tahu.</param>
        /// <returns>Heuristická hodnota stavu.</returns>
        public static HeuristicValue Initial(PlayerColor playerOnMove)
        {
            return new HeuristicValue
            {
                Values = new Dictionary<PlayerColor, double>
                {
                    { playerOnMove, double.MinValue }
                }
            };
        }

        /// <summary>
        /// Konstruktor z heuristických hodnot.
        /// </summary>
        /// <param name="heuristicValues">Heurstické hodnoty jednotlivých hráčů.</param>
        /// <returns>Heuristická hodnota stavu.</returns>
        public static HeuristicValue FromValues(IDictionary<PlayerColor, double> heuristicValues)
        {
            return new HeuristicValue
            {
                Values = heuristicValues.ToDictionary(kv => kv.Key, kv => kv.Value)
            };
        }

        /// <summary>
        /// Konstruktor ze skóre hráčů.
        /// </summary>
        /// <param name="scores">Skóre jednotlivých hráčů.</param>
        /// <returns>Heuristická hodnota stavu.</returns>
        public static HeuristicValue FromScores(IDictionary<PlayerColor, double> scores)
        {
            return new HeuristicValue
            {
                Values = scores.ToDictionary(kv => kv.Key, kv =>
                {
                    var ownScore = kv.Value;
                    var rank = scores.Values.Where(x => x > ownScore).Count();

                    double scoreDiff;

                    if (rank == 0)
                    {
                        scoreDiff = ownScore - scores.Values.OrderByDescending(x => x).Skip(1).First();
                    }
                    else
                    {
                        scoreDiff = -(scores.Values.OrderByDescending(x => x).TakeWhile(x => x > ownScore).Last() - ownScore);
                    }

                    return (scores.Count - rank) * 100000 + scoreDiff;
                })
            };
        }
    }
}
