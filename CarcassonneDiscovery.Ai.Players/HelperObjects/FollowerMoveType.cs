﻿namespace CarcassonneDiscovery.Ai.Players
{
	/// <summary>
	/// Typ tahu s figurkou.
	/// </summary>
	public enum FollowerMoveType
	{
		Place,
		Remove,
		Pass
	}
}