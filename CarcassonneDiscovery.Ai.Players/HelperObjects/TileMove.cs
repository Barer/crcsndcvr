﻿using System;
using CarcassonneDiscovery.Entity;
using CarcassonneDiscovery.Execution;

namespace CarcassonneDiscovery.Ai.Players
{
    /// <summary>
    /// Tah s kartičkou.
    /// </summary>
    public class TileMove
	{
		/// <summary>
		/// Barva hráče na tahu.
		/// </summary>
		public PlayerColor Color { get; set; }

		/// <summary>
		/// Identifikátor kartičky.
		/// </summary>
		public string TileId { get; set; }

		/// <summary>
		/// Souřadnice kartičky.
		/// </summary>
		public Coords Coords { get; set; }

		/// <summary>
		/// Orientace kartičky.
		/// </summary>
		public TileOrientation Orientation { get; set; }

		/// <summary>
		/// Aplikuje tah na stav.
		/// </summary>
		/// <param name="state">Stav hry.</param>
		/// <returns>Informace pro revertování tahu.</returns>
		public MoveRevertInfo ApplyOnState(GameState state)
		{
			var coords = Coords;
			var previousTile = state.Tile;
			var previousCoords = state.Coords;

			GameExecutor.SetPlaceTile(state, Color, state.Tile, Coords, Orientation);

			return new MoveRevertInfo
			{
				RevertAction = new Action<GameState>((gs) =>
				{
					gs.Grid.Remove(coords);
					gs.Coords = previousCoords;
					gs.Tile = previousTile;
					gs.Phase = GamePhase.TileSelected;
				})
			};
		}
	}
}
