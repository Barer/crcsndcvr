﻿namespace CarcassonneDiscovery.Ai.Players
{
    using CarcassonneDiscovery.Entity;
    using System.Collections.Generic;

    /// <summary>
    /// Skóre pro umístění figurky.
    /// </summary>
    public class FollowerPlacementScore
    {
        /// <summary>
        /// Barva figurky.
        /// </summary>
        public PlayerColor Color { get; set; }

        /// <summary>
        /// Souřadnice kartičky, na které se figurka nachází.
        /// </summary>
        public Coords Coords { get; set; }

        /// <summary>
        /// Identifikátor regionu, na kterém se kartička nachází.
        /// </summary>
        public int RegionId { get; set; }

        /// <summary>
        /// Bodové ohodnocení figurky.
        /// </summary>
        public double Score { get; set; }

        /// <summary>
        /// Potenciálové ohodnocení figurky.
        /// </summary>
        public double Potential { get; set; }

        /// <summary>
        /// Otevřené pozice regionu.
        /// </summary>
        public IList<Coords> OpenCoords { get; set; }
    }
}
