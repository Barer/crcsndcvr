﻿using System;
using CarcassonneDiscovery.Entity;

namespace CarcassonneDiscovery.Ai.Players
{
    /// <summary>
    /// Informace pro revertování tahu.
    /// </summary>
    public class MoveRevertInfo
	{
		/// <summary>
		/// Akce k provedení.
		/// </summary>
		public Action<GameState> RevertAction { get; set; }

		/// <summary>
		/// Revertuje tah.
		/// </summary>
		/// <param name="state">Stav po provedení tahu.</param>
		public void RevertOnState(GameState state)
		{
			RevertAction.Invoke(state);
		}
	}
}
