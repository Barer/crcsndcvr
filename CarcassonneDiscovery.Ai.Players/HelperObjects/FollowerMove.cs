﻿using System;
using System.Linq;
using CarcassonneDiscovery.Entity;
using CarcassonneDiscovery.Execution;
using CarcassonneDiscovery.Tools;

namespace CarcassonneDiscovery.Ai.Players
{
    /// <summary>
    /// Tah s figurkou.
    /// </summary>
    public class FollowerMove
	{
		/// <summary>
		/// Typ tahu.
		/// </summary>
		public FollowerMoveType Type { get; set; }

		/// <summary>
		/// Barva hráče na tahu.
		/// </summary>
		public PlayerColor Color { get; set; }

		/// <summary>
		/// Souřadnice kartičky.
		/// </summary>
		public Coords Coords { get; set; }

		/// <summary>
		/// Identifikátor regionu s figurkou.
		/// </summary>
		public int RegionId { get; set; }

		/// <summary>
		/// Aplikuje tah na stav.
		/// </summary>
		/// <param name="state">Stav hry.</param>
		/// <returns>Informace pro revertování tahu.</returns>
		public MoveRevertInfo ApplyOnState(GameState state)
		{
			var revert = new MoveRevertInfo();

			var coords = Coords;
			var color = Color;
			var regionId = RegionId;

			switch (Type)
			{
				case FollowerMoveType.Place:
					revert.RevertAction = new Action<GameState>((gs) =>
					{
						gs.Grid[coords].FollowerPlacement = null;
						gs.PlacedFollowers[color] = gs.PlacedFollowers[color].Where(x => x.Coords != coords).ToList();
						gs.Phase = GamePhase.TilePlaced;
					});
					GameExecutor.SetPlaceFollower(state, color, coords, regionId);
					break;

				case FollowerMoveType.Remove:
					var score = GridSearch.GetScoreForFollower(state.Grid, coords, regionId);
					revert.RevertAction = new Action<GameState>((gs) =>
					{
						var followerPlacement = new FollowerPlacement
						{
							Color = color,
							Coords = coords,
							RegionId = regionId
						};
						gs.Grid[coords].FollowerPlacement = followerPlacement;
						gs.PlacedFollowers[color].Add(followerPlacement);
						gs.Score[color] -= score;
						gs.Phase = GamePhase.TilePlaced;
					});
					GameExecutor.SetRemoveFollower(state, color, coords, regionId, score);
					break;

				case FollowerMoveType.Pass:
					revert.RevertAction = new Action<GameState>((gs) => { gs.Phase = GamePhase.TilePlaced; });
					GameExecutor.SetPassMove(state, color);
					break;
			}

			return revert;
		}
	}
}