﻿namespace CarcassonneDiscovery.Ai.Players
{
    using CarcassonneDiscovery.Client.Base;
    using CarcassonneDiscovery.Entity;
    using CarcassonneDiscovery.Tools;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Umělá inteligence využívající algoritmus expectiminimax s heuristikou.
    /// </summary>
    /// <typeparam name="THeuristics">Typ heuristiky.</typeparam>
    public class ExpectiminimaxAi<THeuristics> : HeuristicsAi<THeuristics>
        where THeuristics : IHeuristics, new()
    {
        /// <summary>
        /// Maximální hloubka procházení.
        /// </summary>
        protected int MaxLevel;

        /// <inheritdoc />
        public override string Name => $"ExpectiminimaxAi-{MaxLevel}-{Heuristics.Name}";

        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="color">Barva hráče.</param>
        /// <param name="gameId">Identifikátor hry.</param>
        /// <param name="service">Služba pro komunikaci se serverem.</param>
        /// <param name="maxLevel">Maximální hloubka procházení.</param>
        public ExpectiminimaxAi(PlayerColor color, int gameId, PlayerApiService service, int maxLevel) : base(color, gameId, service)
        {
            MaxLevel = maxLevel;
        }

		/// <inheritdoc />
        protected override void SelectMove()
		{
			var placedTiles = State.Grid.Values.Select(x => x.Tile.Id).ToHashSet();

			var remainingTiles = StandardTileSet.Where(x => !placedTiles.Contains(x.Id) && State.Tile.Id != x.Id).ToList();

			var bestMove = SearchBestMove(State, remainingTiles, MaxLevel).Item1;
			SelectedTileMove = bestMove.Item1;
			SelectedFollowerMove = bestMove.Item2;
		}

		/// <summary>
		/// Provede krok prohledávání.
		/// </summary>
		/// <param name="state">Stav hry.</param>
		/// <param name="remainingTiles">Enumerátor nad zbývajícími kartičkami.</param>
		/// <param name="remainingLevel">Zbývající hloubka.</param>
		/// <returns>Dvojice udávající nejlepší tah a jeho hodnotu.</returns>
		protected virtual Tuple<Tuple<TileMove, FollowerMove>, HeuristicValue> SearchBestMove(GameState state, IEnumerable<ITileScheme> remainingTiles, int remainingLevel)
        {
	        if (remainingLevel <= 1 || !remainingTiles.Any())
	        {
		        return GetBestMove(state);
	        }

	        var onMove = state.Params.PlayerOrder[state.PlayerOnMove];

	        HeuristicValue bestScore = HeuristicValue.Initial(onMove);
	        Tuple<TileMove, FollowerMove> bestMove = null;

			var heuristics = new THeuristics();
            heuristics.Init(GameId, Color);

			foreach (var move in EnumerateAllMoves(state, heuristics))
			{
				var score = SearchNextMove(state, remainingTiles, remainingLevel);

				if (score[onMove] > bestScore[onMove])
				{
					bestScore = score;
					bestMove = move;
				}
			}

			return new Tuple<Tuple<TileMove, FollowerMove>, HeuristicValue>(bestMove, bestScore);
		}

		/// <summary>
        /// Projde všechny možnosti pro nálsedující kartičku a vrátí očekávané skóre.
        /// </summary>
        /// <param name="state">Stav hry.</param>
        /// <param name="remainingTiles">Enumerátor nad zbývajícími kartičkami.</param>
        /// <param name="remainingLevel">Zbývající hloubka.</param>
        /// <returns>Trojice udávající hodnodu nejlepšího tahu a nejlepší tah s figurkou.</returns>
        protected virtual HeuristicValue SearchNextMove(GameState state, IEnumerable<ITileScheme> remainingTiles, int remainingLevel)
		{
			var oldTile = state.Tile;
            var oldOnMove = state.PlayerOnMove;

            var newOnMove = (state.PlayerOnMove + 1) % state.Params.PlayerOrder.Length;
            var newRemainingLevel = remainingLevel - 1;

            var cummulativeValues = state.Score.ToDictionary(kv => kv.Key, kv => 0.0);
            var cummulativePlacements = 0;

            foreach (var tile in remainingTiles)
            {
                if (GridSearch.IsTilePlaceable(state.Grid, tile))
                {
                    // IEnumerable v LINQu nekopíruje
                    var newRemainingTiles = remainingTiles.Where(x => x.Id != tile.Id);

                    state.Phase = GamePhase.TileSelected;
                    state.PlayerOnMove = newOnMove;
                    state.Tile = tile;

                    var newValues = SearchBestMove(state, newRemainingTiles, newRemainingLevel).Item2;

                    foreach (var color in state.Params.PlayerOrder)
                    {
                        cummulativeValues[color] += newValues[color];
                    }

                    cummulativePlacements++;
                }
            }

            state.Phase = GamePhase.MoveEnded;
            state.Tile = oldTile;
            state.PlayerOnMove = oldOnMove;

            foreach (var color in state.Params.PlayerOrder)
            {
                cummulativeValues[color] /= cummulativePlacements;
            }

            return HeuristicValue.FromValues(cummulativeValues);
        }
    }
}
