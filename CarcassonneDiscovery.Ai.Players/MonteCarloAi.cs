﻿using System;
using System.Collections.Generic;
using System.Linq;
using CarcassonneDiscovery.Client.Base;
using CarcassonneDiscovery.Entity;
using CarcassonneDiscovery.Tools;

namespace CarcassonneDiscovery.Ai.Players
{
    /// <summary>
    /// Umělá inteligence využívající Monte Carlo simulace.
    /// </summary>
    /// <typeparam name="THeuristics">Typ heuristiky.</typeparam>
    public class MonteCarloAi<THeuristics> : HeuristicsAi<THeuristics>
		where THeuristics : IHeuristics, new()
	{
		/// <inheritdoc />
		public override string Name => $"MonteCarloAi-{SimulationCount}-{Gamma}-{GammaLimit}-{Heuristics.Name}";

		/// <summary>
		/// Počet simulací dohromady
		/// </summary>
		protected int SimulationCount;

		/// <summary>
		/// Koeficient znavýhodnění.
		/// </summary>
		protected double Gamma;

		/// <summary>
		/// Minimální zásah znevýhodněného přínosu do celkového skóre, při kterém se stále provádí simulace.
		/// </summary>
		protected double GammaLimit;

		/// <summary>
		/// Konstruktor.
		/// </summary>
		/// <param name="color">Barva hráče.</param>
		/// <param name="gameId">Identifikátor hry.</param>
		/// <param name="service">Služba pro komunikaci se serverem.</param>
		/// <param name="simulationCount">Počet simulací pro každou variantu.</param>
		/// <param name="gamma">Koeficient znevýhodnění.</param>
		/// <param name="gammaLimit">Minimální zásah znevýhodněného přínosu do celkového skóre, při kterém se stále provádí simulace.</param>
		public MonteCarloAi(PlayerColor color, int gameId, PlayerApiService service, int simulationCount, double gamma, double gammaLimit)
			: base(color, gameId, service)
		{
			SimulationCount = simulationCount;
			Gamma = gamma;
			GammaLimit = gammaLimit;
		}

		/// <summary>
		/// Vypočítá nejlepší tah (pro hráče na tahu) v daném stavu.
		/// </summary>
		/// <param name="state">Stav hry.</param>
		/// <returns>Dvojice udávající nejlepší tah a jeho hodnotu.</returns>
		protected override Tuple<Tuple<TileMove, FollowerMove>, HeuristicValue> GetBestMove(GameState state)
		{
			var onMove = state.Params.PlayerOrder[state.PlayerOnMove];

			HeuristicValue bestScore = HeuristicValue.Initial(onMove);
			Tuple<TileMove, FollowerMove> bestMove = null;

			var placedTiles = State.Grid.Values.Select(x => x.Tile.Id).ToHashSet();
			var remainingTiles = StandardTileSet.Where(x => !placedTiles.Contains(x.Id)).ToList();

			var heuristics = new THeuristics();
			heuristics.Init(GameId, Color);

			var moveCount = 0;

			foreach (var move in EnumerateAllMoves(state, heuristics))
			{
				moveCount++;
			}

			if (moveCount > 0)
			{ 
				var steps = Math.Max(SimulationCount / moveCount, 1);

				foreach (var move in EnumerateAllMoves(state, heuristics))
				{
					var score = heuristics.GetValue(state, move.Item1, move.Item2);
					score = GetMcScore(state, score, remainingTiles, steps);

					if (score[onMove] > bestScore[onMove])
					{
						bestScore = score;
						bestMove = move;
					}
				}
			}


			return new Tuple<Tuple<TileMove, FollowerMove>, HeuristicValue>(bestMove, bestScore);
		}

		/// <summary>
		/// Zjistí skóre stavu podle MC simulací.
		/// </summary>
		/// <param name="state">Ohodnocovaný stav.</param>
		/// <param name="initialValue">Počáteční hodnota.</param>
		/// <param name="remainingTiles">Zbývající kartičky.</param>
		/// <param name="steps">Počet kroků vyhodnocení.</param>
		protected virtual HeuristicValue GetMcScore(GameState state, HeuristicValue initialValue, IList<ITileScheme> remainingTiles, int steps)
		{
			var oldTile = state.Tile;
			var oldOnMove = state.PlayerOnMove;

			var cummulativeValues = state.Score.ToDictionary(kv => kv.Key, kv => 0.0);

			var rnd = new Random();

			for (var step = 0; step < steps; step++)
			{
				var remainingTilesInSimulation = new List<ITileScheme>(remainingTiles);

				var totalValue = initialValue.Copy();

				var moveReverts = new Stack<Tuple<MoveRevertInfo, MoveRevertInfo>>();

				var gamma_n = Gamma;

				while (remainingTilesInSimulation.Count > 0)
				{
					ITileScheme tile = null;
					for (var tries = 0; tries < 10; tries++)
					{
						var index = rnd.Next(remainingTilesInSimulation.Count);
						tile = remainingTilesInSimulation[index];
						if (GridSearch.IsTilePlaceable(state.Grid, tile))
						{
							break;
						}
						tile = null;
					}
					if (tile == null)
					{
						break;
					}

					remainingTilesInSimulation.Remove(tile);

					state.Phase = GamePhase.TileSelected;
					state.PlayerOnMove = (state.PlayerOnMove + 1) % state.Params.PlayerOrder.Length;
					state.Tile = tile;

					var bestMovePair = base.GetBestMove(state);

					foreach (var color in state.Params.PlayerOrder)
					{
						totalValue[color] += bestMovePair.Item2[color] * gamma_n;
					}

					var tileMoveRevert = bestMovePair.Item1.Item1.ApplyOnState(state);
					var followerMoveRevert = bestMovePair.Item1.Item2.ApplyOnState(state);

					moveReverts.Push(new Tuple<MoveRevertInfo, MoveRevertInfo>(tileMoveRevert, followerMoveRevert));

					// Suma geometrické řady: s_n=a_1*(1-q^n)/(1-q), člen: a_n=a_1*q^(n-1)
					// Poměr: a_n/s_n = (q^(n-1)-q^n)/(1-q^n)
					if (gamma_n < 1)
					{
						var ratio = gamma_n;
						gamma_n *= Gamma;
						ratio = (ratio - gamma_n) / (1 - gamma_n);
						if (ratio < GammaLimit)
						{
							break;
						}
					}
					else
					{
						gamma_n *= Gamma;
					}
				}

				while (moveReverts.Count > 0)
				{
					var reverts = moveReverts.Pop();

					reverts.Item2.RevertOnState(state);
					reverts.Item1.RevertOnState(state);
				}

				foreach (var color in state.Params.PlayerOrder)
				{
					cummulativeValues[color] += totalValue[color];
				}
			}

			foreach (var color in state.Params.PlayerOrder)
			{
				cummulativeValues[color] /= steps;
			}

			state.Phase = GamePhase.MoveEnded;
			state.PlayerOnMove = oldOnMove;
			state.Tile = oldTile;

			return HeuristicValue.FromValues(cummulativeValues);
		}
	}
}