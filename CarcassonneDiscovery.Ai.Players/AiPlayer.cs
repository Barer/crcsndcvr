﻿namespace CarcassonneDiscovery.Ai.Players
{
    using CarcassonneDiscovery.Api;
    using CarcassonneDiscovery.Client.Base;
    using CarcassonneDiscovery.Entity;
    using CarcassonneDiscovery.Execution;
    using CarcassonneDiscovery.StandardTileSet;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// Hrající umělá inteligence.
    /// </summary>
    public abstract class AiPlayer
    {
        /// <summary>
        /// Jméno hráče.
        /// </summary>
        public abstract string Name { get; }

        /// <summary>
        /// Barva hráče.
        /// </summary>
        public PlayerColor Color { get; protected set; }

        /// <summary>
        /// Identifikátor hry.
        /// </summary>
        protected int GameId;

        /// <summary>
        /// Služba pro komunikaci s API serveru.
        /// </summary>
        protected PlayerApiService Service;

        /// <summary>
        /// Stav hry.
        /// </summary>
        protected GameState State;

        /// <summary>
        /// Seznam kartiček ve standardní sadě.
        /// </summary>
        protected IList<ITileScheme> StandardTileSet;

        /// <summary>
        /// Zámek proti úpravám stavu hry.
        /// </summary>
        protected object Lock;

        /// <summary>
        /// Vlákno se smyčkou pro získání herních akcí.
        /// </summary>
        protected Thread GameActionLoopThread;

        /// <summary>
        /// Je hráč v průběhu?
        /// </summary>
        public bool IsRunning { get; protected set; }

        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="color">Barva hráče.</param>
        /// <param name="gameId">Identifikátor hry.</param>
        /// <param name="service">Služba pro komunikaci s API serveru.</param>
        public AiPlayer(PlayerColor color, int gameId, PlayerApiService service)
        {
            Color = color;
            GameId = gameId;
            Service = service;

            Lock = new object();

            StandardTileSet = StandardTileGenerator.GenerateStandardTileSet();
        }

        /// <summary>
        /// Provede první část tahu.
        /// </summary>
        protected abstract void InvokeTileMovePart();

        /// <summary>
        /// Provede druhou část tahu.
        /// </summary>
        protected abstract void InvokeFollowerMovePart();

        /// <summary>
        /// Umísti kartičku.
        /// </summary>
        /// <param name="coords">Souřadnice kartičky.</param>
        /// <param name="tileId">Identifikátor kartičky.</param>
        /// <param name="orientation">Orientace kartičky.</param>
        protected void PlaceTile(Coords coords, string tileId, TileOrientation orientation)
        {
            var request = new PlaceTileRequest
            {
                GameId = GameId,
                Color = Color,
                Coords = coords,
                TileId = tileId,
                Orientation = orientation
            };

            RunTaskUntilFinished(async () =>
            {
                var response = await Service.PlaceTile(request);

                if (response == null || response.IsError)
                {
                    RageQuit($"Error while PlaceTile: {response.ErrorMessage}");
                }
            });
        }

        /// <summary>
        /// Umísti figurku.
        /// </summary>
        /// <param name="coords">Souřadnice regionu.</param>
        /// <param name="regionId">Identifikátor regionu.</param>
        protected void PlaceFollower(Coords coords, int regionId)
        {
            var request = new PlaceFollowerRequest
            {
                GameId = GameId,
                Color = Color,
                Coords = coords,
                RegionId = regionId
            };

            RunTaskUntilFinished(async () =>
            {
                var response = await Service.PlaceFollower(request);

                if (response == null || response.IsError)
                {
                    RageQuit($"Error while PlaceFollower: {response.ErrorMessage}");
                }
            });
        }

        /// <summary>
        /// Odeber figurku.
        /// </summary>
        /// <param name="coords">Souřadnice regionu.</param>
        /// <param name="regionId">Identifikátor regionu.</param>
        protected void RemoveFollower(Coords coords, int regionId)
        {
            var request = new RemoveFollowerRequest
            {
                GameId = GameId,
                Color = Color,
                Coords = coords,
                RegionId = regionId
            };

            RunTaskUntilFinished(async () =>
            {
                var response = await Service.RemoveFollower(request);

                if (response == null || response.IsError)
                {
                    RageQuit($"Error while RemoveFollower: {response.ErrorMessage}");
                }
            });
        }

        /// <summary>
        /// Předej tah.
        /// </summary>
        protected void PassMove()
        {
            var request = new PassMoveRequest
            {
                GameId = GameId,
                Color = Color
            };

            RunTaskUntilFinished(async () =>
            {
                var response = await Service.PassMove(request);

                if (response == null || response.IsError)
                {
                    RageQuit($"Error while PassMove: {response.ErrorMessage}");
                }
            });
        }

        /// <summary>
        /// Předčasně ukonči hru.
        /// </summary>
        protected void StopGame()
        {
            var request = new StopGameRequest
            {
                GameId = GameId
            };

            RunTaskUntilFinished(async () =>
            {
                var response = await Service.StopGame(request);
            });
        }

        /// <summary>
        /// Registuj jméno.
        /// </summary>
        protected void RegisterName()
        {
            var request = new RegisterNameRequest
            {
                GameId = GameId,
                Color = Color,
                Name = Name
            };

            RunTaskUntilFinished(async () =>
            {
                var response = await Service.RegisterName(request);

                if (response == null || response.IsError)
                {
                    RageQuit($"Error while RegisterName: {response.ErrorMessage}");
                }
            });
        }

        /// <summary>
        /// Získej parametry hry.
        /// </summary>
        protected void GameDetail()
        {
            var request = new GameDetailRequest
            {
                GameId = GameId
            };

            RunTaskUntilFinished(async () =>
            {
                var response = await Service.GameDetail(request);

                if (response == null || response.IsError)
                {
                    RageQuit($"Error while GameDetail: {response.ErrorMessage}");
                }

                if (State == null)
                {
                    lock (Lock)
                    {
                        State = new GameState
                        {
                            Grid = new Dictionary<Coords, TilePlacement>(),
                            Params = new GameParams
                            {
                                FollowerCount = response.FollowerCount,
                                PlayerOrder = response.Players.OrderBy(x => x.Order).Select(x => x.Color).ToArray(),
                                TileSetId = response.TileSetId
                            },
                            Score = response.Players.ToDictionary(x => x.Color, x => 0),
                            PlacedFollowers = response.Players.ToDictionary(x => x.Color, x => (IList<FollowerPlacement>)new List<FollowerPlacement>())
                        };
                    }
                }
            });
        }

        /// <summary>
        /// Práce smyčky pro získávání herních akcí.
        /// </summary>
        public virtual void GameActionLoop()
        {
            var looper = new GameActionLooper(Service);

            foreach (var gameAction in looper.Run(GameId, 0))
            {
                if (!IsRunning)
                {
                    break;
                }

                while (State == null && gameAction.Type != GameActionType.GameEnd && gameAction.Type != GameActionType.GameQuit)
                {
                    Thread.Sleep(3000);
                }

                lock (Lock)
                {
                    ExecuteAction(gameAction);
                }
            }
        }

        /// <summary>
        /// Provede herní akci.
        /// </summary>
        /// <param name="gameAction">Herní akce</param>
        protected virtual void ExecuteAction(GameActionResponse gameAction)
        {
            switch (gameAction.Type)
            {
                case GameActionType.StartGame:
                    GameExecutor.SetStartGame(State, StandardTileSet[int.Parse(gameAction.TileId)], gameAction.Coords.Value, gameAction.TileOrientation.Value);
                    break;

                case GameActionType.StartMove:
                    GameExecutor.SetStartMove(State, StandardTileSet[int.Parse(gameAction.TileId)], gameAction.Color.Value);
                    if (gameAction.Color.Value == Color)
                    {
                        InvokeTileMovePart();
                    }
                    break;

                case GameActionType.PlaceTile:
                    GameExecutor.SetPlaceTile(State, gameAction.Color.Value, StandardTileSet[int.Parse(gameAction.TileId)], gameAction.Coords.Value, gameAction.TileOrientation.Value);
                    if (gameAction.Color.Value == Color)
                    {
                        InvokeFollowerMovePart();
                    }
                    break;

                case GameActionType.PlaceFollower:
                    GameExecutor.SetPlaceFollower(State, gameAction.Color.Value, gameAction.Coords.Value, gameAction.RegionId.Value);
                    break;

                case GameActionType.RemoveFollower:
                    GameExecutor.SetRemoveFollower(State, gameAction.Color.Value, gameAction.Coords.Value, gameAction.RegionId.Value, gameAction.Score.Value);
                    break;

                case GameActionType.PassMove:
                    GameExecutor.SetPassMove(State, gameAction.Color.Value);
                    break;

                case GameActionType.GameEnd:
                    GameExecutor.SetEndGame(State, new List<RemoveFollowerExecutionResult>());
                    Stop();
                    break;
            }
        }

        /// <summary>
        /// Ukončí hru pro nesprávném požadavku na herní akci.
        /// </summary>
        /// <param name="msg">Chybová hláška.</param>
        protected virtual void RageQuit(string msg)
        {
            Console.Error.WriteLine("RageQuit: " + msg);
            StopGame();
            Stop();
        }

        /// <summary>
        /// Opakovaně provádí akci, dokud úspěšně neskončí.
        /// </summary>
        /// <param name="action">Akce k provedení.</param>
        protected virtual void RunTaskUntilFinished(Action action)
        {
            while (true)
            {
                try
                {
                    action.Invoke();
                    break;
                }
                catch (TaskCanceledException)
                {
                    continue;
                }
            }
        }

        /// <summary>
        /// Zahájí činnost hráče.
        /// </summary>
        /// <param name="verbose">Vypsat debugovací hlášku?</param>
        public virtual void Start(bool verbose = false)
        {
            if (!IsRunning)
            {
                lock (Lock)
                {
                    RegisterName();
                    GameDetail();

                    IsRunning = true;

                    if (verbose)
                    {
                        Console.WriteLine("Ai started.");
                    }

                    GameActionLoopThread = new Thread(GameActionLoop);
                    GameActionLoopThread.Start();
                }
            }
            else
            {
                if (verbose)
                {
                    Console.WriteLine("Could not start ai player which is already running.");
                }
            }
        }

        /// <summary>
        /// Ukončí činnost hráče.
        /// </summary>
        public virtual void Stop()
        {
            if (IsRunning)
            {
                lock (Lock)
                {
                    State = null;

                    IsRunning = false;
                }
            }
        }
    }
}
