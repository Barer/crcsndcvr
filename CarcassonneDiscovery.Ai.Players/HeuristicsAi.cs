﻿namespace CarcassonneDiscovery.Ai.Players
{
    using CarcassonneDiscovery.Client.Base;
    using CarcassonneDiscovery.Entity;
    using CarcassonneDiscovery.Tools;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Umělá inteligence ohodnocující všechny tahy pomocí heuristiky.
    /// </summary>
    /// <typeparam name="THeuristics">Typ heuristiky.</typeparam>
    public class HeuristicsAi<THeuristics> : AiPlayer
		where THeuristics : IHeuristics, new()
	{
		/// <inheritdoc />
		public override string Name => "HeuristicsAi-" + Heuristics.Name;

		/// <summary>
		/// Heuristika použita pro výpočty.
		/// </summary>
		protected THeuristics Heuristics;

		/// <summary>
		/// Vybraný tah s kartičkou.
		/// </summary>
		protected TileMove SelectedTileMove;

		/// <summary>
		/// Vybraný tah s figurkou.
		/// </summary>
		protected FollowerMove SelectedFollowerMove;

		/// <summary>
		/// Konstruktor.
		/// </summary>
		/// <param name="color">Barva hráče.</param>
		/// <param name="gameId">Identifikátor hry.</param>
		/// <param name="service">Služba pro komunikaci se serverem.</param>
		public HeuristicsAi(PlayerColor color, int gameId, PlayerApiService service) : base(color, gameId, service)
		{
			Heuristics = new THeuristics();
		}

		/// <inheritdoc />
		protected override void InvokeTileMovePart()
		{
			SelectMove();

			if (SelectedTileMove != null)
			{
				PlaceTile(SelectedTileMove.Coords, SelectedTileMove.TileId, SelectedTileMove.Orientation);
				SelectedTileMove = null;
			}
			else
			{
				RageQuit("MoveTilePartAction was not selected.");
			}
		}

		/// <inheritdoc />
		protected override void InvokeFollowerMovePart()
		{
			if (SelectedFollowerMove != null)
			{
				switch (SelectedFollowerMove.Type)
				{
					case FollowerMoveType.Place:
						PlaceFollower(SelectedFollowerMove.Coords, SelectedFollowerMove.RegionId);
						break;
					case FollowerMoveType.Remove:
						RemoveFollower(SelectedFollowerMove.Coords, SelectedFollowerMove.RegionId);
						break;
					case FollowerMoveType.Pass:
						PassMove();
						break;
					default:
						RageQuit("MoveFollowerPartAction was not selected.");
						break;
				}
				SelectedFollowerMove = null;
			}
			else
			{
				RageQuit("MoveFollowerPartAction was not selected.");
			}
		}

		/// <summary>
		/// Vypočítá nejlepší tah (pro hráče na tahu) k provedení.
		/// </summary>
		protected virtual void SelectMove()
		{
			var bestMove = GetBestMove(State).Item1;
			SelectedTileMove = bestMove.Item1;
			SelectedFollowerMove = bestMove.Item2;
		}

		/// <summary>
		/// Vypočítá nejlepší tah (pro hráče na tahu) v daném stavu.
		/// </summary>
		/// <param name="state">Stav hry.</param>
		/// <returns>Dvojice udávající nejlepší tah a jeho hodnotu.</returns>
		protected virtual Tuple<Tuple<TileMove, FollowerMove>, HeuristicValue> GetBestMove(GameState state)
		{
			var onMove = state.Params.PlayerOrder[state.PlayerOnMove];

			HeuristicValue bestScore = HeuristicValue.Initial(onMove);
			Tuple<TileMove, FollowerMove> bestMove = null;

			var heuristics = new THeuristics();
			heuristics.Init(GameId, Color);

			foreach (var move in EnumerateAllMoves(state, heuristics))
			{
				var score = heuristics.GetValue(state, move.Item1, move.Item2);

				if (score[onMove] > bestScore[onMove])
				{
					bestScore = score;
					bestMove = move;
				}
			}

			return new Tuple<Tuple<TileMove, FollowerMove>, HeuristicValue>(bestMove, bestScore);
		}

		/// <summary>
		/// Projde všechny možné tahy a průběžně je aplikuje na stav.
		/// </summary>
		/// <param name="state">Počáteční stav.</param>
		/// <param name="heuristics">Použitá heuristika.</param>
		/// <returns>Enumerátor nad stavy.</returns>
		protected IEnumerable<Tuple<TileMove, FollowerMove>> EnumerateAllMoves(GameState state, THeuristics heuristics)
		{
			heuristics.InitState(state);

			foreach (var tileMove in GetAllTileMoves(state))
			{
				var tileRevert = tileMove.ApplyOnState(state);

				heuristics.InitTileMove(state, tileMove);

				foreach (var followerMove in GetAllFollowerMoves(state))
				{
					var followerRevert = followerMove.ApplyOnState(state);

					yield return new Tuple<TileMove, FollowerMove>(tileMove, followerMove);

					followerRevert.RevertOnState(state);
				}

				tileRevert.RevertOnState(state);
			}
		}


		/// <summary>
		/// Vrátí všechna platná umístení kartičky v daném stavu.
		/// </summary>
		/// <param name="state">Stav hry.</param>
		/// <returns>Enumerátor nad umístěním kartičky.</returns>
		protected IList<TileMove> GetAllTileMoves(GameState state)
		{
			var onMove = state.Params.PlayerOrder[state.PlayerOnMove];
			var tileId = state.Tile.Id;

			return GridSearch.GetAllTilePlacements(state.Grid, state.Tile)
				.Select(tilePlacement => new TileMove
				{
					Color = onMove,
					TileId = tileId,
					Coords = tilePlacement.Coords,
					Orientation = tilePlacement.Orientation
				}).ToList();
		}

		/// <summary>
		/// Projde všechna platná umístění figurky v daném stavu.
		/// Při vrácení následujícího prvku je stav změněn podle umístění figurky.
		/// </summary>
		/// <param name="state">Stav hry.</param>
		/// <returns>Enumerátor nad umístěním figurky.</returns>
		protected IList<FollowerMove> GetAllFollowerMoves(GameState state)
		{
			var onMove = state.Params.PlayerOrder[state.PlayerOnMove];

			var allMoves = new List<FollowerMove>();

			// Položení figurky
			if (state.Params.FollowerCount > state.PlacedFollowers[onMove].Count)
			{
				for (var rId = 0; rId < state.Tile.RegionCount; rId++)
				{
					if (!GridSearch.IsRegionOccupied(state.Grid, state.Coords, rId))
					{
						allMoves.Add(new FollowerMove
						{
							Color = Color,
							Coords = state.Coords,
							RegionId = rId,
							Type = FollowerMoveType.Place
						});
					}
				}
			}

			// Odebrání figurky
			allMoves.AddRange(state.PlacedFollowers[onMove].Select(fp =>
				new FollowerMove
				{
					Color = onMove,
					Coords = fp.Coords,
					RegionId = fp.RegionId,
					Type = FollowerMoveType.Remove
				}
			));

			// Předání tahu
			allMoves.Add(new FollowerMove
			{
				Color = onMove,
				Type = FollowerMoveType.Pass
			});

			return allMoves;
		}
	}
}
