﻿namespace CarcassonneDiscovery.Entity
{
    /// <summary>
    /// Otočení kartičky; hrana kartičky; směr sousední kartičky.
    /// </summary>
    /// <remarks>
    /// Využívá bitovou logiku; více směrů lze zapsat naráz.
    /// </remarks>
    public enum TileOrientation : int
    {
        // Světové strany
        N = 0b0001,
        E = 0b0010,
        S = 0b0100,
        W = 0b1000,

        // Žádný směr
        None = 0b0000,

        // 2 směry
        NE = N | E,
        NS = N | S,
        NW = N | W,
        SE = S | E,
        SW = S | W,
        EW = E | W,

        // 3 směry
        NSE = N | S | E,
        NSW = N | S | W,
        NEW = N | E | W,
        SEW = S | E | W,

        // 4 směry
        All = N | E | S | W
    }
}
