﻿namespace CarcassonneDiscovery.Entity
{
    /// <summary>
    /// Fáze hry.
    /// </summary>
    public enum GamePhase
    {
        /// <summary>
        /// Před zahájením hry.
        /// </summary>
        BeforeStart,

        /// <summary>
        /// Hra byla zahájena, první kartička je položena.
        /// </summary>
        GameStarted,

        /// <summary>
        /// Začátek tahu. Kartička byla vybrána, čeká se na její umístění
        /// </summary>
        TileSelected,

        /// <summary>
        /// Kartička byla uístěna, čeká se na tah s figurkou.
        /// </summary>
        TilePlaced,

        /// <summary>
        /// Tah s figurkou byl dokončen, čeká se na následující tah.
        /// </summary>
        MoveEnded,

        /// <summary>
        /// Konec hry.
        /// </summary>
        GameEnded
    }
}
