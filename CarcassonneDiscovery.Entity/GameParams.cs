﻿namespace CarcassonneDiscovery.Entity
{
    /// <summary>
    /// Parametry hry.
    /// </summary>
    public class GameParams
    {
        /// <summary>
        /// Počet figurek pro každého hráče.
        /// </summary>
        public int FollowerCount { get; set; }

        /// <summary>
        /// Pořadí hráčů na tahu.
        /// </summary>
        public PlayerColor[] PlayerOrder { get; set; }

        /// <summary>
        /// Identifikátor sady kartiček.
        /// </summary>
        public string TileSetId { get; set; }
    }
}
