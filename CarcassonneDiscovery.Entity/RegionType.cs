﻿namespace CarcassonneDiscovery.Entity
{
    /// <summary>
    /// Druh regionu.
    /// </summary>
    public enum RegionType
    {
        /// <summary>
        /// Hory.
        /// </summary>
        Mountain,

        /// <summary>
        /// Nížiny.
        /// </summary>
        Grassland,

        /// <summary>
        /// Moře.
        /// </summary>
        Sea
    }
}
