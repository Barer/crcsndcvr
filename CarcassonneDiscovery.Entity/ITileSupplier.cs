﻿namespace CarcassonneDiscovery.Entity
{
    /// <summary>
    /// Poskytovatel kartiček ve hře.
    /// </summary>
    public interface ITileSupplier
    {
        /// <summary>
        /// Počet zbývajících kartiček.
        /// </summary>
        int RemainingCount { get; }

        /// <summary>
        /// Vylosuje první katičku ve hře.
        /// </summary>
        /// <returns>Kartička.</returns>
        ITileScheme GetFirstTile();

        /// <summary>
        /// Vylosuje násedující kartičku.
        /// </summary>
        /// <returns>Kartička.</returns>
        ITileScheme GetNextTile();

        /// <summary>
        /// Vrátí nepoužitou kartičku do baíčku.
        /// </summary>
        /// <param name="tile">Nepoužitá kartička.</param>
        void ReturnTile(ITileScheme tile);
    }
}
