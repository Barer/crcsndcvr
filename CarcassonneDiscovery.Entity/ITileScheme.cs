﻿namespace CarcassonneDiscovery.Entity
{
    using System.Collections.Generic;

    /// <summary>
    /// Schéma kartičky.
    /// </summary>
    public interface ITileScheme
    {
        /// <summary>
        /// Identifikátor kartičky v sadě.
        /// </summary>
        string Id { get; }

        /// <summary>
        /// Počet měst na kartičce.
        /// </summary>
        int CityCount { get; }

        /// <summary>
        /// Počet regionů na kartičce.
        /// </summary>
        int RegionCount { get; }

        /// <summary>
        /// Vrátí typ regionu.
        /// </summary>
        /// <param name="regionId">Identifikátor regionu.</param>
        /// <returns>Typ regionu.</returns>
        RegionType GetRegionType(int regionId);

        /// <summary>
        /// Vrátí seznam měst na regionu.
        /// </summary>
        /// <param name="regionId">Identifikátor regionu.</param>
        /// <returns>Seznam identifikátoů měst na daném regionu.</returns>
        IEnumerable<int> GetRegionCities(int regionId);

        /// <summary>
        /// Vrátí seznam sousedních regionů daného regionu.
        /// </summary>
        /// <param name="regionId">Identifikátor regionu.</param>
        /// <returns>Seznam identifikátorů regionů sousedících s regionem.</returns>
        IEnumerable<int> GetRegionNeighbors(int regionId);

        /// <summary>
        /// Vrátí seznam okrajů kartičky, na kterých se nachází daný region.
        /// </summary>
        /// <param name="regionId">Identifikátor regionu.</param>
        /// <returns>Okraje kartičky, na kterých se nachází daný region. (Využívá bitovou logiku)</returns>
        TileOrientation GetRegionBorders(int regionId);

        /// <summary>
        /// Vrátí identifikátor regionu, který se nachází na daném okraji kartičky.
        /// </summary>
        /// <param name="border">Okraj kartičky.</param>
        /// <returns>Identifikátor regionu, který se nachází na daném okraji.</returns>
        int GetRegionOnBorder(TileOrientation border);
    }
}
