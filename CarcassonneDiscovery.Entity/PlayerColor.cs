﻿namespace CarcassonneDiscovery.Entity
{
    /// <summary>
    /// Barva hráče.
    /// </summary>
    public enum PlayerColor
    {
        /// <summary>
        /// Žádná.
        /// </summary>
        None = 0,

        /// <summary>
        /// Červená.
        /// </summary>
        Red = 1,

        /// <summary>
        /// Zelená.
        /// </summary>
        Green = 2,

        /// <summary>
        /// Modrá.
        /// </summary>
        Blue = 3,

        /// <summary>
        /// Žlutá.
        /// </summary>
        Yellow = 4,

        /// <summary>
        /// Černá.
        /// </summary>
        Black = 5,

        /// <summary>
        /// Červená (alias).
        /// </summary>
        R = 1,

        /// <summary>
        /// Zelená (alias).
        /// </summary>
        G = 2,

        /// <summary>
        /// Modrá (alias).
        /// </summary>
        B = 3,

        /// <summary>
        /// Žlutá (alias).
        /// </summary>
        Y = 4,

        /// <summary>
        /// Černá (alias).
        /// </summary>
        K = 5,
    }
}
