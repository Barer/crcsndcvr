﻿namespace CarcassonneDiscovery.Entity
{
    using System.Collections.Generic;

    /// <summary>
    /// Stav hry.
    /// </summary>
    public class GameState
    {
        /// <summary>
        /// Parametry hry.
        /// </summary>
        public GameParams Params { get; set; }

        /// <summary>
        /// Poskytovatel kartiček ve hře.
        /// </summary>
        public ITileSupplier TileSupplier { get; set; }

        /// <summary>
        /// Aktuální fáze hry.
        /// </summary>
        public GamePhase Phase { get; set; }

        /// <summary>
        /// Hráč na tahu (index v pořadí hráčů).
        /// </summary>
        public int PlayerOnMove { get; set; }

        /// <summary>
        /// Naposledy vylosovaná kartička.
        /// </summary>
        public ITileScheme Tile { get; set; }

        /// <summary>
        /// Souřadnice naposledy umístěné kartičky.
        /// </summary>
        public Coords Coords { get; set; }

        /// <summary>
        /// Mapa umístěných kartiček.
        /// </summary>
        public IDictionary<Coords, TilePlacement> Grid { get; set; }

        /// <summary>
        /// Umístěné figurky jednotlivých hráčů.
        /// </summary>
        public IDictionary<PlayerColor, IList<FollowerPlacement>> PlacedFollowers { get; set; }

        /// <summary>
        /// Skóre jednotlivých hráčů.
        /// </summary>
        public IDictionary<PlayerColor, int> Score { get; set; }
    }
}
