﻿namespace CarcassonneDiscovery.Entity
{
    /// <summary>
    /// Souřadnice kartičky.
    /// </summary>
    public struct Coords
    {
        /// <summary>
        /// X-ová souřadnice.
        /// </summary>
        public int X { get; set; }

        /// <summary>
        /// Y-ová souřadnice.
        /// </summary>
        public int Y { get; set; }

        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="x">X-ová souřadnice.</param>
        /// <param name="y">Y-ová souřadnice.</param>
        public Coords(int x, int y)
        {
            X = x;
            Y = y;
        }

        /// <inheritdoc/>
        public static bool operator ==(Coords a, Coords b)
        {
            return a.X == b.X && a.Y == b.Y;
        }

        /// <inheritdoc/>
        public static bool operator !=(Coords a, Coords b)
        {
            return !(a == b);
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if (obj is Coords)
            {
                return this == (Coords)obj;
            }

            return false;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return (X << 16) ^ Y;
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"[{X},{Y}]";
        }
    }
}
