﻿namespace CarcassonneDiscovery.Entity
{
    /// <summary>
    /// Umístění kartičky na mapě.
    /// </summary>
    public class TilePlacement
    {
        /// <summary>
        /// Souřadnice kartičky.
        /// </summary>
        public Coords Coords { get; set; }

        /// <summary>
        /// Orientace kartičky.
        /// </summary>
        public TileOrientation Orientation { get; set; }

        /// <summary>
        /// Schéma kartičky.
        /// </summary>
        public ITileScheme Tile { get; set; }

        /// <summary>
        /// Umístění figurky na kartičce.
        /// </summary>
        public FollowerPlacement FollowerPlacement { get; set; }
    }
}
