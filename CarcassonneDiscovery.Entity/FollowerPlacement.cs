﻿namespace CarcassonneDiscovery.Entity
{
    /// <summary>
    /// Umístění figurky na mapě.
    /// </summary>
    public class FollowerPlacement
    {
        /// <summary>
        /// Barva figurky.
        /// </summary>
        public PlayerColor Color { get; set; }
        /// <summary>
        /// Souřadnice kartičky, na kterém se nachází figurka.
        /// </summary>
        public Coords Coords { get; set; }

        /// <summary>
        /// Identifikátor regionu na kartičce, kde je figurka umístěna.
        /// </summary>
        public int RegionId { get; set; }
    }
}
