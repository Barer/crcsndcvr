﻿namespace CarcassonneDiscovery.Server.Web.Controllers
{
    using CarcassonneDiscovery.Api;
    using CarcassonneDiscovery.Server.Base;
    using Microsoft.AspNetCore.Mvc;
    using System.Linq;

    /// <summary>
    /// API kontroler pro herní akce.
    /// </summary>
    [Route("Api/Game")]
    public class GameApiController : ApiControllerBase
    {
        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="serverManager">Řízení herního serveru.</param>
        public GameApiController(ServerManager serverManager) : base(serverManager)
        {
        }

        /// <summary>
        /// Žádost o výsledek herní akce.
        /// </summary>
        /// <param name="request">Parametry.</param>
        /// <returns>Výsledek akce.</returns>
        [Route("GameAction")]
        public GameActionResponse GameAction([FromBody]GameActionRequest request)
        {
            return SafeExecute<GameActionRequest, GameActionResponse>(request, () =>
            {
                var action = ServerManager.GetAction(request.GameId, request.ActionId);

                return new GameActionResponse
                {
                    Type = action.Type,
                    Color = action.Color,
                    Coords = action.Coords,
                    TileId = action.TileId,
                    TileOrientation = action.TileOrientation,
                    RegionId = action.RegionId,
                    Score = action.Score,
                    ErrorCode = ErrorCode.Ok
                };
            });
        }

        /// <summary>
        /// Žádost o detail hry.
        /// </summary>
        /// <param name="request">Parametry.</param>
        /// <returns>Výsledek akce.</returns>
        [Route("GameDetail")]
        public GameDetailResponse GameDetail([FromBody]GameDetailRequest request)
        {
            return SafeExecute<GameDetailRequest, GameDetailResponse>(request, () =>
            {
                var response = new GameDetailResponse();

                var game = ServerManager.GetGameDetail(request.GameId);

                response.Progress = game.Progress;
                response.FollowerCount = game.FollowerCount;
                response.TileSetId = game.TileSetId;
                response.Players = game.GamePlayers.Select(x => new GamePlayerInfo
                {
                    Order = x.Order,
                    Color = x.Color,
                    Name = x.Name,
                    Score = x.Score
                }).ToList();

                response.ErrorCode = ErrorCode.Ok;

                return response;
            });
        }

        /// <summary>
        /// Žádost o umístění kartičky.
        /// </summary>
        /// <param name="request">Parametry.</param>
        /// <returns>Výsledek akce.</returns>
        [Route("PlaceTile")]
        public ResponseBase PlaceTile([FromBody]PlaceTileRequest request)
        {
            return SafeExecute<PlaceTileRequest, ResponseBase>(request, () =>
            {
                ServerManager.PlaceTile(request.GameId, request.Color, request.TileId, request.Coords, request.Orientation);
            });
        }

        /// <summary>
        /// Žádost o umístění figurky.
        /// </summary>
        /// <param name="request">Parametry.</param>
        /// <returns>Výsledek akce.</returns>
        [Route("PlaceFollower")]
        public ResponseBase PlaceFollower([FromBody]PlaceFollowerRequest request)
        {
            return SafeExecute<PlaceFollowerRequest, ResponseBase>(request, () =>
            {
                ServerManager.PlaceFollower(request.GameId, request.Color, request.Coords, request.RegionId);
            });
        }

        /// <summary>
        /// Žádost o odebrání figurky.
        /// </summary>
        /// <param name="request">Parametry.</param>
        /// <returns>Výsledek akce.</returns>
        [Route("RemoveFollower")]
        public ResponseBase RemoveFollower([FromBody]RemoveFollowerRequest request)
        {
            return SafeExecute<RemoveFollowerRequest, ResponseBase>(request, () =>
            {
                ServerManager.RemoveFollower(request.GameId, request.Color, request.Coords, request.RegionId);
            });
        }

        /// <summary>
        /// Žádost o předání tahu.
        /// </summary>
        /// <param name="request">Parametry.</param>
        /// <returns>Výsledek akce.</returns>
        [Route("PassMove")]
        public ResponseBase PassMove([FromBody]PassMoveRequest request)
        {
            return SafeExecute<PassMoveRequest, ResponseBase>(request, () =>
             {
                 ServerManager.PassMove(request.GameId, request.Color);
             });
        }
    }
}
