﻿namespace CarcassonneDiscovery.Server.Web.Controllers
{
    using CarcassonneDiscovery.Api;
    using CarcassonneDiscovery.Entity;
    using CarcassonneDiscovery.Server.Base;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// API kontroler pro pomocné akce.
    /// </summary>
    [Route("Api/Control")]
    public class ControlApiController : ApiControllerBase
    {
        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="serverManager">Řízení herního serveru.</param>
        public ControlApiController(ServerManager serverManager) : base(serverManager)
        {
        }

        /// <summary>
        /// Žádost o zahájení nové hry.
        /// </summary>
        /// <param name="request">Parametry.</param>
        /// <returns>Výsledek akce.</returns>
        [Route("StartGame")]
        public StartGameResponse StartGame([FromBody]StartGameRequest request)
        {
            return SafeExecute<StartGameRequest, StartGameResponse>(request, () =>
            {
                var response = new StartGameResponse
                {
                    GameId = ServerManager.StartGame(new GameParams
                    {
                        FollowerCount = request.FollowerCount,
                        PlayerOrder = request.PlayerOrder,
                        TileSetId = request.TileSetId
                    }),
                    ErrorCode = ErrorCode.Ok
                };

                return response;
            });
        }

        /// <summary>
        /// Žádost o ukončení hry.
        /// </summary>
        /// <param name="request">Parametry.</param>
        /// <returns>Výsledek akce.</returns>
        [Route("StopGame")]
        public ResponseBase StopGame([FromBody]StopGameRequest request)
        {
            return SafeExecute<StopGameRequest, ResponseBase>(request, () =>
             {
                 ServerManager.StopGame(request.GameId);
             });
        }

        /// <summary>
        /// Žádost o seznam her v průběhu.
        /// </summary>
        /// <returns>Výsledek akce.</returns>
        [Route("GameList")]
        public GameListResponse GameList()
        {
            return SafeExecute<int, GameListResponse>(0, () =>
            {
                var response = new GameListResponse
                {
                    InProgressIds = ServerManager.GetGamesInProgress(),
                    FinishedIds = ServerManager.GetFinishedGames(),
                    ErrorCode = ErrorCode.Ok
                };

                return response;
            });
        }

        /// <summary>
        /// Žádost o archivaci ukončených her.
        /// </summary>
        /// <returns>Výsledek akce.</returns>
        [Route("ArchiveFinishedGames")]
        public ResponseBase ArchiveFinishedGames()
        {
            return SafeExecute<int, ResponseBase>(0, () =>
            {
                ServerManager.ArchiveFinishedGames();
            });
        }

        /// <summary>
        /// Žádost o registraci jména hráče.
        /// </summary>
        /// <param name="request">Parametry.</param>
        /// <returns>Výsledek akce.</returns>
        [Route("RegisterName")]
        public ResponseBase RegisterName([FromBody]RegisterNameRequest request)
        {
            return SafeExecute<RegisterNameRequest, ResponseBase>(request, () =>
            {
                ServerManager.RegisterName(request.GameId, request.Color, request.Name);
            });
        }
    }
}
