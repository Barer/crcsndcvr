﻿namespace CarcassonneDiscovery.Server.Web.Controllers
{
    using CarcassonneDiscovery.Api;
    using CarcassonneDiscovery.Server.Base;
    using CarcassonneDiscovery.Server.Database;
    using Microsoft.AspNetCore.Mvc;
    using System;

    /// <summary>
    /// Základ pro API kontrollery.
    /// </summary>
    public abstract class ApiControllerBase : Controller
    {
        /// <summary>
        /// Řízení herního serveru.
        /// </summary>
        protected ServerManager ServerManager;

        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="serverManager">Řízení herního serveru.</param>
        public ApiControllerBase(
            ServerManager serverManager
            )
        {
            ServerManager = serverManager;
        }


        /// <summary>
        /// Bezpečně vyřídí žádost.
        /// </summary>
        /// <typeparam name="TResponse">Typ výsledku žádosti.</typeparam>
        /// <param name="action">Funkce vyřizující žádost.</param>
        /// <returns>Výsledek žádosti.</returns>
        protected TResponse SafeExecute<TRequest, TResponse>(TRequest request, Action action)
            where TResponse : ResponseBase, new()
        {
            return SafeExecute(request, () =>
            {
                action.Invoke();
                return new TResponse();
            });
        }

        /// <summary>
        /// Bezpečně vyřídí žádost.
        /// </summary>
        /// <typeparam name="TRequest">Typ žádosti.</typeparam>
        /// <typeparam name="TResponse">Typ výsledku žádosti.</typeparam>
        /// <param name="request">Žádost.</param>
        /// <param name="func">Funkce vyřizující žádost.</param>
        /// <returns>Výsledek žádosti.</returns>
        protected TResponse SafeExecute<TRequest, TResponse>(TRequest request, Func<TResponse> func)
            where TResponse : ResponseBase, new()
        {
            if (request == null)
            {
                return new TResponse
                {
                    ErrorCode = ErrorCode.InvalidParameters,
                    ErrorMessage = ErrorCode.InvalidParameters.ToString()
                };
            }

            try
            {
                var response = func.Invoke();
                response.ErrorCode = ErrorCode.Ok;
                return response;
            }
            catch (RequestException ex) // TODO: Exception refactoring
            {
                return new TResponse
                {
                    ErrorCode = ex.ErrorCode,
                    ErrorMessage = ex.ErrorCode.ToString()
                };
            }
        }
    }
}
