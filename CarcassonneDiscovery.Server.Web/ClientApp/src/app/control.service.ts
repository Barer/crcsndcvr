import { Injectable, Inject } from '@angular/core';
import { AppModule } from './app.module';
import { HttpClient } from '@angular/common/http';
import { RegisterNameRequest, StopGameRequest, StartGameRequest } from './models/models.request';
import { Observable } from 'rxjs';
import { ResponseBase, StartGameResponse, GameListResponse } from './models/models.response';

@Injectable({
  providedIn: 'root',
})
export class ControlService {

  constructor(
    private http: HttpClient,
    @Inject('BASE_URL') private baseUrl: string
  ) {
  }

  public startGame(request: StartGameRequest): Observable<StartGameResponse> {
    return this.http.post<StartGameResponse>(this.baseUrl + 'Api/Control/StartGame',
      request
    );
  }

  public stopGame(request: StopGameRequest): Observable<ResponseBase> {
    return this.http.post<ResponseBase>(this.baseUrl + 'Api/Control/StopGame',
      request
    );
  }

  public registerName(request: RegisterNameRequest): Observable<ResponseBase> {
    return this.http.post<ResponseBase>(this.baseUrl + 'Api/Control/RegisterName',
      request
    );
  }

  public gameList(): Observable<GameListResponse> {
    return this.http.post<GameListResponse>(this.baseUrl + 'Api/Control/GameList',
      null
    );
  }

  public archiveFinishedGames(): Observable<ResponseBase> {
    return this.http.post<ResponseBase>(this.baseUrl + 'Api/Control/ArchiveFinishedGames',
      null
    );
  }
}
