import { Component, Inject, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PlayerColor, TileOrientation, ErrorCode } from '../models/models.enum';
import { GameService } from '../game.service';
import { PlaceTileRequest, PlaceFollowerRequest, RemoveFollowerRequest, PassMoveRequest, RegisterNameRequest, StopGameRequest } from '../models/models.request';
import { ResponseBase } from '../models/models.response';
import { Observable } from 'rxjs';
import { ControlService } from '../control.service';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html'
})
export class GameControlComponent {
  @Input('gameId') private gameId: number;

  public optionsColor = [
    { viewName: "Červená", value: PlayerColor.Red },
    { viewName: "Zelená", value: PlayerColor.Green },
    { viewName: "Modrá", value: PlayerColor.Blue },
    { viewName: "Žlutá", value: PlayerColor.Yellow },
    { viewName: "Černá", value: PlayerColor.Black },
  ];

  public optionsOrientation = [
    { viewName: "N", value: TileOrientation.N },
    { viewName: "E", value: TileOrientation.E },
    { viewName: "S", value: TileOrientation.S },
    { viewName: "W", value: TileOrientation.W }
  ];

  public selectedColor: number;
  public selectedX: number;
  public selectedY: number;
  public selectedTileId: string;
  public selectedRegionId: number;
  public selectedOrientation: number;
  public selectedName: string;

  constructor(
    private gameSvc: GameService,
    private controlSvc: ControlService
  ) {
  }

  sendPlaceTile(): void {
    if (this.selectedColor === undefined || this.selectedTileId === undefined ||
      this.selectedX === undefined || this.selectedY === undefined ||
      this.selectedOrientation === undefined) {
      return;
    }

    const request: PlaceTileRequest = {
      gameId: this.gameId,
      color: Number(this.selectedColor),
      tileId: this.selectedTileId,
      coordsX: this.selectedX,
      coordsY: this.selectedY,
      orientation: Number(this.selectedOrientation)
    };

    const response = this.gameSvc.placeTile(request);

    this.resolveResponse(response);
  }

  sendPlaceFollower(): void {
    if (this.selectedColor === undefined || this.selectedRegionId === undefined ||
      this.selectedX === undefined || this.selectedY === undefined) {
      return;
    }

    const request: PlaceFollowerRequest = {
      gameId: this.gameId,
      color: Number(this.selectedColor),
      regionId: Number(this.selectedRegionId),
      coordsX: this.selectedX,
      coordsY: this.selectedY
    };

    const response = this.gameSvc.placeFollower(request);

    this.resolveResponse(response);
  }

  sendRemoveFollower(): void {
    if (this.selectedColor === undefined ||
      this.selectedX === undefined || this.selectedY === undefined) {
      return;
    }

    const request: RemoveFollowerRequest = {
      gameId: this.gameId,
      color: Number(this.selectedColor),
      regionId: Number(this.selectedRegionId),
      coordsX: this.selectedX,
      coordsY: this.selectedY
    };

    const response = this.gameSvc.removeFollower(request);

    this.resolveResponse(response);
  }

  sendPassMove(): void {
    if (this.selectedColor === undefined) {
      return;
    }

    const request: PassMoveRequest = {
      gameId: this.gameId,
      color: Number(this.selectedColor)
    };

    const response = this.gameSvc.passMove(request);

    this.resolveResponse(response);
  }

  sendRegisterName(): void {
    if (this.selectedColor === undefined) {
      return;
    }

    const request: RegisterNameRequest = {
      gameId: this.gameId,
      color: Number(this.selectedColor),
      name: this.selectedName
    };

    const response = this.controlSvc.registerName(request);

    this.resolveResponse(response);
  }

  sendStopGame(): void {
    const request: StopGameRequest = {
      gameId : this.gameId
    };

    const response = this.controlSvc.stopGame(request);

    this.resolveResponse(response);
  }

  private resolveResponse(responseObservable: Observable<ResponseBase>): void {
    responseObservable.subscribe(response => {
      if (response.errorCode !== ErrorCode.Ok) {
        alert(response.errorMessage);
      }
    }, error => {
      alert(error.message);
    });
  }
}

