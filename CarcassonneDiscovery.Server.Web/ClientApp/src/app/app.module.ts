import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { GameDetailComponent } from './game-detail/game-detail.component';
import { GameControlComponent } from './game-control/game-control.component';
import { GameListComponent } from './game-list/game-list.component';
import { ControlService } from './control.service';
import { GameService } from './game.service';
import { GameStartComponent } from './game-start/game-start.component';


@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    GameDetailComponent,
    GameControlComponent,
    GameListComponent,
    GameStartComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'game-list', component: GameListComponent },
      { path: 'game-detail/:id', component: GameDetailComponent },
      { path: 'game-start', component: GameStartComponent },
    ])
  ],
  providers: [
    GameService,
    ControlService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
