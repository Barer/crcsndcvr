"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var StartGameRequest = /** @class */ (function () {
    function StartGameRequest() {
    }
    return StartGameRequest;
}());
exports.StartGameRequest = StartGameRequest;
var StopGameRequest = /** @class */ (function () {
    function StopGameRequest() {
    }
    return StopGameRequest;
}());
exports.StopGameRequest = StopGameRequest;
var RegisterNameRequest = /** @class */ (function () {
    function RegisterNameRequest() {
    }
    return RegisterNameRequest;
}());
exports.RegisterNameRequest = RegisterNameRequest;
//# sourceMappingURL=models.request.control.js.map