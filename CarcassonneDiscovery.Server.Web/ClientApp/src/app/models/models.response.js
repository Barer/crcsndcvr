"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var GamePlayerInfo = /** @class */ (function () {
    function GamePlayerInfo() {
    }
    return GamePlayerInfo;
}());
exports.GamePlayerInfo = GamePlayerInfo;
var ResponseBase = /** @class */ (function () {
    function ResponseBase() {
    }
    return ResponseBase;
}());
exports.ResponseBase = ResponseBase;
var GameListResponse = /** @class */ (function (_super) {
    __extends(GameListResponse, _super);
    function GameListResponse() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return GameListResponse;
}(ResponseBase));
exports.GameListResponse = GameListResponse;
var StartGameResponse = /** @class */ (function (_super) {
    __extends(StartGameResponse, _super);
    function StartGameResponse() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return StartGameResponse;
}(ResponseBase));
exports.StartGameResponse = StartGameResponse;
var GameDetailResponse = /** @class */ (function (_super) {
    __extends(GameDetailResponse, _super);
    function GameDetailResponse() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return GameDetailResponse;
}(ResponseBase));
exports.GameDetailResponse = GameDetailResponse;
var GameActionResponse = /** @class */ (function (_super) {
    __extends(GameActionResponse, _super);
    function GameActionResponse() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return GameActionResponse;
}(ResponseBase));
exports.GameActionResponse = GameActionResponse;
//# sourceMappingURL=models.response.js.map