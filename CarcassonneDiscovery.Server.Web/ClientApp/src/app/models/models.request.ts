import { PlayerColor, TileOrientation } from "./models.enum";

export class StartGameRequest {
  followerCount: number;
  playerOrder: PlayerColor[];
  tileSetId: string;
}

export class StopGameRequest {
  gameId: number;
}

export class RegisterNameRequest {
  gameId: number;
  color: PlayerColor;
  name: string;
}

export class ActionRequestBase {
  gameId: number;
  color: PlayerColor;
}

export class PlaceTileRequest extends ActionRequestBase {
  tileId: string;
  coordsX: number;
  coordsY: number;
  orientation: TileOrientation;
}

export class PlaceFollowerRequest extends ActionRequestBase {
  regionId: number;
  coordsX: number;
  coordsY: number;
}

export class RemoveFollowerRequest extends ActionRequestBase {
  regionId: number;
  coordsX: number;
  coordsY: number;
}

export class PassMoveRequest extends ActionRequestBase {
}

export class GameActionRequest {
  gameId: number;
  actionId: number;
}
export class GameDetailRequest {
  gameId: number;
}
