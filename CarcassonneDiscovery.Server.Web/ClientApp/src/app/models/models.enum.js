"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PlayerColor;
(function (PlayerColor) {
    PlayerColor[PlayerColor["None"] = 0] = "None";
    PlayerColor[PlayerColor["Red"] = 1] = "Red";
    PlayerColor[PlayerColor["Green"] = 2] = "Green";
    PlayerColor[PlayerColor["Blue"] = 3] = "Blue";
    PlayerColor[PlayerColor["Yellow"] = 4] = "Yellow";
    PlayerColor[PlayerColor["Black"] = 5] = "Black";
    PlayerColor[PlayerColor["R"] = 1] = "R";
    PlayerColor[PlayerColor["G"] = 2] = "G";
    PlayerColor[PlayerColor["B"] = 3] = "B";
    PlayerColor[PlayerColor["Y"] = 4] = "Y";
    PlayerColor[PlayerColor["K"] = 5] = "K";
})(PlayerColor = exports.PlayerColor || (exports.PlayerColor = {}));
var TileOrientation;
(function (TileOrientation) {
    TileOrientation[TileOrientation["N"] = 1] = "N";
    TileOrientation[TileOrientation["E"] = 2] = "E";
    TileOrientation[TileOrientation["S"] = 4] = "S";
    TileOrientation[TileOrientation["W"] = 8] = "W";
})(TileOrientation = exports.TileOrientation || (exports.TileOrientation = {}));
var GameActionType;
(function (GameActionType) {
    GameActionType[GameActionType["StartGame"] = 0] = "StartGame";
    GameActionType[GameActionType["StartMove"] = 1] = "StartMove";
    GameActionType[GameActionType["PlaceTile"] = 2] = "PlaceTile";
    GameActionType[GameActionType["PlaceFollower"] = 3] = "PlaceFollower";
    GameActionType[GameActionType["RemoveFollower"] = 4] = "RemoveFollower";
    GameActionType[GameActionType["PassMove"] = 5] = "PassMove";
    GameActionType[GameActionType["GameEnd"] = 6] = "GameEnd";
    GameActionType[GameActionType["GameQuit"] = 7] = "GameQuit";
})(GameActionType = exports.GameActionType || (exports.GameActionType = {}));
var GameProgress;
(function (GameProgress) {
    GameProgress[GameProgress["InProgress"] = 0] = "InProgress";
    GameProgress[GameProgress["Finished"] = 1] = "Finished";
    GameProgress[GameProgress["Dead"] = 2] = "Dead";
    GameProgress[GameProgress["Other"] = 3] = "Other";
})(GameProgress = exports.GameProgress || (exports.GameProgress = {}));
var ErrorCode = /** @class */ (function () {
    function ErrorCode() {
    }
    ErrorCode.Ok = 0;
    return ErrorCode;
}());
exports.ErrorCode = ErrorCode;
//# sourceMappingURL=models.enum.js.map