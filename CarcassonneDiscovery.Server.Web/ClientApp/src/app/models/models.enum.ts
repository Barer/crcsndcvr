export enum PlayerColor {
  None = 0,
  Red = 1,
  Green = 2,
  Blue = 3,
  Yellow = 4,
  Black = 5,
  R = 1,
  G = 2,
  B = 3,
  Y = 4,
  K = 5
}

export enum TileOrientation {
  N = 1,
  E = 2,
  S = 4,
  W = 8
}

export enum GameActionType {
  StartGame = 0,
  StartMove = 1,
  PlaceTile = 2,
  PlaceFollower = 3,
  RemoveFollower = 4,
  PassMove = 5,
  GameEnd = 6,
  GameQuit = 7
}

export enum GameProgress {
  InProgress = 0,
  Finished = 1,
  Dead = 2,
  Other = 3
}

export class ErrorCode {
  static Ok: number = 0;
}
