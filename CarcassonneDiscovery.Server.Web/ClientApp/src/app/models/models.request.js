"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var StartGameRequest = /** @class */ (function () {
    function StartGameRequest() {
    }
    return StartGameRequest;
}());
exports.StartGameRequest = StartGameRequest;
var StopGameRequest = /** @class */ (function () {
    function StopGameRequest() {
    }
    return StopGameRequest;
}());
exports.StopGameRequest = StopGameRequest;
var RegisterNameRequest = /** @class */ (function () {
    function RegisterNameRequest() {
    }
    return RegisterNameRequest;
}());
exports.RegisterNameRequest = RegisterNameRequest;
var ActionRequestBase = /** @class */ (function () {
    function ActionRequestBase() {
    }
    return ActionRequestBase;
}());
exports.ActionRequestBase = ActionRequestBase;
var PlaceTileRequest = /** @class */ (function (_super) {
    __extends(PlaceTileRequest, _super);
    function PlaceTileRequest() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return PlaceTileRequest;
}(ActionRequestBase));
exports.PlaceTileRequest = PlaceTileRequest;
var PlaceFollowerRequest = /** @class */ (function (_super) {
    __extends(PlaceFollowerRequest, _super);
    function PlaceFollowerRequest() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return PlaceFollowerRequest;
}(ActionRequestBase));
exports.PlaceFollowerRequest = PlaceFollowerRequest;
var RemoveFollowerRequest = /** @class */ (function (_super) {
    __extends(RemoveFollowerRequest, _super);
    function RemoveFollowerRequest() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return RemoveFollowerRequest;
}(ActionRequestBase));
exports.RemoveFollowerRequest = RemoveFollowerRequest;
var PassMoveRequest = /** @class */ (function (_super) {
    __extends(PassMoveRequest, _super);
    function PassMoveRequest() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return PassMoveRequest;
}(ActionRequestBase));
exports.PassMoveRequest = PassMoveRequest;
var GameActionRequest = /** @class */ (function () {
    function GameActionRequest() {
    }
    return GameActionRequest;
}());
exports.GameActionRequest = GameActionRequest;
var GameDetailRequest = /** @class */ (function () {
    function GameDetailRequest() {
    }
    return GameDetailRequest;
}());
exports.GameDetailRequest = GameDetailRequest;
//# sourceMappingURL=models.request.js.map