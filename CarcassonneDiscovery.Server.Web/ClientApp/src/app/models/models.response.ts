import { PlayerColor, ErrorCode, GameActionType, TileOrientation, GameProgress } from "./models.enum";

export class GamePlayerInfo {
  order: number;
  name: string;
  color: PlayerColor;
  score: number;
}

export class ResponseBase {
  errorCode: number;
  errorMessage: string;
}

export class GameListResponse extends ResponseBase {
  inProgressIds: number[];
  finishedIds: number[];
}

export class StartGameResponse extends ResponseBase {
  gameId: number;
}

export class GameDetailResponse extends ResponseBase {
  progress: GameProgress;
  tileSetId: string;
  followerCount: number;
  players: GamePlayerInfo[];
}

export class GameActionResponse extends ResponseBase {
  type: GameActionType;
  color: PlayerColor;
  coordsX: number;
  coordsY: number;
  tileId: string;
  tileOrientation: TileOrientation;
  regionId: number;
  score: number;
}
