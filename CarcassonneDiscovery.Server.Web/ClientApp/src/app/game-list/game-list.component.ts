import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { ControlService } from '../control.service';
import { StartGameRequest } from '../models/models.request';
import { PlayerColor, ErrorCode } from '../models/models.enum';

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html'
})
export class GameListComponent {
  public inProgress: number[] = [];
  public finished: number[] = [];

  constructor(
    private router: Router,
    private controlSvc: ControlService
  ) {
    this.refresh();
  }

  refresh(): void {
    this.controlSvc.gameList().subscribe(response => {
      if (response.errorCode === ErrorCode.Ok) {
        this.inProgress = response.inProgressIds;
        this.finished = response.finishedIds;
      } else {
        alert(response.errorMessage);
      }
    }, error => {
      alert(error.message);
    });
  }

  startNewGame(): void {
    this.router.navigate(['/game-start']);
  }

  archiveFinishedGames(): void {
    this.controlSvc.archiveFinishedGames().subscribe(response => {
      if (response.errorCode === ErrorCode.Ok) {
        this.finished = [];
      } else {
        alert(response.errorMessage);
      }
    }, error => {
      alert(error.message);
    });
  }
}


