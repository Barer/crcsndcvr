import { Component } from '@angular/core';
import { PlayerColor, ErrorCode } from '../models/models.enum';
import { ControlService } from '../control.service';
import { Router } from '@angular/router';
import { StartGameRequest } from '../models/models.request';

@Component({
  selector: 'app-game-start',
  templateUrl: './game-start.component.html'
})
export class GameStartComponent {

  public optionsColor = [
    { viewName: "Nehrající", value: PlayerColor.None },
    { viewName: "Červená", value: PlayerColor.Red },
    { viewName: "Zelená", value: PlayerColor.Green },
    { viewName: "Modrá", value: PlayerColor.Blue },
    { viewName: "Žlutá", value: PlayerColor.Yellow },
    { viewName: "Černá", value: PlayerColor.Black },
  ];

  public optionsFollowerCount = [
    { viewName: 1, value: 1 },
    { viewName: 2, value: 2 },
    { viewName: 3, value: 3 },
    { viewName: 4, value: 4 },
    { viewName: 5, value: 5 },
    { viewName: 6, value: 6 },
    { viewName: 7, value: 7 },
    { viewName: 8, value: 8 },
    { viewName: 9, value: 9 },
    { viewName: 10, value: 10 }
  ];

  public optionsTileSetId = [
    { viewName: "Standardní", value: "Standard" }
  ];

  public color1: PlayerColor = PlayerColor.Red;
  public color2: PlayerColor = PlayerColor.Blue;
  public color3: PlayerColor = PlayerColor.None;
  public color4: PlayerColor = PlayerColor.None;
  public color5: PlayerColor = PlayerColor.None;
  public followerCount: number = 4;
  public tileSetId: string = "Standard";

  constructor(
    private router: Router,
    private controlSvc: ControlService
  ) {
  }

  startGame(): void {
    if (this.color1 === PlayerColor.None || this.color2 === PlayerColor.None) {
      alert("Hru musí hrát alespoň 2 hráči.");
      return;
    } else if (this.color1 === this.color2) {
      alert("Barva hráčů se nesmí opakovat.");
      return;
    }

    let playerOrder: PlayerColor[] = [Number(this.color1), Number(this.color2)];

    if (this.color3 !== PlayerColor.None) {
      if (this.color1 === this.color3 || this.color2 === this.color3) {
        alert("Barva hráčů se nesmí opakovat.");
        return;
      }
      playerOrder.push(Number(this.color3));

      if (this.color4 !== PlayerColor.None) {
        if (this.color1 === this.color4 || this.color2 === this.color4 || this.color3 === this.color4) {
          alert("Barva hráčů se nesmí opakovat.");
          return;
        }
        playerOrder.push(Number(this.color4));

        if (this.color5 !== PlayerColor.None) {
          if (this.color1 === this.color5 || this.color2 === this.color5 ||
            this.color3 === this.color5 || this.color4 === this.color5) {
            alert("Barva hráčů se nesmí opakovat.");
            return;
          }
          playerOrder.push(Number(this.color5));
        }
      }
    }

    const request: StartGameRequest = {
      followerCount: Number(!this.followerCount ? 4 : this.followerCount),
      playerOrder: playerOrder.map(x => <number>x),
      tileSetId: (!this.tileSetId ? "Standard" : this.tileSetId)
    };

    /*const request: StartGameRequest = {
      followerCount: 4,
      playerOrder: [PlayerColor.Red, PlayerColor.Blue],
      tileSetId: "Standard"
    };*/

    this.controlSvc.startGame(request).subscribe(response => {
      if (response.errorCode === ErrorCode.Ok) {
        this.router.navigate(['/game-detail', response.gameId]);
      } else {
        alert(response.errorMessage);
      }
    }, error => {
      alert(error.message);
    });
  }
}

