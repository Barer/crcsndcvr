import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { GameActionType, PlayerColor, ErrorCode, GameProgress } from '../models/models.enum';
import { ControlService } from '../control.service';
import { GameService } from '../game.service';
import { GameActionRequest, GameDetailRequest } from '../models/models.request';
import { GameActionResponse, GameDetailResponse } from '../models/models.response';

@Component({
  selector: 'app-game-detail',
  templateUrl: './game-detail.component.html'
})
export class GameDetailComponent {
  public actions: GameActionResponse[] = [];
  public detail: GameDetailResponse = new GameDetailResponse();
  public gameId: number;

  constructor(
    activatedRoute: ActivatedRoute,
    private gameSvc: GameService
  ) {
    activatedRoute.paramMap.subscribe(params => {
      this.gameId = Number(params.get('id'));
    });

    this.actionLoop(0);
  }

  log(action: GameActionResponse): string {
    if (action.errorCode !== ErrorCode.Ok) {
      return 'Error: ' + action.errorMessage;
    }

    switch (action.type) {
      case GameActionType.StartGame:
        return `StartGame(Tile=${action.tileId},Coords=[${action.coordsX},${action.coordsY}],Or=${action.tileOrientation})`;
      case GameActionType.StartMove:
        return `StartMove(Color=${action.color},Tile=${action.tileId})`;
      case GameActionType.PlaceTile:
        return `PlaceTile(Color=${action.color},Tile=${action.tileId},Coords=[${action.coordsX},${action.coordsY}],Or=${action.tileOrientation})`;
      case GameActionType.PlaceFollower:
        return `PlaceFollower(Color=${action.color},Coords=[${action.coordsX},${action.coordsY}],RegId=${action.regionId})`;
      case GameActionType.RemoveFollower:
        return `RemoveFollower(Color=${action.color},Coords=[${action.coordsX},${action.coordsY}],RegId=${action.regionId},Score=${action.score})`;
      case GameActionType.PassMove:
        return `PassMove(Color=${action.color})`;
      case GameActionType.GameEnd:
        return `GameEnd`;
    }

    return `UnknownResponse`;
  }

  actionLoop(actionId: number): void {
    this.refreshGameDetail();

    const request: GameActionRequest = {
      gameId: this.gameId,
      actionId: actionId
    };

    this.gameSvc.gameAction(request)
      .subscribe(response => {
        const finished = /[Ff]inished/g;
        const timeout = /[Tt]imeout/g;
        if (response.errorCode === ErrorCode.Ok) {
          this.actions.push(response);
          this.actionLoop(actionId + 1);
        } else if (!!response.errorMessage.toLowerCase().match(finished)) {
          // break;
        } else if (!!response.errorMessage.toLowerCase().match(timeout)) {
          setTimeout(() => this.actionLoop(actionId), 5000);
        } else {
          alert(response.errorMessage);
        }
      }, error => {
        alert(error.message);
      });
  }

  refreshGameDetail(): void {
    const request: GameDetailRequest = {
      gameId: this.gameId
    };

    this.gameSvc.gameDetail(request)
      .subscribe(response => {
        if (response.errorCode !== ErrorCode.Ok) {
          alert(response.errorMessage);
        } else {
          this.detail = response;
        }
      }, error => {
        alert(error.message);
      });
  }

  getColorName(colorId: number): string {
    const color = <PlayerColor>colorId;
    switch (color) {
      case PlayerColor.Red:
        return "Červená";
      case PlayerColor.Green:
        return "Zelená";
      case PlayerColor.Blue:
        return "Modrá";
      case PlayerColor.Yellow:
        return "Žlutá";
      case PlayerColor.Black:
        return "Černá";
    }

    return "Neznámá";
  }

  getGameProgress(detail: GameDetailResponse): string {
    switch (detail.progress) {
      case GameProgress.InProgress:
        return "V průběhu";
      case GameProgress.Finished:
        return "Ukončena";
      case GameProgress.Dead:
        return "Nedohratelná";
      case GameProgress.Other:
      default:
        return "Neznámý";
    }
  }
}


