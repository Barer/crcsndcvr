import { Injectable, Inject } from '@angular/core';
import { AppModule } from './app.module';
import { PlaceTileRequest, RemoveFollowerRequest, PlaceFollowerRequest, PassMoveRequest, GameDetailRequest, GameActionRequest } from './models/models.request';
import { HttpClient } from '@angular/common/http';
import { ResponseBase, GameActionResponse, GameDetailResponse } from './models/models.response';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root',
})
export class GameService {

  constructor(
    private http: HttpClient,
    @Inject('BASE_URL') private baseUrl: string
  ) {
  }

  public placeTile(request: PlaceTileRequest): Observable<ResponseBase> {
    return this.http.post<ResponseBase>(this.baseUrl + 'Api/Game/PlaceTile',
      request
    );
  }

  public placeFollower(request: PlaceFollowerRequest): Observable<ResponseBase> {
    return this.http.post<ResponseBase>(this.baseUrl + 'Api/Game/PlaceFollower',
      request
    );
  }

  public removeFollower(request: RemoveFollowerRequest): Observable<ResponseBase> {
    return this.http.post<ResponseBase>(this.baseUrl + 'Api/Game/RemoveFollower',
      request
    );
  }

  public passMove(request: PassMoveRequest): Observable<ResponseBase> {
    return this.http.post<ResponseBase>(this.baseUrl + 'Api/Game/PassMove',
      request
    );
  }

  public gameDetail(request: GameDetailRequest): Observable<GameDetailResponse> {
    return this.http.post<GameDetailResponse>(this.baseUrl + 'Api/Game/GameDetail',
      request
    );
  }

  public gameAction(request: GameActionRequest): Observable<GameActionResponse> {
    return this.http.post<GameActionResponse>(this.baseUrl + 'Api/Game/GameAction',
      request
    );
  }
}
