﻿using CarcassonneDiscovery.Api;
using CarcassonneDiscovery.Entity;
using CarcassonneDiscovery.Server.Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CarcassonneDiscovery.PostprocessingTool
{
    /// <summary>
    /// Hlavní třída programu.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Vstupní bod programu.
        /// </summary>
        /// <param name="args">Argumenty.</param>
        public static void Main(string[] args)
        {
            Console.Write("Enter output file name: ");
            var dbPath = Console.ReadLine();

            var opt = new DbContextOptionsBuilder();
            opt.UseSqlite($"Data source={dbPath}");

            using (var ctx = new DatabaseContext(opt.Options))
            {
                ctx.Database.Migrate();

                var allResults = new List<Game>();

                while (true)
                {
                    bool continuing;
                    while (true)
                    {
                        Console.Write("Do you want to add results? [Y/N] ");
                        var addResults = Console.ReadLine().ToUpper();
                        if (addResults == "Y" || addResults == "N")
                        {
                            continuing = addResults == "Y";
                            break;
                        }
                    }
                    if (!continuing)
                    {
                        break;
                    }

                    var results = LoadResults();
                    allResults.AddRange(results);
                }

                Console.WriteLine("Ordering data");

                foreach (var game in allResults)
                {
                    game.GameActions = game.GameActions.OrderBy(x => x.ActionId).ToList();
                    game.GamePlayers = game.GamePlayers.OrderBy(x => x.Order).ToList();
                }

                var ordered = allResults.OrderBy(x => x.GamePlayers.ElementAtOrDefault(0)?.Name)
                    .ThenBy(x => x.GamePlayers.ElementAtOrDefault(1)?.Name)
                    .ThenBy(x => x.GamePlayers.ElementAtOrDefault(2)?.Name)
                    .ThenBy(x => x.GamePlayers.ElementAtOrDefault(3)?.Name)
                    .ThenBy(x => x.GamePlayers.ElementAtOrDefault(4)?.Name);

                Console.WriteLine("Saving data one by one");

                var count = 0;
                foreach (var g in ordered)
                {
                    if (++count % 100 == 0)
                    {
                        Console.WriteLine($"Saved {count}");
                    }
                    ctx.Games.Add(g);
                    ctx.SaveChanges();
                }
            }

            Console.WriteLine("Finished.");
        }

        /// <summary>
        /// Načte výsledky ze skupiny souborů.
        /// </summary>
        /// <returns></returns>
        protected static IEnumerable<Game> LoadResults()
        {
            Console.Write("Enter base path to table files (w/o \".games.csv\" etc): ");
            var basePath = Console.ReadLine();
            var gamesPath = basePath + ".games.csv";
            var gamePlayersPath = basePath + ".game-players.csv";
            var gameActionsPath = basePath + ".game-actions.csv";

            Console.Write("Does csv contain header line? [Y/N; default N] ");
            var header = Console.ReadLine().ToUpper() == "Y";
            Console.Write("Which delimiter is used in csv? ");
            var delim = Console.ReadLine();

            var gameData = new Dictionary<int, Game>();

            Predicate<string> isNull = (s) => s.ToUpper() == "NULL" || string.IsNullOrEmpty(s);

            try
            {
                foreach (var line in ReadFile(gamesPath, header, true))
                {
                    var splitted = line.Split(delim);

                    var game = new Game
                    {
                        Progress = (GameProgress)int.Parse(splitted[1]),
                        PlayerCount = int.Parse(splitted[2]),
                        TileSetId = isNull.Invoke(splitted[3]) ? (string)null : splitted[3],
                        FollowerCount = int.Parse(splitted[4]),
                        GamePlayers = new List<GamePlayer>(),
                        GameActions = new List<GameAction>()
                    };

                    gameData.Add(int.Parse(splitted[0]), game);
                }

                foreach (var line in ReadFile(gamePlayersPath, header, true))
                {
                    var splitted = line.Split(delim);

                    var game = gameData[int.Parse(splitted[1])];

                    var gamePlayer = new GamePlayer
                    {
                        Game = game,
                        Order = int.Parse(splitted[2]),
                        Color = (PlayerColor)int.Parse(splitted[3]),
                        Score = int.Parse(splitted[4]),
                        Name = isNull.Invoke(splitted[5]) ? (string)null : splitted[5]
                    };

                    game.GamePlayers.Add(gamePlayer);
                }

                foreach (var line in ReadFile(gameActionsPath, header, true))
                {
                    var splitted = line.Split(delim);

                    var game = gameData[int.Parse(splitted[1])];

                    var gameAction = new GameAction
                    {
                        Game = game,
                        ActionId = int.Parse(splitted[2]),
                        Type = (GameActionType)int.Parse(splitted[3]),
                        Color = isNull.Invoke(splitted[4]) ? (PlayerColor?)null : (PlayerColor)int.Parse(splitted[4]),
                        CoordsX = isNull.Invoke(splitted[5]) ? (int?)null : int.Parse(splitted[5]),
                        CoordsY = isNull.Invoke(splitted[6]) ? (int?)null : int.Parse(splitted[6]),
                        TileId = isNull.Invoke(splitted[7]) ? (string)null : splitted[7],
                        TileOrientation = isNull.Invoke(splitted[8]) ? (TileOrientation?)null : (TileOrientation)int.Parse(splitted[8]),
                        RegionId = isNull.Invoke(splitted[9]) ? (int?)null : int.Parse(splitted[9]),
                        Score = isNull.Invoke(splitted[10]) ? (int?)null : int.Parse(splitted[10]),
                    };

                    game.GameActions.Add(gameAction);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error while adding data: {ex.Message}");
                Console.WriteLine(ex.StackTrace);
                return new Game[0];
            }

            return gameData.Values;
        }

        /// <summary>
        /// Přečte vstup po řádcích.
        /// </summary>
        /// <param name="filePath">Cesta k souboru</param>
        /// <param name="skipFirst">Přeskočit první řádek?</param>
        /// <param name="skipEmpty">Přeskočit prázdné řádky?</param>
        /// <returns>Enumerátor nad řádky.</returns>
        protected static IEnumerable<string> ReadFile(string filePath, bool skipFirst, bool skipEmpty)
        {
            using (var sr = new StreamReader(filePath))
            {
                var first = true;

                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    if ((first && skipFirst) || (string.IsNullOrWhiteSpace(line) && skipEmpty))
                    {
                        first = false;
                        continue;
                    }

                    yield return line;
                }
            }
        }
    }
}