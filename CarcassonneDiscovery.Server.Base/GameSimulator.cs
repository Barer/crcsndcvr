﻿namespace CarcassonneDiscovery.Server.Base
{
    using CarcassonneDiscovery.Api;
    using CarcassonneDiscovery.Entity;
    using CarcassonneDiscovery.Execution;
    using CarcassonneDiscovery.Server.Database;
    using Microsoft.Extensions.DependencyInjection;
    using System.Collections.Generic;

    /// <summary>
    /// Simulátor hry.
    /// </summary>
    public class GameSimulator : IGameSimulator
    {
        /// <summary>
        /// Mapování <see cref="ExecutionErrorCode"/> na <see cref="ErrorCode"/>.
        /// </summary>
        private static readonly IDictionary<ExecutionErrorCode, ErrorCode> ErrorCodes;

        /// <summary>
        /// Statický konstruktor.
        /// </summary>
        static GameSimulator()
        {
            ErrorCodes = new Dictionary<ExecutionErrorCode, ErrorCode>
            {
                { ExecutionErrorCode.AlreadyOccupied, ErrorCode.AlreadyOccupied },
                { ExecutionErrorCode.InvalidGameAction, ErrorCode.InvalidGameAction },
                { ExecutionErrorCode.InvalidPlayerOnMove, ErrorCode.InvalidPlayerOnMove },
                { ExecutionErrorCode.NoFollowerLeft, ErrorCode.NoFollowerLeft },
                { ExecutionErrorCode.NoFollowerOnCoords, ErrorCode.NoFollowerOnCoords },
                { ExecutionErrorCode.NoPlaceableTileRemaining, ErrorCode.NoPlaceableTileRemaining },
                { ExecutionErrorCode.NoSurroundings, ErrorCode.NoSurroundings },
                { ExecutionErrorCode.NotEmptyCoords, ErrorCode.NotEmptyCoords },
                { ExecutionErrorCode.NoTileRemaining, ErrorCode.NoTileRemaining },
                { ExecutionErrorCode.NotOwnFollower, ErrorCode.NotOwnFollower },
                { ExecutionErrorCode.NotSameCoords, ErrorCode.NotSameCoords },
                { ExecutionErrorCode.NotSameTile, ErrorCode.NotSameTile },
                { ExecutionErrorCode.Ok, ErrorCode.Ok },
                { ExecutionErrorCode.TileRemaining, ErrorCode.TileRemaining },
                { ExecutionErrorCode.WrongSurroundings, ErrorCode.WrongSurroundings }
            };
        }

        /// <summary>
        /// Factory pro scope.
        /// </summary>
        private readonly IServiceScopeFactory ServiceScopeFactory;

        /// <summary>
        /// Zámek při úpravách.
        /// </summary>
        private readonly object Lock;

        /// <summary>
        /// Stav hry.
        /// </summary>
        private GameState State;

        /// <inheritdoc/>
        public int GameId { get; private set; }

        /// <inheritdoc/>
        public bool Finished { get; private set; }

        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="serviceScopeFactory">Factory pro scope.</param>
        public GameSimulator(IServiceScopeFactory serviceScopeFactory)
        {
            ServiceScopeFactory = serviceScopeFactory;
            Lock = new object();
            Finished = false;
        }

        /// <inheritdoc/>
        public GameAction GetAction(int actionId)
        {
            using (var scope = ServiceScopeFactory.CreateScope())
            {
                var repository = scope.ServiceProvider.GetService<Repository>();
                return repository.GetAction(GameId, actionId);
            }
        }

        /// <inheritdoc/>
        public Game GetDetail()
        {
            using (var scope = ServiceScopeFactory.CreateScope())
            {
                var repository = scope.ServiceProvider.GetService<Repository>();
                return repository.GetGame(GameId, true);
            }
        }

        /// <inheritdoc/>
        public void RegisterName(PlayerColor color, string name)
        {
            using (var scope = ServiceScopeFactory.CreateScope())
            {
                var repository = scope.ServiceProvider.GetService<Repository>();
                repository.RegisterPlayerName(GameId, color, name);
            }
        }

        /// <inheritdoc/>
        public void Start(GameParams gameParams)
        {
            lock (Lock)
            {
                if (gameParams == null ||
                    gameParams.FollowerCount < 0 ||
                    gameParams.PlayerOrder == null || gameParams.PlayerOrder.Length == 0 ||
                    gameParams.TileSetId != "Standard")
                {
                    throw new RequestException(ErrorCode.InvalidParameters);
                }

                using (var scope = ServiceScopeFactory.CreateScope())
                {
                    var repository = scope.ServiceProvider.GetService<Repository>();

                    State = new GameState
                    {
                        Params = gameParams
                    };

                    var result = GameExecutor.StartGame(State);

                    if (result.ErrorCode == ExecutionErrorCode.Ok)
                    {
                        GameId = repository.AddGame(State.Params);

                        var action = new GameAction
                        {
                            GameId = GameId,
                            Type = GameActionType.StartGame,
                            TileId = result.FirstTile.Id,
                            Coords = result.FirstTileCoords,
                            TileOrientation = result.FirstTileOrientation
                        };

                        repository.AddAction(action);

                        StartMove();
                    }
                    else
                    {
                        throw new RequestException(ErrorCodes[result.ErrorCode]);
                    }
                }
            }
        }

        /// <summary>
        /// Zahájí další tah.
        /// </summary>
        private void StartMove()
        {
            lock (Lock)
            {
                using (var scope = ServiceScopeFactory.CreateScope())
                {
                    var repository = scope.ServiceProvider.GetService<Repository>();

                    var result = GameExecutor.StartMove(State);

                    if (result.ErrorCode == ExecutionErrorCode.Ok)
                    {
                        var action = new GameAction
                        {
                            GameId = GameId,
                            Type = GameActionType.StartMove,
                            Color = result.PlayerOnMove,
                            TileId = result.Tile.Id
                        };

                        repository.AddAction(action);
                    }
                    else if (result.ErrorCode == ExecutionErrorCode.NoTileRemaining)
                    {
                        GameEnd();
                    }
                    else
                    {
                        throw new RequestException(ErrorCodes[result.ErrorCode]);
                    }
                }
            }
        }

        /// <summary>
        /// Ukončí hru.
        /// </summary>
        private void GameEnd()
        {
            lock (Lock)
            {
                using (var scope = ServiceScopeFactory.CreateScope())
                {
                    var repository = scope.ServiceProvider.GetService<Repository>();

                    var result = GameExecutor.GameEnd(State);

                    if (result.ErrorCode == ExecutionErrorCode.Ok)
                    {
                        foreach (var removedFollower in result.RemovedFollowers)
                        {
                            var removedFollowerAction = new GameAction
                            {
                                GameId = GameId,
                                Type = GameActionType.RemoveFollower,
                                Color = removedFollower.Color,
                                Coords = removedFollower.Coords,
                                RegionId = removedFollower.RegionId,
                                Score = removedFollower.Score
                            };

                            repository.AddAction(removedFollowerAction);
                        }

                        var action = new GameAction
                        {
                            GameId = GameId,
                            Type = GameActionType.GameEnd
                        };

                        repository.AddAction(action);

                        repository.FinishGame(GameId, State.Score);

                        Finished = true;
                    }
                    else
                    {
                        throw new RequestException(ErrorCodes[result.ErrorCode]);
                    }
                }
            }
        }


        /// <inheritdoc/>
        public void PlaceTile(PlayerColor color, string tileId, Coords coords, TileOrientation orientation)
        {
            lock (Lock)
            {
                if (State?.Tile?.Id != tileId)
                {
                    throw new RequestException(ErrorCode.NotSameTile);
                }

                using (var scope = ServiceScopeFactory.CreateScope())
                {
                    var repository = scope.ServiceProvider.GetService<Repository>();

                    var result = GameExecutor.PlaceTile(State, color, State.Tile, coords, orientation);

                    if (result.ErrorCode == ExecutionErrorCode.Ok)
                    {
                        var action = new GameAction
                        {
                            GameId = GameId,
                            Type = GameActionType.PlaceTile,
                            Color = result.Color,
                            TileId = result.Tile.Id,
                            Coords = result.Coords,
                            TileOrientation = result.TileOrientation
                        };

                        repository.AddAction(action);
                    }
                    else
                    {
                        throw new RequestException(ErrorCodes[result.ErrorCode]);
                    }
                }
            }
        }

        /// <inheritdoc/>
        public void PlaceFollower(PlayerColor color, Coords coords, int regionId)
        {
            lock (Lock)
            {
                using (var scope = ServiceScopeFactory.CreateScope())
                {
                    var repository = scope.ServiceProvider.GetService<Repository>();

                    var result = GameExecutor.PlaceFollower(State, color, coords, regionId);

                    if (result.ErrorCode == ExecutionErrorCode.Ok)
                    {
                        var action = new GameAction
                        {
                            GameId = GameId,
                            Type = GameActionType.PlaceFollower,
                            Color = result.Color,
                            RegionId = result.RegionId,
                            Coords = result.Coords
                        };

                        repository.AddAction(action);

                        StartMove();
                    }
                    else
                    {
                        throw new RequestException(ErrorCodes[result.ErrorCode]);
                    }
                }
            }
        }

        /// <inheritdoc/>
        public void RemoveFollower(PlayerColor color, Coords coords, int regionId)
        {
            lock (Lock)
            {
                using (var scope = ServiceScopeFactory.CreateScope())
                {
                    var repository = scope.ServiceProvider.GetService<Repository>();

                    var result = GameExecutor.RemoveFollower(State, color, coords, regionId);

                    if (result.ErrorCode == ExecutionErrorCode.Ok)
                    {
                        var action = new GameAction
                        {
                            GameId = GameId,
                            Type = GameActionType.RemoveFollower,
                            Color = result.Color,
                            Coords = result.Coords,
                            RegionId = result.RegionId,
                            Score = result.Score
                        };

                        repository.AddAction(action);

                        repository.SetScore(GameId, color, State.Score[color]);

                        StartMove();
                    }
                    else
                    {
                        throw new RequestException(ErrorCodes[result.ErrorCode]);
                    }
                }
            }
        }

        /// <inheritdoc/>
        public void PassMove(PlayerColor color)
        {
            lock (Lock)
            {
                using (var scope = ServiceScopeFactory.CreateScope())
                {
                    var repository = scope.ServiceProvider.GetService<Repository>();

                    var result = GameExecutor.PassMove(State, color);

                    if (result.ErrorCode == ExecutionErrorCode.Ok)
                    {
                        var action = new GameAction
                        {
                            GameId = GameId,
                            ActionId = 0,
                            Type = GameActionType.PassMove,
                            Color = result.Color
                        };

                        repository.AddAction(action);

                        StartMove();
                    }
                    else
                    {
                        throw new RequestException(ErrorCodes[result.ErrorCode]);
                    }
                }
            }
        }
    }
}
