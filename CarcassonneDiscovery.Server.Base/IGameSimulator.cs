﻿namespace CarcassonneDiscovery.Server.Base
{
    using CarcassonneDiscovery.Entity;
    using CarcassonneDiscovery.Server.Database;

    /// <summary>
    /// Simulátor hry.
    /// </summary>
    public interface IGameSimulator
    {
        /// <summary>
        /// Identifikátor hry.
        /// </summary>
        int GameId { get; }

        /// <summary>
        /// Hra skončila?
        /// </summary>
        bool Finished { get; }

        /// <summary>
        /// Vrátí akci ve hře.
        /// </summary>
        /// <param name="actionId">Číslo akce ve hře.</param>
        /// <returns>Akce ve hře.</returns>
        /// <exception cref="RequestException">Pokud je akce neplatná.</exception>
        GameAction GetAction(int actionId);

        /// <summary>
        /// Vrátí detail hry.
        /// </summary>
        /// <returns>Detail hry.</returns>
        /// <exception cref="RequestException">Pokud je akce neplatná.</exception>
        Game GetDetail();

        /// <summary>
        /// Registrace jména hráče.
        /// </summary>
        /// <param name="color">Barva hráče.</param>
        /// <param name="name">Jméno hráče.</param>
        /// <exception cref="RequestException">Pokud je akce neplatná.</exception>
        void RegisterName(PlayerColor color, string name);

        /// <summary>
        /// Zahájí simulaci.
        /// </summary>
        /// <param name="gameParams">Parametry hry.</param>
        /// <exception cref="RequestException">Pokud je akce neplatná.</exception>
        void Start(GameParams gameParams);

        /// <summary>
        /// Položí kartičku.
        /// </summary>
        /// <param name="color">Barva hráče na tahu.</param>
        /// <param name="tileId">Identifikátor kartičky.</param>
        /// <param name="coords">Souřadnice kartičky.</param>
        /// <param name="orientation">Orientace kartičky.</param>
        /// <exception cref="RequestException">Pokud je akce neplatná.</exception>
        void PlaceTile(PlayerColor color, string tileId, Coords coords, TileOrientation orientation);

        /// <summary>
        /// Položí figurku.
        /// </summary>
        /// <param name="color">Barva hráče na tahu.</param>
        /// <param name="coords">Souřadnice kartičky, na kterém se region nachází.</param>
        /// <param name="regionId">Identifikátor regionu na kartičce.</param>
        /// <exception cref="RequestException">Pokud je akce neplatná.</exception>
        void PlaceFollower(PlayerColor color, Coords coords, int regionId);

        /// <summary>
        /// Odebere figurku.
        /// </summary>
        /// <param name="color">Barva hráče na tahu.</param>
        /// <param name="coords">Souřadnice kartičky, na které je figurka umístěna.</param>
        /// <param name="regionId">Identifikátor regionu, na kterém je figurka umístěna.</param>
        /// <exception cref="RequestException">Pokud je akce neplatná.</exception>
        void RemoveFollower(PlayerColor color, Coords coords, int regionId);

        /// <summary>
        /// Předá tah.
        /// </summary>
        /// <param name="color">Barva hráče na tahu.</param>
        /// <exception cref="RequestException">Pokud je akce neplatná.</exception>
        void PassMove(PlayerColor color);
    }
}
