﻿namespace CarcassonneDiscovery.Server.Base
{
    using CarcassonneDiscovery.Api;
    using CarcassonneDiscovery.Entity;
    using CarcassonneDiscovery.Server.Database;
    using Microsoft.Extensions.DependencyInjection;

    /// <summary>
    /// Pomocná třída pro hry, které již skončily.
    /// </summary>
    public class FinishedGameSimulator : IGameSimulator
    {
        /// <summary>
        /// Factory pro scope.
        /// </summary>
        private readonly IServiceScopeFactory ServiceScopeFactory;

        /// <inheritdoc/>
        public int GameId { get; private set; }

        /// <inheritdoc/>
        public bool Finished => true;

        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="serviceScopeFactory">Factory pro scope.</param>
        /// <param name="gameId">Id hry.</param>
        public FinishedGameSimulator(IServiceScopeFactory serviceScopeFactory, int gameId)
        {
            ServiceScopeFactory = serviceScopeFactory;
            GameId = gameId;
        }

        /// <inheritdoc/>
        public GameAction GetAction(int actionId)
        {
            using (var scope = ServiceScopeFactory.CreateScope())
            {
                var repository = scope.ServiceProvider.GetService<Repository>();
                return repository.GetAction(GameId, actionId);
            }
        }

        /// <inheritdoc/>
        public Game GetDetail()
        {
            using (var scope = ServiceScopeFactory.CreateScope())
            {
                var repository = scope.ServiceProvider.GetService<Repository>();
                return repository.GetGame(GameId, true);
            }
        }

        /// <inheritdoc/>
        public void RegisterName(PlayerColor color, string name)
        {
            throw new RequestException(ErrorCode.GameAlreadyFinished);
        }

        /// <inheritdoc/>
        public void Start(GameParams gameParams)
        {
            throw new RequestException(ErrorCode.GameAlreadyFinished);
        }

        /// <inheritdoc/>
        public void PlaceTile(PlayerColor color, string tileId, Coords coords, TileOrientation orientation)
        {
            throw new RequestException(ErrorCode.GameAlreadyFinished);
        }

        /// <inheritdoc/>
        public void PlaceFollower(PlayerColor color, Coords coords, int regionId)
        {
            throw new RequestException(ErrorCode.GameAlreadyFinished);
        }

        /// <inheritdoc/>
        public void PassMove(PlayerColor color)
        {
            throw new RequestException(ErrorCode.GameAlreadyFinished);
        }

        /// <inheritdoc/>
        public void RemoveFollower(PlayerColor color, Coords coords, int regionId)
        {
            throw new RequestException(ErrorCode.GameAlreadyFinished);
        }
    }
}
