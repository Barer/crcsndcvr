﻿namespace CarcassonneDiscovery.Server.Base
{
    using CarcassonneDiscovery.Api;
    using CarcassonneDiscovery.Entity;
    using CarcassonneDiscovery.Server.Database;
    using Microsoft.Extensions.DependencyInjection;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Řízení herního serveru.
    /// </summary>
    public class ServerManager
    {
        /// <summary>
        /// Maximální počet her v průběhu.
        /// </summary>
        private const int MaxGames = 256;

        /// <summary>
        /// Seznam simulací (v průběhu i ukončených).
        /// </summary>
        private readonly IDictionary<int, IGameSimulator> GameSimulations;

        /// <summary>
        /// Factory pro scope.
        /// </summary>
        private readonly IServiceScopeFactory ServiceScopeFactory;

        /// <summary>
        /// Zámek proti úpravám.
        /// </summary>
        private readonly object Lock;

        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="serviceScopeFactory">Factory pro scope.</param>
        public ServerManager(IServiceScopeFactory serviceScopeFactory)
        {
            GameSimulations = new Dictionary<int, IGameSimulator>();
            Lock = new object();

            ServiceScopeFactory = serviceScopeFactory;
        }

        /// <summary>
        /// Zahájení nové hry.
        /// </summary>
        /// <param name="gameParams">Parametry hry.</param>
        /// <returns>Id hry.</returns>
        /// <exception cref="RequestException">Pokud je akce neplatná.</exception>
        public int StartGame(GameParams gameParams)
        {
            lock (Lock)
            {
                if (GameSimulations.Count >= MaxGames)
                {
                    throw new RequestException(ErrorCode.TooManyGamesInProgress);
                }

                var simulator = new GameSimulator(ServiceScopeFactory);

                simulator.Start(gameParams);

                GameSimulations.Add(simulator.GameId, simulator);

                return simulator.GameId;
            }
        }

        /// <summary>
        /// Ukončení hry.
        /// </summary>
        /// <param name="gameId">Identifikátor hry.</param>
        public void StopGame(int gameId)
        {
            lock (Lock)
            {
                using (var scope = ServiceScopeFactory.CreateScope())
                {
                    var repository = scope.ServiceProvider.GetService<Repository>();

                    repository.StopGame(gameId);
                }

                GameSimulations.Remove(gameId);
            }
        }

        /// <summary>
        /// Seznam her v průběhu.
        /// </summary>
        /// <returns>Id her.</returns>
        /// <exception cref="RequestException">Pokud je akce neplatná.</exception>
        public IList<int> GetGamesInProgress()
        {
            return new List<int>(GameSimulations.Values.Where(x => !x.Finished).Select(x => x.GameId));
        }

        /// <summary>
        /// Seznam ukončených her.
        /// </summary>
        /// <returns>Id her.</returns>
        /// <exception cref="RequestException">Pokud je akce neplatná.</exception>
        public IList<int> GetFinishedGames()
        {
            return new List<int>(GameSimulations.Values.Where(x => x.Finished).Select(x => x.GameId));
        }

        /// <summary>
        /// Archivace ukončených her.
        /// </summary>
        /// <exception cref="RequestException">Pokud je akce neplatná.</exception>
        public void ArchiveFinishedGames()
        {
            lock (Lock)
            {
                foreach (var finishedId in GetFinishedGames())
                {
                    GameSimulations.Remove(finishedId);
                }
            }
        }

        /// <summary>
        /// Detail hry.
        /// </summary>
        /// <param name="gameId">Idenifikátor hry.</param>
        /// <exception cref="RequestException">Pokud je akce neplatná.</exception>
        public Game GetGameDetail(int gameId)
        {
            return FindSimulatorAndExecute(gameId, (simulator) => simulator.GetDetail());
        }

        /// <summary>
        /// Výsledek herní akce.
        /// </summary>
        /// <param name="gameId">Identifikátor hry.</param>
        /// <param name="actionId">Pořadové číslo akce.</param>
        /// <exception cref="RequestException">Pokud je akce neplatná.</exception>
        public GameAction GetAction(int gameId, int actionId)
        {
            return FindSimulatorAndExecute(gameId, (simulator) => simulator.GetAction(actionId));
        }

        /// <summary>
        /// Registrace jména hráče.
        /// </summary>
        /// <param name="gameId">Identifikátor hry.</param>
        /// <param name="color">Barva hráče.</param>
        /// <param name="name">Jméno hráče.</param>
        /// <exception cref="RequestException">Pokud je akce neplatná.</exception>
        public void RegisterName(int gameId, PlayerColor color, string name)
        {
            FindSimulatorAndExecute(gameId, (simulator) => simulator.RegisterName(color, name));
        }

        /// <summary>
        /// Položení kartičky.
        /// </summary>
        /// <param name="gameId">Identifikátor hry.</param>
        /// <param name="color">Barva hráče na tahu.</param>
        /// <param name="tileId">Identifikátor kartičky.</param>
        /// <param name="coords">Souřadnice kartičky.</param>
        /// <param name="orientation">Orientace kartičky.</param>
        /// <exception cref="RequestException">Pokud je akce neplatná.</exception>
        public void PlaceTile(int gameId, PlayerColor color, string tileId, Coords coords, TileOrientation orientation)
        {
            FindSimulatorAndExecute(gameId, (simulator) => simulator.PlaceTile(color, tileId, coords, orientation));
        }

        /// <summary>
        /// Položení figurky.
        /// </summary>
        /// <param name="gameId">Identifikátor hry.</param>
        /// <param name="color">Barva hráče na tahu.</param>
        /// <param name="coords">Souřadnice kartičky, na kterém se region nachází.</param>
        /// <param name="regionId">Identifikátor regionu na kartičce.</param>
        /// <exception cref="RequestException">Pokud je akce neplatná.</exception>
        public void PlaceFollower(int gameId, PlayerColor color, Coords coords, int regionId)
        {
            FindSimulatorAndExecute(gameId, (simulator) => simulator.PlaceFollower(color, coords, regionId));
        }

        /// <summary>
        /// Odebrání figurky.
        /// </summary>
        /// <param name="gameId">Identifikátor hry.</param>
        /// <param name="color">Barva hráče na tahu.</param>
        /// <param name="coords">Souřadnice kartičky, na které je figurka umístěna.</param>
        /// <param name="regionId">Identifikátor regionu, na kterém je figurka umístěna.</param>
        /// <exception cref="RequestException">Pokud je akce neplatná.</exception>
        public void RemoveFollower(int gameId, PlayerColor color, Coords coords, int regionId)
        {
            FindSimulatorAndExecute(gameId, (simulator) => simulator.RemoveFollower(color, coords, regionId));
        }

        /// <summary>
        /// Předání tahu.
        /// </summary>
        /// <param name="gameId">Identifikátor hry.</param>
        /// <param name="color">Barva hráče na tahu.</param>
        /// <exception cref="RequestException">Pokud je akce neplatná.</exception>
        public void PassMove(int gameId, PlayerColor color)
        {
            FindSimulatorAndExecute(gameId, (simulator) => simulator.PassMove(color));
        }

        /// <summary>
        /// Najde simulátor v seznamu her a provede na něm akci.
        /// </summary>
        /// <param name="gameId">Identifikátor hry.</param>
        /// <param name="action">Akce k provedení.</param>
        /// <exception cref="RequestException">Pokud je akce neplatná.</exception>
        private void FindSimulatorAndExecute(int gameId, Action<IGameSimulator> action)
        {
            FindSimulatorAndExecute(gameId, (simulator) => { action.Invoke(simulator); return 0; });
        }

        /// <summary>
        /// Najde simulátor v seznamu her a provede na něm akci.
        /// </summary>
        /// <typeparam name="T">Typ návratové hodnoty.</typeparam>
        /// <param name="gameId">Identifikátor hry.</param>
        /// <param name="action">Akce k provedení.</param>
        /// <returns>Návratová hodnota.</returns>
        /// <exception cref="RequestException">Pokud je akce neplatná.</exception>
        private T FindSimulatorAndExecute<T>(int gameId, Func<IGameSimulator, T> action)
        {
            IGameSimulator simulator;

            lock (Lock)
            {
                if (!GameSimulations.TryGetValue(gameId, out simulator))
                {
                    using (var scope = ServiceScopeFactory.CreateScope())
                    {
                        var repository = scope.ServiceProvider.GetService<Repository>();

                        repository.StopGame(gameId);

                        simulator = new FinishedGameSimulator(ServiceScopeFactory, gameId);

                        GameSimulations.Add(gameId, simulator);
                    }
                }
            }

            return action.Invoke(simulator);
        }
    }
}
