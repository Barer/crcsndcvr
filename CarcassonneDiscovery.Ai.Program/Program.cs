﻿namespace CarcassoneDiscovery.Client.Ai.Program
{
    using CarcassonneDiscovery.Ai.Players;
    using CarcassonneDiscovery.Client.Base;
    using CarcassonneDiscovery.Entity;
    using System;
    using System.Globalization;
    using System.Threading;

    /// <summary>
    /// Program umělé inteligence.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Vstupí bod programu.
        /// </summary>
        /// <param name="args">Argumenty programu.</param>
        public static void Main(string[] args)
        {
            AiPlayer player;

            string baseUrl, aiKind, playerColorStr, gameIdStr;

            if (args == null || args.Length == 0)
            {
                Console.WriteLine("No arguments specified. Specify them now.");

                Console.Write("Enter <baseUrl>: ");
                baseUrl = Console.ReadLine();

                Console.Write("Enter <aiKind>: ");
                aiKind = Console.ReadLine();

                Console.Write("Enter <playerColor>: ");
                playerColorStr = Console.ReadLine();

                Console.Write("Enter <gameId>: ");
                gameIdStr = Console.ReadLine();
            }
            else if (args.Length >= 4 && args.Length <= 7)
            {
                baseUrl = args[0];
                aiKind = args[1];
                playerColorStr = args[2];
                gameIdStr = args[3];
            }
            else
            {
                Console.Error.WriteLine("Expected no or 4-7 args: <baseUrl> <aiKind> <playerColor> <gameId> [<depth>]|[<savePath> [<loadPath>]]|[<simulationCount> <gamma> <gammaLimit>]");
                return;
            }

            PlayerColor color;
            switch (playerColorStr.ToUpper())
            {
                case "1":
                case "R":
                case "RED":
                    color = PlayerColor.Red;
                    break;
                case "2":
                case "G":
                case "GREEN":
                    color = PlayerColor.Green;
                    break;
                case "3":
                case "B":
                case "BLUE":
                    color = PlayerColor.Blue;
                    break;
                case "4":
                case "Y":
                case "YELLOW":
                    color = PlayerColor.Yellow;
                    break;
                case "5":
                case "K":
                case "BLACK":
                    color = PlayerColor.Black;
                    break;
                default:
                    Console.Error.WriteLine("Argument <playerColor> must be integer in range from 1 to 5 or a valid color name.");
                    return;
            }

            if (!int.TryParse(gameIdStr, out int gameId) || gameId <= 0)
            {
                Console.Error.WriteLine("Argument <gameId> must be positive integer.");
                return;
            }

            var service = new PlayerApiService($"{baseUrl.TrimEnd('/')}/Api/{{0}}/{{1}}");

            switch (aiKind)
            {
                case "rnd":
                case "random":
                    player = new HeuristicsAi<RandomHeuristics>(color, gameId, service);
                    break;

                case "gr":
                case "greedy":
                    player = new HeuristicsAi<GreedyHeuristics>(color, gameId, service);
                    break;

                case "pot":
                case "potential":
                    player = new HeuristicsAi<PotentialHeuristics>(color, gameId, service);
                    break;

                case "pl":
                case "learning":
                    string savePath, loadPath;
                    if (args.Length == 0)
                    {
                        Console.Write("Enter <savePath> (leave empty for no saving): ");
                        savePath = Console.ReadLine();
                        savePath = string.IsNullOrWhiteSpace(savePath) ? null : savePath;
                        Console.Write("Enter <loadPath> (leave empty for default coefficients): ");
                        loadPath = Console.ReadLine();
                        loadPath = string.IsNullOrWhiteSpace(loadPath) ? null : loadPath;
                    }
                    else if (args.Length == 5 || args.Length == 6)
                    {
                        savePath = args[4];
                        loadPath = args.Length == 6 ? args[5] : null;
                    }
                    else
                    {
                        Console.Error.WriteLine($"Invalid argument count for potential learning ai: got {args.Length}, expected 5 or 6.");
                        return;
                    }

                    player = new PotentialLearningAi(color, gameId, service, savePath, loadPath);
                    break;

                case "ex":
                case "expectiminimax":
                    string depthStr;
                    if (args.Length == 0)
                    {
                        Console.Write("Enter <depth>: ");
                        depthStr = Console.ReadLine();
                    }
                    else if (args.Length == 5)
                    {
                        depthStr = args[4];
                    }
                    else
                    {
                        Console.Error.WriteLine($"Invalid argument count for expectiminimax ai: got {args.Length}, expected 5.");
                        return;
                    }

                    if (!int.TryParse(depthStr, out int depth) || depth <= 0)
                    {
                        Console.Error.WriteLine("Argument <depth> must be positive integer.");
                        return;
                    }
                    player = new ExpectiminimaxAi<GreedyHeuristics>(color, gameId, service, depth);
                    break;

                case "mc":
                case "montecarlo":
                    string simulationCountStr, gammaStr, gammaLimitStr;
                    if (args.Length == 0)
                    {
                        Console.Write("Enter <simulationCount>: ");
                        simulationCountStr = Console.ReadLine();
                        Console.Write("Enter <gamma>: ");
                        gammaStr = Console.ReadLine();
                        Console.Write("Enter <gammaLimit>: ");
                        gammaLimitStr = Console.ReadLine();
                    }
                    else if (args.Length == 7)
                    {
                        simulationCountStr = args[4];
                        gammaStr = args[5];
                        gammaLimitStr = args[6];
                    }
                    else
                    {
                        Console.Error.WriteLine($"Invalid argument count for monte carlo ai: got {args.Length}, expected 7.");
                        return;
                    }

                    if (!int.TryParse(simulationCountStr, out int simulationCount) || simulationCount <= 0)
                    {
                        Console.Error.WriteLine("Argument <simulationCount> must be positive integer.");
                        return;
                    }
                    if (!double.TryParse(gammaStr, NumberStyles.Float, CultureInfo.GetCultureInfo("cs-CZ"), out var gamma) || gamma < 0 || gamma > 1)
                    {
                        Console.Error.WriteLine("Argument <gamma> must be real number in range [0;1].");
                        return;
                    }
                    if (!double.TryParse(gammaLimitStr, NumberStyles.Float, CultureInfo.GetCultureInfo("cs-CZ"), out var gammaLimit) || gammaLimit < 0 || gammaLimit > 1)
                    {
                        Console.Error.WriteLine("Argument <gammaLimit> must be real number in range [0;1].");
                        return;
                    }
                    player = new MonteCarloAi<GreedyHeuristics>(color, gameId, service, simulationCount, gamma, gammaLimit);
                    break;

                default:
                    Console.Error.WriteLine("Argument <aiKind> must be in range {rnd, random, gr, greedy, pot, potential, pl, learning, ex, expectiminimax, mc, montecarlo}");
                    return;
            }

            player.Start(verbose: true);

            while (player.IsRunning)
            {
                Thread.Sleep(5000);
            }

            Console.ReadLine();
        }
    }
}
